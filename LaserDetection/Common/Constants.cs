﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Common
{
    internal class Constants
    {
        internal class Settings
        {
            /// <summary>
            /// Folder path for saving profiles
            /// </summary>
            internal static string SaveProfilesPath = "SaveProfilesPath";

            /// <summary>
            /// Boolean to check if there is need to save profiles
            /// </summary>
            internal static string SaveProfiles = "SaveProfiles";

            /// <summary>
            /// Boolean if there is need to check wong profiles
            /// </summary>
            internal static string WrongProfileCheck = "WrongProfileCheck";

            /// <summary>
            /// Boolean to set is statistics is saved
            /// </summary>
            internal static string SaveStatistics = "SaveStatistics";

            /// <summary>
            /// Statistics folder path
            /// </summary>
            internal static string SaveStatisticsPath = "SaveStatisticsPath";

            /// <summary>
            /// If Sort signal is actually sent
            /// </summary>
            internal static string SendQualitySignal = "SendQualitySignal";


        }

        internal class Lasers
        {
            /// <summary>
            /// Laser2
            /// </summary>
            internal static Guid LaserLeft = new Guid("f907af80-e7b7-4fcf-ae19-7cb9d69af317");

            /// <summary>
            /// Laser3
            /// </summary>
            internal static Guid LaserRight = new Guid("20e54caf-fb35-4d07-98ce-0b7395595ef9");

            /// <summary>
            /// Laser1
            /// </summary>
            internal static Guid LaserTop = new Guid("1d4575f8-0ebf-48e4-b817-d3a0183e3e84");
        }

        internal class QualityClasses
        {
            internal static Guid Fixable = new Guid("467d854e-25fd-48fd-bced-91c131fca57c");
            internal static Guid NotFixable = new Guid("84f38c04-a4e0-48db-8cb1-a6f3d58e75d8");
            internal static Guid Correct = new Guid("33e9a320-a7a0-4a73-b280-aa2cd2d5c01c");
        }

        internal class Defects
        {
            internal static Guid Hole = new Guid("e95b63c1-957d-4fb7-b00c-69cafdc6642c");
            internal static Guid Crack = new Guid("f285233c-b032-4380-bd9b-e781f6212950");
            internal static Guid Peak = new Guid("57fbca1d-92f7-41dc-b004-810cbdc1eb5e");
        }

       

        internal class ImageHeight
        {
            internal static double Hole = 50;
            internal static double Peak = 150;
            internal static double Normal = 0;
        }

        internal class DefectParameters
        {
            internal static Guid Length = new Guid("00fb654d-1436-44f7-a52f-145db5947a17");
            internal static Guid Depth = new Guid("df6ad2b3-9f59-49ed-b991-24943419c0d8");
            internal static Guid Area = new Guid("7e24fe4d-f81e-4686-b07b-27e8e2fe002c");
            internal static Guid Height = new Guid("f9ec0c93-7eb4-44a8-bba5-adf86efec71a");
            internal static Guid Count = new Guid(" bebb3e6b-44b7-49cd-bfcf-ef1975b8218d");
        }
    }
}