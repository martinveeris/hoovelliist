﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Common
{
    public static class Extensions
    {
        public static double SampleVariance(this List<double> values, double mean, int start, int end)
        {
            double variance = 0;

            for (int i = start; i < end; i++)
            {
                variance += Math.Pow((values[i] - mean), 2);
            }

            int n = end - start;
            if (start > 0) n -= 1;

            return variance / (n - 1);
        }

        static object lockObject = new object();

        public static void InvokeIfRequired(this Control c, Action<Control> action)
        {
            try
            {
                lock (lockObject)
                {
                    if (c.InvokeRequired)
                    {
                        c.Invoke(new Action(() => action(c)));
                    }
                    else
                    {
                        action(c);
                    }
                }
            }
            catch { }
           
        }
    }
}
