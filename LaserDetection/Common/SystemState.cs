﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Common
{
    public static class SystemState
    {
        private static Laser.MultiLaserClass laserGetDataClass;
        private static Laser.SimulatedLaserClass laserGetDataClassSimulated;

        public static Statistics CurrentStatistics { get; set; }

        public static Guid? SelectedProductID { get; set; }

        public static Data.DataObjects.Product SelectedProduct { get; set; }

        public static bool IsControlRunning { get; set; }

        public static int CurrentActiveLasers = 0;

        public delegate void DataProcessingCompleteEventHandler(Laser.DataProcessingClass dataProcessingClass);
        public static event DataProcessingCompleteEventHandler DataProcessingCompleteEvent;


        public delegate void ProcessingCompleteHandler(string message);
        public static event ProcessingCompleteHandler ProcessingCompleteEvent;


        // list of currentry processed items
        public static List<Laser.DataProcessingClass> dataProccessingList = new List<Laser.DataProcessingClass>();

        // valitud toote laserite seaded
        private static List<Data.DataObjects.ProductLaserSetting> productSettingList = null;

        // sensors
        static Sensor.SensorReadClass sensorClass = new Sensor.SensorReadClass();

        public static void StartControl()
        {
            // set statistics
            InitStatistics();

            // Init start
            SetStartValues();

            // start scanners
            StartScanner();
        }

        private static async void StartScanner()
        {
            SelectedProduct = Data.Products.Get(SelectedProductID.Value);

            // active laser list
            var activeLaserSettingList = SelectedProduct.ProductLaserSettingList.Where(a => a.IsActive).ToList();

            // Laser 1
            productSettingList = SelectedProduct.ProductLaserSettingList.Where(a => a.IsActive == true).ToList();

            if (productSettingList.Count > 0)
            {
                CurrentActiveLasers = productSettingList.Count;

                if (false)
                {

                    if (laserGetDataClassSimulated == null)
                    {
                        laserGetDataClassSimulated = new Laser.SimulatedLaserClass();
                        Laser.SimulatedLaserClass.DataReceived += Laser1_DataReceived;
                    }
                    //Laser.SimulatedLaserClass.stopValue = false;

                    await laserGetDataClassSimulated.ConnectAsync(productSettingList); // await

                }
                else
                {
                    if (laserGetDataClass == null)
                    {
                        laserGetDataClass = new Laser.MultiLaserClass();
                        Laser.MultiLaserClass.DataReceived += Laser1_DataReceived;
                    }
                    Laser.MultiLaserClass.stopValue = false;

                    Task t = laserGetDataClass.ConnectAsync(productSettingList); // await
                    t.GetAwaiter().OnCompleted(() =>
                    {
                        if (t.Exception != null)
                            System.Windows.Forms.MessageBox.Show(t.Exception.InnerException.StackTrace);

                        if (ProcessingCompleteEvent != null)
                            ProcessingCompleteEvent("Lõpp");

                    });
                }

            }
        }

        private static void Laser1_DataReceived(Laser.LaserDataClass dataClass)
        {
            // collect all data to one object

            Laser.DataProcessingClass task = GetProcessingTask();
            task.AddData(dataClass);

        }

        /// <summary>
        /// Get task to add data to, if no current one, new is created
        /// </summary>
        /// <returns></returns>
        private static Laser.DataProcessingClass GetProcessingTask()
        {
            if (dataProccessingList.Count() == 0)
            {
                var task = new Laser.DataProcessingClass(CurrentActiveLasers, productSettingList, SelectedProduct);
                task.QualityCompleteEvent += Task_QualityCompleteEvent;
                dataProccessingList.Add(task);
                return task;
            }
            else
            {
                var existingTask = dataProccessingList.Where(a => a.ProcessingStatus == Laser.DataProcessingClass.ProcessingStatusEnum.WaitingData
                || a.ProcessingStatus == Laser.DataProcessingClass.ProcessingStatusEnum.ReceivingData).FirstOrDefault();
                if (existingTask != null)
                {
                    return existingTask;
                }
                else
                {
                    var task = new Laser.DataProcessingClass(CurrentActiveLasers, productSettingList, SelectedProduct);
                    task.QualityCompleteEvent += Task_QualityCompleteEvent;
                    dataProccessingList.Add(task);
                    return task;
                }
            }
        }

        private static void Task_QualityCompleteEvent(Laser.DataProcessingClass processingClass)
        {
            // anna signaal
            if (Data.Settings.GetSettingValueBoolean(Common.Constants.Settings.SendQualitySignal))
                sensorClass.SetSensorValue(processingClass.CalculatedQuality.SignalOutputISO);

            // näita kasutajaliidesele andmed välja
            if (DataProcessingCompleteEvent != null)
            {
                CurrentStatistics.SetQualityData(processingClass.CalculatedQuality, processingClass.NotCompletedProfileCount);

                DataProcessingCompleteEvent(processingClass); // TODO: päris klass, protsessi asünkoonselt
                dataProccessingList.Remove(processingClass); // remove from list

            }
        }

        public static void StopControl()
        {
            SetStopValues();

            Task.Delay(2000).ContinueWith(t => Laser.MultiLaserClass.Disconnect());
        }

        #region Statistics
        
        public static void ClearStatisticsCounters()
        {
            // 
        }
        #endregion

        #region InitStart

        private static void SetStopValues()
        {
            IsControlRunning = false;
            Laser.MultiLaserClass.stopValue = true;
        }


        private static void SetStartValues()
        {
            IsControlRunning = true;
            CurrentStatistics.ResetCounters();
        }

        private static void InitStatistics()
        {
            if (CurrentStatistics == null)
                CurrentStatistics = new Statistics();
        }
        #endregion
    }
}
