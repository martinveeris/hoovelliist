﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Common
{
    public class Statistics
    {
        public DateTime? ControlStartDateTime { get; set; }

        /// <summary>
        /// Count check details
        /// </summary>
        public int TotalDetailCount { get; set; }

        public int FixableTotalCount { get; set; }
        public int NotFixableTotalDetailCount { get; set; }
        public int CorrectTotalDetailCount { get; set; }
        public int DataPackageUnprocessedCount { get; set; }

        public void ResetCounters()
        {
            ControlStartDateTime = DateTime.Now;
            TotalDetailCount = 0;
            FixableTotalCount = 0;
            NotFixableTotalDetailCount = 0;
            CorrectTotalDetailCount = 0;
            DataPackageUnprocessedCount = 0;
        }

        public void SetQualityData(Data.DataObjects.QualityClass qualityClass, int dataPackageUnprocessedCount)
        {
            TotalDetailCount++;

            DataPackageUnprocessedCount = dataPackageUnprocessedCount;

            if (qualityClass.QualityClassID == Common.Constants.QualityClasses.Correct)
                CorrectTotalDetailCount++;
            else if (qualityClass.QualityClassID == Common.Constants.QualityClasses.Fixable)
                FixableTotalCount++;
            else if (qualityClass.QualityClassID == Common.Constants.QualityClasses.NotFixable)
                NotFixableTotalDetailCount++;
        }
    }
}
