﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.QualityCalculation
{
    public class QualityCalculationClass
    {
        List<Data.DataObjects.ProductDefect> allowedDefectList;
        List<Data.DataObjects.QualityClass> allowedQualityClassList;
        List<Guid> allowedQualityClassIDList;
        LinkedList<Guid> allowedQualityLinkedList = new LinkedList<Guid>();

        Dictionary<Guid, string> qualityClassNameList = new Dictionary<Guid, string>();
        Dictionary<Guid, int> qualityClassIsoSignalList = new Dictionary<Guid, int>();

        public QualityCalculationClass(Data.DataObjects.Product product)
        {

            // päri andmed välja
            allowedQualityClassList = product.QualityClassList.ToList();
            allowedQualityClassIDList = allowedQualityClassList.Select(a => a.QualityClassID).ToList();
            allowedDefectList = product.ProductDefectList.Where(a => allowedQualityClassIDList.Contains(a.QualityClassID)).ToList();

            // ehita üles selle tootepõhine hierarhia
            foreach (var qClassItem in allowedQualityClassList.OrderBy(s => s.Order))
            {
                qualityClassNameList.Add(qClassItem.QualityClassID, qClassItem.Name);
                qualityClassIsoSignalList.Add(qClassItem.QualityClassID, qClassItem.SignalOutputISO);

                // tekita nimeki järjestikustest klassidest
                allowedQualityLinkedList.AddLast(qClassItem.QualityClassID);
            }

        }

        public void CalculateQuality(Laser.DataProcessingClass dataProcessingClass)
        {

            // määra igale defektide klass
            foreach (var contourListItem in dataProcessingClass.contourList)
            {
                foreach (var contourItem  in contourListItem.Value)
                {
                    // määra klass
                    var calculatedClass = GetDefectClass(contourItem, dataProcessingClass.imageList[contourListItem.Key], contourListItem.Key);

                    if (calculatedClass != null)
                        dataProcessingClass.CalculatedQuality.CalculatedDefectList.Add(calculatedClass);
                }
            }

            // arvuta defektide pealt kvaliteet
            ClaculateQualityClass(dataProcessingClass.CalculatedQuality);
        }

        private bool CheckIfFitsToQuality(Guid qualityClassID, Data.DataObjects.QualityClass qualityClass)
        {
            bool qualified = true;


            // itereeri üle parameetrite
            foreach (var allowedDefect in allowedDefectList.Where(a => a.QualityClassID == qualityClassID))
            {
                foreach (var defectSetting in allowedDefect.ProductDefectSettingList.Where(a => a.IsActive))
                {
                    if (defectSetting.DefectParameterID == Common.Constants.DefectParameters.Area)
                    {
                        var defectCount = qualityClass.CalculatedDefectList.Where(a => a.Area > defectSetting.ToValue && a.DefectID == allowedDefect.DefectID).Count();
                        if (defectCount > 0)
                        {
                            qualified = false;
                            break;
                        }

                    }
                    if (defectSetting.DefectParameterID == Common.Constants.DefectParameters.Count)
                    {
                        var defectCount = qualityClass.CalculatedDefectList.Where(a => a.DefectID == allowedDefect.DefectID).Count();
                        if (defectCount > defectSetting.ToValue)
                        {
                            qualified = false;
                            break;
                        }

                    }
                    if (defectSetting.DefectParameterID == Common.Constants.DefectParameters.Length)
                    {
                        var defectCount = qualityClass.CalculatedDefectList.Where(a => a.Length > defectSetting.ToValue && a.DefectID == allowedDefect.DefectID).Count();
                        if (defectCount > 0)
                        {
                            qualified = false;
                            break;
                        }

                    }
                }


            }

            return qualified;

        }

        private void ClaculateQualityClass(Data.DataObjects.QualityClass qualityClass)
        {

            foreach (var qualityID in allowedQualityLinkedList)
            {
                // kui on viimane, siis määra alati see kvaliteediks
               if(qualityID == allowedQualityLinkedList.Last.Value)
                {
                    qualityClass.QualityClassID = qualityID;
                    qualityClass.Name = qualityClassNameList[qualityID]; // nimi
                    qualityClass.SignalOutputISO = qualityClassIsoSignalList[qualityID]; // signaal
                } else
                {
                    // kui sobib, siis määra
                    if(CheckIfFitsToQuality(qualityID, qualityClass))
                    {
                        qualityClass.QualityClassID = qualityID;
                        qualityClass.Name = qualityClassNameList[qualityID]; // nimi
                        qualityClass.SignalOutputISO = qualityClassIsoSignalList[qualityID]; // signaal
                        break;
                    }
                }
            }

          
        }

        private Data.DataObjects.QualityClass.CalculatedDefectClass GetDefectClass(Contour<Point> contour, Emgu.CV.Image<Bgr, double> image, Guid LaserID)
        {

            if (contour.GetMinAreaRect().size.IsEmpty)
                return null;

            

            Data.DataObjects.QualityClass.CalculatedDefectClass defect = new Data.DataObjects.QualityClass.CalculatedDefectClass();
            defect.Area = contour.Area;

            Bgr color = image[contour[0].Y, contour[0].X];


            defect.Width = contour.GetMinAreaRect().size.Width;
            defect.Height = contour.GetMinAreaRect().size.Height;
            defect.Length = Math.Max(defect.Height, defect.Width);

            // triipu pildi all ignoreeri
            //if (defect.Width == Math.Max(image.Size.Height, image.Size.Width))
            //    return null;

            if (color.Red == 255)
            {
                defect.DefectID = Common.Constants.Defects.Peak;
                defect.DefectName = "Kühm";
               
            }
            else if (color.Green == 128)
            {
                // Jaguneb praoks ja auguks
                defect.DefectID = Common.Constants.Defects.Hole;
                defect.DefectName = "Auk";

                if (Math.Min(defect.Height, defect.Width) / Math.Max(defect.Height, defect.Width) < 0.25)
                {
                    defect.DefectID = Common.Constants.Defects.Crack;
                    defect.DefectName = "Pragu";
                }
            }

            // kui klass määramata
            if (defect.DefectID == Guid.Empty)
                return null;

            if (LaserID == Common.Constants.Lasers.LaserTop)
                defect.LaserName = "Top";
            else if (LaserID == Common.Constants.Lasers.LaserLeft)
                defect.LaserName = "Left";
            else if (LaserID == Common.Constants.Lasers.LaserRight)
                defect.LaserName = "Right";

            return defect;
        }

    }
}
