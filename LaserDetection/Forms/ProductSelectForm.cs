﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class ProductSelectForm : Form
    {
        private Guid? SelectedProductID = null;

        public ProductSelectForm()
        {
            InitializeComponent();

            InitData();
        }

        private void InitData()
        {
            bindingSourceProductList.DataSource = Data.Products.GetList();
        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            var dataRow = gridView1.GetRow(e.FocusedRowHandle) as Data.DataObjects.Product;

            if (dataRow != null)
            {
                lblName.Text = dataRow.Name;
                lblCode.Text = dataRow.Code;
                lblWidth.Text = string.Format("{0}mm", dataRow.Width);
                lblAllowedQualities.Text = string.Join(@"/", dataRow.QualityClassList.Select(a => a.Name).ToList());

                SelectedProductID = dataRow.ProductID;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Common.SystemState.SelectedProductID = SelectedProductID;
        }
    }
}
