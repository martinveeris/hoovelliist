﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class SystemSettingsForm : Form
    {
        public SystemSettingsForm()
        {
            InitializeComponent();
        }

        private void SystemSettingsForm_Shown(object sender, EventArgs e)
        {
            // Save profiles
            var saveProfiles = Data.Settings.GetSettingValueBoolean(Common.Constants.Settings.SaveProfiles);
            chkProfiles.Checked = saveProfiles;

            // Profile path
            var profilePath = Data.Settings.GetSettingValueString(Common.Constants.Settings.SaveProfilesPath);
            txtProfiles.Text = profilePath;

            // Profile check
            var wrongProfileCheck = Data.Settings.GetSettingValueBoolean(Common.Constants.Settings.WrongProfileCheck);
            chkProductWarning.Checked = wrongProfileCheck;

            // Save statistics
            var saveStatistics = Data.Settings.GetSettingValueBoolean(Common.Constants.Settings.SaveStatistics);
            chkSaveStatistics.Checked = saveStatistics;

            // Save statistics path
            var saveStatisticsPath = Data.Settings.GetSettingValueString(Common.Constants.Settings.SaveStatisticsPath);
            txtStatistics.Text = saveStatisticsPath;

            // Quality signal
            var sendQualitySignal = Data.Settings.GetSettingValueBoolean(Common.Constants.Settings.SendQualitySignal);
            chkSortSignal.Checked = sendQualitySignal;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOpenFolderProfiles_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                txtProfiles.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Save profiles
            Data.Settings.SetSettingValueBoolean(Common.Constants.Settings.SaveProfiles, chkProfiles.Checked);

            // Profile path
            Data.Settings.SetSettingValueString(Common.Constants.Settings.SaveProfilesPath, txtProfiles.Text);

            // Profile check
            Data.Settings.SetSettingValueBoolean(Common.Constants.Settings.WrongProfileCheck, chkProductWarning.Checked);

            // Save statistics
            Data.Settings.SetSettingValueBoolean(Common.Constants.Settings.SaveStatistics, chkSaveStatistics.Checked);

            // Save statistics path
            Data.Settings.SetSettingValueString(Common.Constants.Settings.SaveStatisticsPath, txtStatistics.Text);

            // Quality signal
            Data.Settings.SetSettingValueBoolean(Common.Constants.Settings.SendQualitySignal, chkSortSignal.Checked);

        }

        private void btnOpenFolderStatistics_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                txtStatistics.Text = folderBrowserDialog.SelectedPath;
            }
        }
    }
}
