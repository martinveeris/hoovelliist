﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class QualityClassDetailForm : Form
    {
        public Data.DataObjects.QualityClass QualityClassObject { get; set; }

        public QualityClassDetailForm()
        {
            InitializeComponent();
        }

        public void SetData()
        {
            if (QualityClassObject != null)
            {
                txtName.Text = QualityClassObject.Name;
                cmbSortSignal.SelectedItem = QualityClassObject.SignalOutputISO.ToString();
            }
        }

        private void QualityClassDetailForm_Load(object sender, EventArgs e)
        {
            SetData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (QualityClassObject != null)
            {
                QualityClassObject.Name = txtName.Text;
                QualityClassObject.SignalOutputISO = int.Parse(cmbSortSignal.SelectedItem.ToString());
            }
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            if (txtName.Text.Length == 0)
            {
                e.Cancel = true;
                txtName.ErrorText = "Nõutud tähemärkide arv vähemalt 1";
            }
               
        }

        private void QualityClassDetailForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
