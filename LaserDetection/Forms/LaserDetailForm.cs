﻿using DevExpress.XtraCharts;
using MathNet.Numerics.LinearAlgebra.Complex32;
using MathNet.Numerics.LinearAlgebra.Complex;
using MathNet.Numerics.LinearAlgebra.Factorization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class LaserDetailForm : Form
    {
        public Data.DataObjects.Laser LaserObject { get; set; }

        double[] xValues = new double[0];
        double[] zValues = new double[0];
        ushort[] wValues = new ushort[0];

        List<double> newXData = new List<double>();
        List<double> newZData = new List<double>();
        List<double> newWData = new List<double>();

        public LaserDetailForm()
        {
            InitializeComponent();
        }

        public void SetData()
        {
            if(LaserObject != null)
            {
                txtName.Text = LaserObject.Name;
                txtIPAddress.Text = LaserObject.IPAddress;
                txtExposureTime.Text = LaserObject.ExposureTime.ToString();
            }
        }

        private void LaserDetailForm_Load(object sender, EventArgs e)
        {
            SetData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (LaserObject != null)
            {
                LaserObject.Name = txtName.Text;
                LaserObject.IPAddress = txtIPAddress.Text;
                LaserObject.ExposureTime = Decimal.Parse(txtExposureTime.Text);
            }
        }

        private void btnShowProfile_Click(object sender, EventArgs e)
        {

            newXData.Clear();
            newZData.Clear();
            newWData.Clear();

            //if (System.Diagnostics.Debugger.IsAttached)
            //{
            //    newXData.Clear();
            //    newZData.Clear();

            //    xValues = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            //    zValues = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            //    wValues = new ushort[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            //} else
            //{
                Laser.SingleLaserClass laserClass = new Laser.SingleLaserClass();
                laserClass.Connect(LaserObject.IPAddress);

                laserClass.GetProfiles(ref zValues, ref xValues, ref wValues);
                laserClass.Disconnect();
            //}


          

            List<ChartDataClass> dataList = new List<ChartDataClass>();
            for (int i = 0; i < zValues.Count(); i++)
            {
                if (zValues[i] == 0)
                    continue;

                newXData.Add(xValues[i]);
                newZData.Add(zValues[i]);
                newWData.Add(wValues[i]);

                if (chkData.Checked)
                    dataList.Add(new ChartDataClass() { ChartValueX = xValues[i], ChartValueY = zValues[i] });
                else
                    dataList.Add(new ChartDataClass() { ChartValueX = xValues[i], ChartValueY = wValues[i] });
            }


            Series series = chartControl1.Series.Where(a => a.Name == "S1").First() as Series;
            series.ArgumentDataMember = "ChartValueX";
            series.SeriesPointsSortingKey = SeriesPointKey.Argument;
            series.DataSource = dataList;
            series.ValueDataMembers.AddRange(new string[] { "ChartValueY" });

            if (chkData.Checked)
                CalculateLine(newXData, newZData);
            else
                CalculateLine(newXData, newWData);


        }

        double[] baseCurve = null;

        private void CalculateLine(List<double> newXData, List<double> newZData)
        {

            if (chkLinear.Checked)
            {
                // joon
                var yResults = Utils.MathUtils.GetFittedLine(newXData.ToArray(), newZData.ToArray());

                List<ChartDataClass> dataListLinear = new List<ChartDataClass>();
                for (int j = 0; j < newXData.Count; j++)
                {
                    // arvuta uus Y väärtus
                    dataListLinear.Add(new ChartDataClass() { ChartValueX = newXData[j], ChartValueY = yResults[j] });
                }

                Series series = chartControl1.Series.Where(a => a.Name == "S2").First() as Series;
                series.ArgumentDataMember = "ChartValueX";
                series.SeriesPointsSortingKey = SeriesPointKey.Argument;
                series.DataSource = dataListLinear;
                series.ValueDataMembers.AddRange(new string[] { "ChartValueY" });

                List<ChartDataClass> dataListDiff = new List<ChartDataClass>();
                var diffResults = Utils.MathUtils.CalculateDifference(yResults, newZData.ToArray(), 0.1, true);
                for (int f = 0; f < diffResults.Count(); f++)
                {
                    // arvuta uus Y väärtus
                    dataListDiff.Add(new ChartDataClass() { ChartValueX = newXData[f], ChartValueY = diffResults[f] });
                }
                Series serie2s = chartControl1.Series.Where(a => a.Name == "S4").First() as Series;
                serie2s.ArgumentDataMember = "ChartValueX";
                serie2s.SeriesPointsSortingKey = SeriesPointKey.Argument;
                serie2s.DataSource = dataListDiff;
                serie2s.ValueDataMembers.AddRange(new string[] { "ChartValueY" });
            }

            if (chkPolunimical.Checked)
            {

                // kurvi Y väärtused
                double[] outputCurve = null;

                var yResults = Utils.MathUtils.GetFittedCurve(newXData.ToArray(), newZData.ToArray(), 19, out outputCurve);

                if (baseCurve == null)
                {
                    baseCurve = outputCurve;
                }
                else
                {
                    //newXData = new List<double>(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 });

                    // arvuta esimese leitud kurvi pealt (kui toimib, siis muuta meetodeid ja baasi, testida ka nihet)
                    yResults = Utils.MathUtils.GetFittedCurve(newXData.ToArray(), newZData.ToArray(), 5, baseCurve);

                }

                List<ChartDataClass> dataListLinear = new List<ChartDataClass>();
                for (int j = 0; j < newXData.Count; j++)
                {
                    var newYP = yResults[j];

                    dataListLinear.Add(new ChartDataClass() { ChartValueX = newXData[j], ChartValueY = newYP });
                }

                //var p = Polyfit(newXData.ToArray(), newZData.ToArray(), 3);

                //for (int i = 0; i < x.Length; i++)
                //    Console.WriteLine("{0} => {1} diff {2}", x[i], Polyval(p, x[i]), y[i] - Polyval(p, x[i]));

                Series series = chartControl1.Series.Where(a => a.Name == "S3").First() as Series;
                series.ArgumentDataMember = "ChartValueX";
                series.SeriesPointsSortingKey = SeriesPointKey.Argument;
                series.DataSource = dataListLinear;
                series.ValueDataMembers.AddRange(new string[] { "ChartValueY" });

                List<ChartDataClass> dataListDiff = new List<ChartDataClass>();
                var diffResults = Utils.MathUtils.CalculateDifference(yResults, newZData.ToArray(), 0.15, true);
                for (int f = 0; f < diffResults.Count(); f++)
                {
                    // arvuta uus Y väärtus
                    dataListDiff.Add(new ChartDataClass() { ChartValueX = newXData[f], ChartValueY = diffResults[f] });
                }
                Series serie2s = chartControl1.Series.Where(a => a.Name == "S4").First() as Series;
                serie2s.ArgumentDataMember = "ChartValueX";
                serie2s.SeriesPointsSortingKey = SeriesPointKey.Argument;
                serie2s.DataSource = dataListDiff;
                serie2s.ValueDataMembers.AddRange(new string[] { "ChartValueY" });
            }

            SetSeriesVisibility();
        }

        private void SetSeriesVisibility()
        {

            Series series2 = chartControl1.Series.Where(a => a.Name == "S2").First() as Series;
            if (series2 != null)
                series2.Visible = chkLinear.Checked;

            Series series3 = chartControl1.Series.Where(a => a.Name == "S3").First() as Series;
            if (series3 != null)
                series3.Visible = chkPolunimical.Checked;

            Series series4 = chartControl1.Series.Where(a => a.Name == "S4").First() as Series;
            if (series4 != null)
                series4.Visible = chkDiff.Checked;

        }

        private void checkEditRemoveZeros_EditValueChanged(object sender, EventArgs e)
        {
            //if (checkEditRemoveZeros.Checked)
            //    sparklineEdit1.Data = zValues.Where(a => a > 0).ToArray();
            //else
            //    sparklineEdit1.Data = zValues;

        }

        public class ChartDataClass
        {
            public double ChartValueX { get; set; }
            public double ChartValueY { get; set; }
        }

        private void chkLinear_CheckedChanged(object sender, EventArgs e)
        {
            if (chkData.Checked)
                CalculateLine(newXData, newZData);
            else
                CalculateLine(newXData, newWData);
        }

        private void chkDiff_CheckedChanged(object sender, EventArgs e)
        {
            SetSeriesVisibility();
        }
    }
}
