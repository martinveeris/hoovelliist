﻿namespace LaserDetection.Forms
{
    partial class ProductDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.RangeControlRange rangeControlRange1 = new DevExpress.XtraEditors.RangeControlRange();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductDetailForm));
            DevExpress.XtraEditors.RangeControlRange rangeControlRange2 = new DevExpress.XtraEditors.RangeControlRange();
            DevExpress.XtraEditors.RangeControlRange rangeControlRange3 = new DevExpress.XtraEditors.RangeControlRange();
            this.rangeControl1 = new DevExpress.XtraEditors.RangeControl();
            this.numericChartRangeControlClient1 = new DevExpress.XtraEditors.NumericChartRangeControlClient();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnAddDefect = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.checkEditLaser3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditLaser2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditLaser1 = new DevExpress.XtraEditors.CheckEdit();
            this.rangeControl3 = new DevExpress.XtraEditors.RangeControl();
            this.numericChartRangeControlClient3 = new DevExpress.XtraEditors.NumericChartRangeControlClient();
            this.rangeControl2 = new DevExpress.XtraEditors.RangeControl();
            this.numericChartRangeControlClient2 = new DevExpress.XtraEditors.NumericChartRangeControlClient();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceProductDefects = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.chkListQualityClasses = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.qualityClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblWidth = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnMeasureProfile = new DevExpress.XtraEditors.SimpleButton();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemLaser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLaser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLaser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceProductDefects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkListQualityClasses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.qualityClassBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // rangeControl1
            // 
            this.rangeControl1.Client = this.numericChartRangeControlClient1;
            this.rangeControl1.Location = new System.Drawing.Point(66, 125);
            this.rangeControl1.Name = "rangeControl1";
            rangeControlRange1.Maximum = 9D;
            rangeControlRange1.Minimum = 0D;
            rangeControlRange1.Owner = this.rangeControl1;
            this.rangeControl1.SelectedRange = rangeControlRange1;
            this.rangeControl1.SelectionType = DevExpress.XtraEditors.RangeControlSelectionType.ThumbAndFlag;
            this.rangeControl1.Size = new System.Drawing.Size(440, 114);
            this.rangeControl1.StyleController = this.layoutControl1;
            this.rangeControl1.TabIndex = 19;
            this.rangeControl1.Text = "rangeControl1";
            this.rangeControl1.RangeChanged += new DevExpress.XtraEditors.RangeChangedEventHandler(this.rangeControl2_RangeChanged);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnAddDefect);
            this.layoutControl1.Controls.Add(this.checkEditLaser3);
            this.layoutControl1.Controls.Add(this.checkEditLaser2);
            this.layoutControl1.Controls.Add(this.checkEditLaser1);
            this.layoutControl1.Controls.Add(this.rangeControl3);
            this.layoutControl1.Controls.Add(this.rangeControl2);
            this.layoutControl1.Controls.Add(this.rangeControl1);
            this.layoutControl1.Controls.Add(this.labelControl5);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.btnSave);
            this.layoutControl1.Controls.Add(this.btnClose);
            this.layoutControl1.Controls.Add(this.labelControl4);
            this.layoutControl1.Controls.Add(this.chkListQualityClasses);
            this.layoutControl1.Controls.Add(this.lblWidth);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.btnMeasureProfile);
            this.layoutControl1.Controls.Add(this.txtCode);
            this.layoutControl1.Controls.Add(this.txtName);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(941, 632);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnAddDefect
            // 
            this.btnAddDefect.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.btnAddDefect.DropDownControl = this.popupMenu1;
            this.btnAddDefect.Image = ((System.Drawing.Image)(resources.GetObject("btnAddDefect.Image")));
            this.btnAddDefect.Location = new System.Drawing.Point(721, 516);
            this.btnAddDefect.Name = "btnAddDefect";
            this.btnAddDefect.Size = new System.Drawing.Size(208, 38);
            this.btnAddDefect.StyleController = this.layoutControl1;
            this.btnAddDefect.TabIndex = 25;
            this.btnAddDefect.Text = "Lisa kontrollitav defekt";
            // 
            // popupMenu1
            // 
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(941, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 632);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(941, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 632);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(941, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 632);
            // 
            // checkEditLaser3
            // 
            this.checkEditLaser3.Location = new System.Drawing.Point(66, 386);
            this.checkEditLaser3.Name = "checkEditLaser3";
            this.checkEditLaser3.Properties.Caption = "";
            this.checkEditLaser3.Size = new System.Drawing.Size(440, 19);
            this.checkEditLaser3.StyleController = this.layoutControl1;
            this.checkEditLaser3.TabIndex = 24;
            this.checkEditLaser3.CheckedChanged += new System.EventHandler(this.checkEditLaser3_CheckedChanged);
            // 
            // checkEditLaser2
            // 
            this.checkEditLaser2.Location = new System.Drawing.Point(66, 243);
            this.checkEditLaser2.Name = "checkEditLaser2";
            this.checkEditLaser2.Properties.Caption = "";
            this.checkEditLaser2.Size = new System.Drawing.Size(440, 19);
            this.checkEditLaser2.StyleController = this.layoutControl1;
            this.checkEditLaser2.TabIndex = 23;
            this.checkEditLaser2.CheckedChanged += new System.EventHandler(this.checkEditLaser2_CheckedChanged);
            // 
            // checkEditLaser1
            // 
            this.checkEditLaser1.Location = new System.Drawing.Point(66, 102);
            this.checkEditLaser1.Name = "checkEditLaser1";
            this.checkEditLaser1.Properties.Caption = "";
            this.checkEditLaser1.Size = new System.Drawing.Size(440, 19);
            this.checkEditLaser1.StyleController = this.layoutControl1;
            this.checkEditLaser1.TabIndex = 22;
            this.checkEditLaser1.CheckedChanged += new System.EventHandler(this.checkEditLaser1_CheckedChanged);
            // 
            // rangeControl3
            // 
            this.rangeControl3.Client = this.numericChartRangeControlClient3;
            this.rangeControl3.Location = new System.Drawing.Point(66, 409);
            this.rangeControl3.Name = "rangeControl3";
            rangeControlRange2.Maximum = 9D;
            rangeControlRange2.Minimum = 0D;
            rangeControlRange2.Owner = this.rangeControl3;
            this.rangeControl3.SelectedRange = rangeControlRange2;
            this.rangeControl3.SelectionType = DevExpress.XtraEditors.RangeControlSelectionType.ThumbAndFlag;
            this.rangeControl3.Size = new System.Drawing.Size(440, 103);
            this.rangeControl3.StyleController = this.layoutControl1;
            this.rangeControl3.TabIndex = 21;
            this.rangeControl3.Text = "rangeControl3";
            this.rangeControl3.RangeChanged += new DevExpress.XtraEditors.RangeChangedEventHandler(this.rangeControl2_RangeChanged);
            // 
            // rangeControl2
            // 
            this.rangeControl2.Client = this.numericChartRangeControlClient2;
            this.rangeControl2.Location = new System.Drawing.Point(66, 266);
            this.rangeControl2.Name = "rangeControl2";
            rangeControlRange3.Maximum = 36D;
            rangeControlRange3.Minimum = 0D;
            rangeControlRange3.Owner = this.rangeControl2;
            this.rangeControl2.SelectedRange = rangeControlRange3;
            this.rangeControl2.SelectionType = DevExpress.XtraEditors.RangeControlSelectionType.ThumbAndFlag;
            this.rangeControl2.Size = new System.Drawing.Size(440, 116);
            this.rangeControl2.StyleController = this.layoutControl1;
            this.rangeControl2.TabIndex = 20;
            this.rangeControl2.Text = "rangeControl2";
            this.rangeControl2.RangeChanged += new DevExpress.XtraEditors.RangeChangedEventHandler(this.rangeControl2_RangeChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(510, 102);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(419, 16);
            this.labelControl5.StyleController = this.layoutControl1;
            this.labelControl5.TabIndex = 17;
            this.labelControl5.Text = "Toote defektid";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bindingSourceProductDefects;
            this.gridControl1.Location = new System.Drawing.Point(510, 122);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(419, 390);
            this.gridControl1.TabIndex = 16;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Defekt";
            this.gridColumn1.FieldName = "DefectName";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Parameetrid";
            this.gridColumn2.FieldName = "DefectSettingsString";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Kvaliteediklass";
            this.gridColumn3.FieldName = "QualityClassName";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(622, 582);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(141, 38);
            this.btnSave.StyleController = this.layoutControl1;
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "Salvesta";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(767, 582);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(162, 38);
            this.btnClose.StyleController = this.layoutControl1;
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Sulge";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(510, 12);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(419, 16);
            this.labelControl4.StyleController = this.layoutControl1;
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Toote lubatud kvaliteedid:";
            // 
            // chkListQualityClasses
            // 
            this.chkListQualityClasses.CheckOnClick = true;
            this.chkListQualityClasses.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkListQualityClasses.DataSource = this.qualityClassBindingSource;
            this.chkListQualityClasses.DisplayMember = "Name";
            this.chkListQualityClasses.Location = new System.Drawing.Point(510, 32);
            this.chkListQualityClasses.Name = "chkListQualityClasses";
            this.chkListQualityClasses.Size = new System.Drawing.Size(419, 66);
            this.chkListQualityClasses.StyleController = this.layoutControl1;
            this.chkListQualityClasses.TabIndex = 12;
            this.chkListQualityClasses.ValueMember = "QualityClassID";
            // 
            // lblWidth
            // 
            this.lblWidth.Location = new System.Drawing.Point(66, 516);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(440, 38);
            this.lblWidth.StyleController = this.layoutControl1;
            this.lblWidth.TabIndex = 11;
            this.lblWidth.Text = "0,0mm";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 12F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 60);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(215, 38);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Toote profiili lugemiseks aseta \r\nkorrektne toode seadmesse";
            // 
            // btnMeasureProfile
            // 
            this.btnMeasureProfile.Image = ((System.Drawing.Image)(resources.GetObject("btnMeasureProfile.Image")));
            this.btnMeasureProfile.Location = new System.Drawing.Point(335, 60);
            this.btnMeasureProfile.Name = "btnMeasureProfile";
            this.btnMeasureProfile.Size = new System.Drawing.Size(171, 38);
            this.btnMeasureProfile.StyleController = this.layoutControl1;
            this.btnMeasureProfile.TabIndex = 7;
            this.btnMeasureProfile.Text = "Mõõda toote profiil";
            this.btnMeasureProfile.Click += new System.EventHandler(this.btnMeasureProfile_Click);
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(66, 36);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(440, 20);
            this.txtCode.StyleController = this.layoutControl1;
            this.txtCode.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(66, 12);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(440, 20);
            this.txtName.StyleController = this.layoutControl1;
            this.txtName.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.emptySpaceItem3,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem6,
            this.layoutControlItemLaser1,
            this.layoutControlItemLaser2,
            this.layoutControlItemLaser3,
            this.layoutControlItem3,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.emptySpaceItem7,
            this.emptySpaceItem4,
            this.layoutControlItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(941, 632);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtName;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(108, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "Nimi";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtCode;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(108, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(498, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Kood";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnMeasureProfile;
            this.layoutControlItem4.Location = new System.Drawing.Point(323, 48);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(138, 42);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(175, 42);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(219, 48);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(104, 42);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl1;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(219, 42);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(219, 42);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lblWidth;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 504);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(90, 17);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(498, 42);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Laius:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnClose;
            this.layoutControlItem10.Location = new System.Drawing.Point(755, 570);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(166, 42);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnSave;
            this.layoutControlItem11.Location = new System.Drawing.Point(610, 570);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(145, 42);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 570);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(610, 42);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemLaser1
            // 
            this.layoutControlItemLaser1.Control = this.rangeControl1;
            this.layoutControlItemLaser1.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItemLaser1.MinSize = new System.Drawing.Size(151, 20);
            this.layoutControlItemLaser1.Name = "layoutControlItemLaser1";
            this.layoutControlItemLaser1.Size = new System.Drawing.Size(498, 118);
            this.layoutControlItemLaser1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLaser1.Text = "LaserLeft";
            this.layoutControlItemLaser1.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItemLaser2
            // 
            this.layoutControlItemLaser2.Control = this.rangeControl2;
            this.layoutControlItemLaser2.Location = new System.Drawing.Point(0, 254);
            this.layoutControlItemLaser2.MinSize = new System.Drawing.Size(150, 20);
            this.layoutControlItemLaser2.Name = "layoutControlItemLaser2";
            this.layoutControlItemLaser2.Size = new System.Drawing.Size(498, 120);
            this.layoutControlItemLaser2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLaser2.Text = "LaserTop";
            this.layoutControlItemLaser2.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItemLaser3
            // 
            this.layoutControlItemLaser3.Control = this.rangeControl3;
            this.layoutControlItemLaser3.Location = new System.Drawing.Point(0, 397);
            this.layoutControlItemLaser3.MinSize = new System.Drawing.Size(156, 20);
            this.layoutControlItemLaser3.Name = "layoutControlItemLaser3";
            this.layoutControlItemLaser3.Size = new System.Drawing.Size(498, 107);
            this.layoutControlItemLaser3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLaser3.Text = "LaserRight";
            this.layoutControlItemLaser3.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.checkEditLaser1;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(77, 23);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(498, 23);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Aktiivne";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.checkEditLaser2;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 231);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(77, 23);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(498, 23);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Aktiivne";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkEditLaser3;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 374);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(77, 23);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(498, 23);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "Aktiivne";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(51, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl4;
            this.layoutControlItem9.Location = new System.Drawing.Point(498, 0);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(173, 20);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(423, 20);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.chkListQualityClasses;
            this.layoutControlItem8.Location = new System.Drawing.Point(498, 20);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(54, 4);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(423, 70);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.labelControl5;
            this.layoutControlItem13.Location = new System.Drawing.Point(498, 90);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(98, 20);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(423, 20);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControl1;
            this.layoutControlItem12.Location = new System.Drawing.Point(498, 110);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(423, 394);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 546);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(921, 24);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(498, 504);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(211, 42);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnAddDefect;
            this.layoutControlItem7.Location = new System.Drawing.Point(709, 504);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(212, 42);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // ProductDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 632);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ProductDetailForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Toote detailinfo";
            this.Load += new System.EventHandler(this.ProductDetailForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditLaser1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericChartRangeControlClient2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceProductDefects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkListQualityClasses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.qualityClassBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl lblWidth;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnMeasureProfile;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.CheckedListBoxControl chkListQualityClasses;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private System.Windows.Forms.BindingSource qualityClassBindingSource;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.RangeControl rangeControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser1;
        private DevExpress.XtraEditors.NumericChartRangeControlClient numericChartRangeControlClient1;
        private DevExpress.XtraEditors.RangeControl rangeControl3;
        private DevExpress.XtraEditors.RangeControl rangeControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser3;
        private DevExpress.XtraEditors.NumericChartRangeControlClient numericChartRangeControlClient3;
        private DevExpress.XtraEditors.NumericChartRangeControlClient numericChartRangeControlClient2;
        private DevExpress.XtraEditors.CheckEdit checkEditLaser3;
        private DevExpress.XtraEditors.CheckEdit checkEditLaser2;
        private DevExpress.XtraEditors.CheckEdit checkEditLaser1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.DropDownButton btnAddDefect;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.BindingSource bindingSourceProductDefects;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}