﻿using DevExpress.XtraEditors;
using LaserDetection.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class ProductDetailForm : Form
    {
        public Data.DataObjects.Product ProductObject { get; set; }

        public ProductDetailForm()
        {
            InitializeComponent();
        }

        private void InitProductData()
        {
            InitData();
        }

        private void InitData()
        {
            // laser profiles
            if (ProductObject.Version == null)
            {
                ProductObject.ProductLaserSettingList = new List<Data.DataObjects.ProductLaserSetting>();

                ProductObject.ProductLaserSettingList.Add(new Data.DataObjects.ProductLaserSetting()
                {
                    IsActive = true,
                    LaserID = Constants.Lasers.LaserLeft,
                    ProductID = ProductObject.ProductID,
                });
                ProductObject.ProductLaserSettingList.Add(new Data.DataObjects.ProductLaserSetting()
                {
                    IsActive = true,
                    LaserID = Constants.Lasers.LaserTop,
                    ProductID = ProductObject.ProductID,
                });
                ProductObject.ProductLaserSettingList.Add(new Data.DataObjects.ProductLaserSetting()
                {
                    IsActive = true,
                    LaserID = Constants.Lasers.LaserRight,
                    ProductID = ProductObject.ProductID,
                });
            }


            // quality classes
            var qualityClassList = Data.QualityClasses.GetList().OrderBy(a => a.Order);
            qualityClassBindingSource.DataSource = qualityClassList;


            // set items checked
            int itemIndex = 0;
            foreach (var item in qualityClassList)
            {
                if (ProductObject.QualityClassList.Count(a => a.QualityClassID == item.QualityClassID) > 0)
                    chkListQualityClasses.SetItemChecked(itemIndex, true);
                itemIndex++;
            }

            if(ProductObject != null)
            {
                txtName.Text = ProductObject.Name;
                txtCode.Text = ProductObject.Code;
                lblWidth.Text = string.Format("{0} mm", ProductObject.Width);
            }

            // laser names
            layoutControlItemLaser2.Text = Data.Lasers.GetName(Constants.Lasers.LaserLeft);
            layoutControlItemLaser1.Text = Data.Lasers.GetName(Constants.Lasers.LaserTop);
            layoutControlItemLaser3.Text = Data.Lasers.GetName(Constants.Lasers.LaserRight);

            // profiles
            foreach (var laserProductItem in ProductObject.ProductLaserSettingList)
            {
                if (laserProductItem.LaserID == Constants.Lasers.LaserLeft)
                    checkEditLaser2.Checked = laserProductItem.IsActive;
                if (laserProductItem.LaserID == Constants.Lasers.LaserTop)
                    checkEditLaser1.Checked = laserProductItem.IsActive;
                if (laserProductItem.LaserID == Constants.Lasers.LaserRight)
                    checkEditLaser3.Checked = laserProductItem.IsActive;

                SetProfileData(laserProductItem.LaserID, laserProductItem.ProfileDataInPointsZ, laserProductItem.ProfileDataInPointsX, laserProductItem.ProfileCheckStartPoint, laserProductItem.ProfileCheckEndPoint);
            }

            // create defect menu
            var activeDefectList = Data.Defects.GetList();
            foreach (var defectItem in activeDefectList)
            {
                DevExpress.XtraBars.BarButtonItem buttonItem = new DevExpress.XtraBars.BarButtonItem();
                buttonItem.Caption = defectItem.Name;
                buttonItem.ItemClick += ButtonItem_ItemClick;
                buttonItem.Tag = defectItem.DefectID;
                popupMenu1.AddItem(buttonItem);
            }


            LoadDefects(ProductObject.ProductID);
        }

        private void LoadDefects(Guid ProductID)
        {
            // init defect data
            var defectList = Data.ProductDefects.GetList(ProductID);

            bindingSourceProductDefects.DataSource = defectList;
        }

        private void ButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var defectID = (Guid)e.Item.Tag;

            Data.DataObjects.ProductDefect newDefect = new Data.DataObjects.ProductDefect();
            newDefect.DefectID = defectID;
            newDefect.ProductID = ProductObject.ProductID;
            newDefect.ProductDefectID = Guid.NewGuid();
            newDefect.ProductDefectSettingList = new List<Data.DataObjects.ProductDefectSetting>();

            DefectDetailForm newForm = new DefectDetailForm();
            newForm.ProductDefectObject = newDefect;

            if (newForm.ShowDialog() == DialogResult.OK)
            {
                Data.ProductDefects.Save(newDefect);
                LoadDefects(ProductObject.ProductID);
            }
        }

        private void ProductDetailForm_Load(object sender, EventArgs e)
        {
            InitProductData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(ProductObject != null)
            {
                ProductObject.Name = txtName.Text;
                ProductObject.Code = txtCode.Text;

                // get check list
                List<Data.DataObjects.QualityClass> checkItemList = new List<Data.DataObjects.QualityClass>();
                foreach (var checkItem in chkListQualityClasses.CheckedItems)
                {
                    Data.DataObjects.QualityClass qualityClassItem = checkItem as Data.DataObjects.QualityClass;
                    checkItemList.Add(qualityClassItem);
                }

                // add new
                foreach (var newItem in checkItemList)
                {
                    var checkedItem = ProductObject.QualityClassList.Where(a => a.QualityClassID == newItem.QualityClassID).FirstOrDefault();
                    if (checkedItem == null)
                    {
                        ProductObject.QualityClassList.Add(new Data.DataObjects.QualityClass() { QualityClassID = newItem.QualityClassID, Checked = true, Name = newItem.Name});
                    }
                }

                foreach (var qualityItem in ProductObject.QualityClassList)
                {
                    // is already checked
                    var checkedItem = checkItemList.Where(a => a.QualityClassID == qualityItem.QualityClassID).FirstOrDefault();
                    if (checkedItem != null)
                    {
                        qualityItem.Checked = true;
                        continue;
                    }

                    // not checked
                    qualityItem.Checked = false;
                }

                //foreach (var productLaserSettingList in ProductObject.ProductLaserSettingList)
                //{
                //    if(productLaserSettingList.LaserID == comm)
                //    productLaserSettingList.
                //}
            }
        }

        private void btnMeasureProfile_Click(object sender, EventArgs e)
        {
            // measure only active ones
            foreach (var laserProductItem in ProductObject.ProductLaserSettingList.Where(a => a.IsActive))
            {
                Laser.SingleLaserClass laserClass = new Laser.SingleLaserClass();

                Data.DataObjects.Laser laser = Data.Lasers.Get(laserProductItem.LaserID);

                double[] zValues = new double[0];
                double[] xValues = new double[0];
                ushort[] wValues = new ushort[0];

                if (laserClass.Connect(laser.IPAddress))
                {
                    laserClass.GetProfiles(ref zValues, ref xValues, ref wValues);
                }
                
                laserClass.Disconnect();

                // update object
                laserProductItem.ProfileDataInPointsZ = zValues;
                laserProductItem.ProfileDataInPointsX = xValues;

                // update UI
                SetProfileData(laserProductItem.LaserID, zValues, xValues, laserProductItem.ProfileCheckStartPoint, laserProductItem.ProfileCheckEndPoint);
            }

            // measure
            var width = MeasureProductWidth(Constants.Lasers.LaserTop);
            lblWidth.Text = string.Format("{0} mm", width);
            ProductObject.Width = width;
        }

        private void SetProfileData(Guid LaserID, double[] zPoints, double[] xPoints, double? profileCheckStartPoint, double? profileCheckEndPoint)
        {
            if (zPoints == null || xPoints == null)
                return;

            RangeControl rangeControl = null;
            NumericChartRangeControlClient controlClient = null;
            if (LaserID == Constants.Lasers.LaserLeft)
            {
                controlClient = numericChartRangeControlClient2;
                rangeControl = rangeControl2;
            }
            if (LaserID == Constants.Lasers.LaserTop)
            {
                controlClient = numericChartRangeControlClient1;
                rangeControl = rangeControl1;

            }
            if (LaserID == Constants.Lasers.LaserRight)
            {
                controlClient = numericChartRangeControlClient3;
                rangeControl = rangeControl3;
            }

            List<NumericItem> data = new List<NumericItem>();

            for (int i = 0; i < zPoints.Count(); i++)
            {
                var xValue = xPoints[i];
                var zValue = zPoints[i];

                if (zValue == 0)
                    continue;

                data.Add(new NumericItem() { Argument = xValue, Series = 1, Value = zValue });
            }

            controlClient.DataProvider.DataSource = data;

            controlClient.DataProvider.ArgumentDataMember = "Argument";
            controlClient.DataProvider.ValueDataMember = "Value";
            controlClient.DataProvider.SeriesDataMember = "Series";


            // Specify the chart range control client view.
            AreaChartRangeControlClientView areaView = new AreaChartRangeControlClientView();
            controlClient.DataProvider.TemplateView = areaView;

            // Customize the area view appearance. 
            areaView.AreaOpacity = 90;
            areaView.Color = Color.Gray;
            areaView.ShowMarkers = true;
            areaView.MarkerSize = 2;
            areaView.MarkerColor = Color.Red;

            // Specify the palette name to get a nice-looking chart.
            //controlClient.PaletteName = "NatureColors";

            // Change the default range of the numeric chart range control client.          
            //numericChartRangeControlClient1.Range.Min = 4;
            //numericChartRangeControlClient1.Range.Max = 12;

            // Customize the grid options of the numeric chart range control client.          
            controlClient.GridOptions.GridSpacing = 1;
            controlClient.GridOptions.SnapSpacing = 0.1;


            
            if (profileCheckStartPoint.HasValue)
                rangeControl.SelectedRange.Minimum = profileCheckStartPoint.Value;

            if (profileCheckEndPoint.HasValue)
                rangeControl.SelectedRange.Maximum = profileCheckEndPoint.Value;
        }

        private double MeasureProductWidth(Guid LaserID)
        {
            var settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == LaserID).FirstOrDefault();
            if (settingObject != null)
            {
                if (settingObject.ProfileDataInPointsX == null || settingObject.ProfileDataInPointsX.Where(a => a > 0).Count() == 0)
                    return 0;

                double width = Math.Abs(settingObject.ProfileDataInPointsX.Where(a => a > 0).Max() + settingObject.ProfileDataInPointsX.Where(a => a > 0).Min());
                return width;
            }

            return 0;
        }

        public class NumericItem
        {
            public double Argument { get; set; }
            public double Value { get; set; }
            public double Series { get; set; }
        }

        private void checkEditLaser1_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkEditLaser1.Focused)
                return;

            var settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserTop).FirstOrDefault();
            if (settingObject != null)
                settingObject.IsActive = checkEditLaser1.Checked;
        }

        private void checkEditLaser2_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkEditLaser2.Focused)
                return;

            var settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserLeft).FirstOrDefault();
            if (settingObject != null)
                settingObject.IsActive = checkEditLaser2.Checked;
        }

        private void checkEditLaser3_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkEditLaser3.Focused)
                return;

            var settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserRight).FirstOrDefault();
            if (settingObject != null)
                settingObject.IsActive = checkEditLaser3.Checked;
        }

        private void rangeControl2_RangeChanged(object sender, RangeControlRangeEventArgs range)
        {
            RangeControl rangeControl = sender as RangeControl;

            if (!rangeControl.Focused)
                return;

            var minValue = (double)range.Range.Minimum;
            var maxValue = (double)range.Range.Maximum;

            Data.DataObjects.ProductLaserSetting settingObject = null;
            if (rangeControl == rangeControl1)
                settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserTop).FirstOrDefault();
            if (rangeControl == rangeControl2)
                settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserRight).FirstOrDefault();
            if (rangeControl == rangeControl3)
                settingObject = ProductObject.ProductLaserSettingList.Where(a => a.LaserID == Constants.Lasers.LaserRight).FirstOrDefault();

            if (settingObject != null)
            {
                settingObject.ProfileCheckStartPoint = minValue;
                settingObject.ProfileCheckEndPoint = maxValue;
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            // open detail

            var dataRow = gridView1.GetFocusedRow() as Data.DataObjects.ProductDefect;

            if (dataRow != null)
            {
                DefectDetailForm newForm = new DefectDetailForm();
                newForm.ProductDefectObject = dataRow;

                if (newForm.ShowDialog() == DialogResult.OK)
                {
                    Data.ProductDefects.Save(dataRow);
                    LoadDefects(ProductObject.ProductID);
                }
            }




        }
    }
}
