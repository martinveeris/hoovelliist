﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class ProductListForm : Form
    {
        public ProductListForm()
        {
            InitializeComponent();

            InitData();
        }

        private void InitData()
        {
            productBindingSource.DataSource = Data.Products.GetList();
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            Data.DataObjects.Product newProduct = new Data.DataObjects.Product();
            newProduct.ProductID = Guid.NewGuid();
            newProduct.QualityClassList = new List<Data.DataObjects.QualityClass>();

            ProductDetailForm newForm = new ProductDetailForm();
            newForm.ProductObject = newProduct;
            if(newForm.ShowDialog() == DialogResult.OK)
            {
                Data.Products.Save(newProduct);
                InitData();
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var dataRow = gridView1.GetFocusedRow() as Data.DataObjects.Product;

            if (dataRow != null)
            {
                ProductDetailForm newForm = new ProductDetailForm();
                newForm.ProductObject = dataRow;
                if (newForm.ShowDialog() == DialogResult.OK)
                {
                    Data.Products.Save(dataRow);
                    InitData();
                }
            }
        }
    }
}