﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class QualityClassesListForm : Form
    {
        public QualityClassesListForm()
        {
            InitializeComponent();

            InitData();
        }

        private void InitData()
        {
            qualityClassBindingSource.DataSource = Data.QualityClasses.GetList();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var qualityRow = gridView1.GetFocusedRow() as Data.DataObjects.QualityClass;

            if (qualityRow != null)
            {
                QualityClassDetailForm newForm = new QualityClassDetailForm();
                newForm.QualityClassObject = qualityRow;
                if (newForm.ShowDialog() == DialogResult.OK)
                {
                    Data.QualityClasses.Save(qualityRow);
                }
            }
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            Data.DataObjects.QualityClass qualityRow = new Data.DataObjects.QualityClass();
            qualityRow.QualityClassID = Guid.NewGuid();
            qualityRow.SignalOutputISO = 0;

            QualityClassDetailForm newForm = new QualityClassDetailForm();
            newForm.QualityClassObject = qualityRow;
            if (newForm.ShowDialog() == DialogResult.OK)
            {
                Data.QualityClasses.Save(qualityRow);
                InitData();
            }
        }


        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var qualityRow = gridView1.GetFocusedRow() as Data.DataObjects.QualityClass;

            if (qualityRow != null)
            {
                if(MessageBox.Show("Soovite kustutada?", "Kustuta", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Data.QualityClasses.Delete(qualityRow);
                    InitData();
                }
            }
        }
    }
}