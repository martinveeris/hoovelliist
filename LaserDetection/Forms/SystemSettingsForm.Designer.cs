﻿namespace LaserDetection.Forms
{
    partial class SystemSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemSettingsForm));
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.chkSaveStatistics = new DevExpress.XtraEditors.CheckEdit();
            this.btnOpenFolderStatistics = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatistics = new DevExpress.XtraEditors.TextEdit();
            this.chkProductWarning = new DevExpress.XtraEditors.CheckEdit();
            this.btnOpenFolderProfiles = new DevExpress.XtraEditors.SimpleButton();
            this.txtProfiles = new DevExpress.XtraEditors.TextEdit();
            this.chkProfiles = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.chkSortSignal = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSaveStatistics.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatistics.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProductWarning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfiles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfiles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSortSignal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chkSortSignal);
            this.layoutControl1.Controls.Add(this.btnSave);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.chkSaveStatistics);
            this.layoutControl1.Controls.Add(this.btnOpenFolderStatistics);
            this.layoutControl1.Controls.Add(this.txtStatistics);
            this.layoutControl1.Controls.Add(this.chkProductWarning);
            this.layoutControl1.Controls.Add(this.btnOpenFolderProfiles);
            this.layoutControl1.Controls.Add(this.txtProfiles);
            this.layoutControl1.Controls.Add(this.chkProfiles);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(530, 253);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(255, 219);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(126, 22);
            this.btnSave.StyleController = this.layoutControl1;
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Salvesta";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(385, 219);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(133, 22);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Sulge";
            // 
            // chkSaveStatistics
            // 
            this.chkSaveStatistics.Location = new System.Drawing.Point(12, 123);
            this.chkSaveStatistics.Name = "chkSaveStatistics";
            this.chkSaveStatistics.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkSaveStatistics.Properties.Appearance.Options.UseFont = true;
            this.chkSaveStatistics.Properties.Caption = "Salvesta mõõtmise statistika";
            this.chkSaveStatistics.Size = new System.Drawing.Size(506, 20);
            this.chkSaveStatistics.StyleController = this.layoutControl1;
            this.chkSaveStatistics.TabIndex = 10;
            // 
            // btnOpenFolderStatistics
            // 
            this.btnOpenFolderStatistics.Location = new System.Drawing.Point(385, 147);
            this.btnOpenFolderStatistics.Name = "btnOpenFolderStatistics";
            this.btnOpenFolderStatistics.Size = new System.Drawing.Size(133, 22);
            this.btnOpenFolderStatistics.StyleController = this.layoutControl1;
            this.btnOpenFolderStatistics.TabIndex = 9;
            this.btnOpenFolderStatistics.Text = "Vali kataloog";
            this.btnOpenFolderStatistics.Click += new System.EventHandler(this.btnOpenFolderStatistics_Click);
            // 
            // txtStatistics
            // 
            this.txtStatistics.Location = new System.Drawing.Point(172, 147);
            this.txtStatistics.Name = "txtStatistics";
            this.txtStatistics.Properties.ReadOnly = true;
            this.txtStatistics.Size = new System.Drawing.Size(209, 20);
            this.txtStatistics.StyleController = this.layoutControl1;
            this.txtStatistics.TabIndex = 8;
            // 
            // chkProductWarning
            // 
            this.chkProductWarning.Location = new System.Drawing.Point(12, 86);
            this.chkProductWarning.Name = "chkProductWarning";
            this.chkProductWarning.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkProductWarning.Properties.Appearance.Options.UseFont = true;
            this.chkProductWarning.Properties.Caption = "Näita hoiatust kui valitud toode ei vasta kontrollitavale";
            this.chkProductWarning.Size = new System.Drawing.Size(506, 20);
            this.chkProductWarning.StyleController = this.layoutControl1;
            this.chkProductWarning.TabIndex = 7;
            // 
            // btnOpenFolderProfiles
            // 
            this.btnOpenFolderProfiles.Location = new System.Drawing.Point(385, 48);
            this.btnOpenFolderProfiles.Name = "btnOpenFolderProfiles";
            this.btnOpenFolderProfiles.Size = new System.Drawing.Size(133, 22);
            this.btnOpenFolderProfiles.StyleController = this.layoutControl1;
            this.btnOpenFolderProfiles.TabIndex = 6;
            this.btnOpenFolderProfiles.Text = "Vali kataloog";
            this.btnOpenFolderProfiles.Click += new System.EventHandler(this.btnOpenFolderProfiles_Click);
            // 
            // txtProfiles
            // 
            this.txtProfiles.Location = new System.Drawing.Point(12, 48);
            this.txtProfiles.Name = "txtProfiles";
            this.txtProfiles.Properties.ReadOnly = true;
            this.txtProfiles.Size = new System.Drawing.Size(369, 20);
            this.txtProfiles.StyleController = this.layoutControl1;
            this.txtProfiles.TabIndex = 5;
            // 
            // chkProfiles
            // 
            this.chkProfiles.Location = new System.Drawing.Point(12, 12);
            this.chkProfiles.Name = "chkProfiles";
            this.chkProfiles.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkProfiles.Properties.Appearance.Options.UseFont = true;
            this.chkProfiles.Properties.Caption = "Salvesta profiilid hilisemaks kontrolliks";
            this.chkProfiles.Size = new System.Drawing.Size(506, 20);
            this.chkProfiles.StyleController = this.layoutControl1;
            this.chkProfiles.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem5,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(530, 253);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chkProfiles;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(510, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 184);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(510, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtProfiles;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(373, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnOpenFolderProfiles;
            this.layoutControlItem3.Location = new System.Drawing.Point(373, 36);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chkProductWarning;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(510, 24);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtStatistics;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 135);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(373, 26);
            this.layoutControlItem5.Text = "Mõõtmise statistika asukoht";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(157, 16);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnOpenFolderStatistics;
            this.layoutControlItem6.Location = new System.Drawing.Point(373, 135);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(510, 12);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 62);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(510, 12);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.chkSaveStatistics;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 111);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(510, 24);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(510, 13);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnCancel;
            this.layoutControlItem8.Location = new System.Drawing.Point(373, 207);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnSave;
            this.layoutControlItem9.Location = new System.Drawing.Point(243, 207);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(130, 26);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 207);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(243, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // chkSortSignal
            // 
            this.chkSortSignal.Location = new System.Drawing.Point(12, 173);
            this.chkSortSignal.Name = "chkSortSignal";
            this.chkSortSignal.Properties.Caption = "Saada sorteerimissignaal";
            this.chkSortSignal.Size = new System.Drawing.Size(506, 19);
            this.chkSortSignal.StyleController = this.layoutControl1;
            this.chkSortSignal.TabIndex = 13;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.chkSortSignal;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(510, 23);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // SystemSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 253);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SystemSettingsForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Süsteemiseaded";
            this.Shown += new System.EventHandler(this.SystemSettingsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkSaveStatistics.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatistics.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProductWarning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProfiles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkProfiles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSortSignal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.CheckEdit chkSaveStatistics;
        private DevExpress.XtraEditors.SimpleButton btnOpenFolderStatistics;
        private DevExpress.XtraEditors.TextEdit txtStatistics;
        private DevExpress.XtraEditors.CheckEdit chkProductWarning;
        private DevExpress.XtraEditors.SimpleButton btnOpenFolderProfiles;
        private DevExpress.XtraEditors.TextEdit txtProfiles;
        private DevExpress.XtraEditors.CheckEdit chkProfiles;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.CheckEdit chkSortSignal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}