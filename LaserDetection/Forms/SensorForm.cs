﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class SensorForm : Form
    {
        public SensorForm()
        {
            InitializeComponent();
        }

        private void btnTestSignal_Click(object sender, EventArgs e)
        {
            if (!chkSinalDirection.Checked)
            {
                int lineNumber = 0;
                if (int.TryParse(txtLineNumber.Text, out lineNumber))
                {
                    Sensor.SensorReadClass sensorClass = new Sensor.SensorReadClass();
                    sensorClass.SetSensorValue(lineNumber);
                }
            }
            else
            {
                int lineNumber = 0;
                if (int.TryParse(txtLineNumber.Text, out lineNumber))
                {
                    Sensor.SensorReadClass sensorClass = new Sensor.SensorReadClass();
                    chkSignal.Checked = sensorClass.GetSensorValue(lineNumber);
                }
            }

        }
    }
}