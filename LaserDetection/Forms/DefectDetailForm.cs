﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class DefectDetailForm : Form
    {
        public Data.DataObjects.ProductDefect ProductDefectObject { get; set; }

        public DefectDetailForm()
        {
            InitializeComponent();
        }

        private void InitData()
        {
            // quality classes  (filter only allowedones for this product)
            var qualityClassList = Data.QualityClasses.GetList();
            bindingSourceQualityClass.DataSource = qualityClassList;

            // defect parameters
            var defectParameters = Data.DefectParameters.GetList();

            if(ProductDefectObject != null)
            {
                var defect = Data.Defects.Get(ProductDefectObject.DefectID);
                lblDefect.Text = defect.Name;

                // add new
                if(ProductDefectObject.Version == null)
                {
                    ProductDefectObject.QualityClassID = Common.Constants.QualityClasses.Correct;

                    foreach (var parameterItem in defectParameters)
                    {
                        // check if this parameter is allowed for this defect
                        if (!CheckIfDataRowIsVisible(ProductDefectObject.DefectID, parameterItem.DefectParameterID))
                            continue;

                        ProductDefectObject.ProductDefectSettingList.Add(new Data.DataObjects.ProductDefectSetting()
                        {
                            DefectParameterID = parameterItem.DefectParameterID,
                            ParameterName = parameterItem.Name,
                            IsActive = true,
                            ProductDefectSettingID = Guid.NewGuid(),
                            ProductDefectID = ProductDefectObject.ProductDefectID
                        });
                    }
                }

                bindingSourceDefectParams.DataSource = ProductDefectObject.ProductDefectSettingList;

                lookUpEditQualityClass.EditValue = ProductDefectObject.QualityClassID;
            }
        }

        private void DefectDetailForm_Load(object sender, EventArgs e)
        {
            InitData();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ProductDefectObject.QualityClassID = (Guid)lookUpEditQualityClass.EditValue;
            ProductDefectObject.QualityClassName = Data.QualityClasses.Get(ProductDefectObject.QualityClassID).Name;
        }

        private void SetParameterVisibility(Guid DefectID, Guid DefectParameterID)
        {

        }

        private bool CheckIfDataRowIsVisible(Guid DefectID, Guid DefectParameterID)
        {
            if(DefectID == Common.Constants.Defects.Hole)
            {
                if (DefectParameterID == Common.Constants.DefectParameters.Area)
                    return true;
                //else if (DefectParameterID == Common.Constants.DefectParameters.Depth)
                //    return true;
                else if (DefectParameterID == Common.Constants.DefectParameters.Count)
                    return true;
                else
                    return false;
            }
            if (DefectID == Common.Constants.Defects.Crack)
            {
                if (DefectParameterID == Common.Constants.DefectParameters.Length)
                    return true;
                else if (DefectParameterID == Common.Constants.DefectParameters.Count)
                    return true;
                else
                    return false;
            }
            if (DefectID == Common.Constants.Defects.Peak)
            {
                if (DefectParameterID == Common.Constants.DefectParameters.Area)
                    return true;
                else if (DefectParameterID == Common.Constants.DefectParameters.Count)
                    return true;
                else
                    return false;
            }
            return false;
        }
    }
}