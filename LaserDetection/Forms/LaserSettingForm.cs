﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Forms
{
    public partial class LaserSettingForm : Form
    {
        public LaserSettingForm()
        {
            InitializeComponent();

            InitData();
        }

        private void InitData()
        {
           laserBindingSource.DataSource = Data.Lasers.GetList();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var laserRow = gridView1.GetFocusedRow() as Data.DataObjects.Laser;

            if(laserRow != null)
            {
                LaserDetailForm newForm = new LaserDetailForm();
                newForm.LaserObject = laserRow;
                if (newForm.ShowDialog() == DialogResult.OK)
                {
                    Data.Lasers.Save(laserRow);
                }
            }



        }
    }
}
