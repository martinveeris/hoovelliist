﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Laser
{
    public class LaserDataClass
    {
        public enum ProfileDataStatusEnum
        {
            Start, End, Mid, Empty, Zero
        }


        public ProfileDataStatusEnum ProfileDataStatus { get; set; }


        public Data.DataObjects.ProductLaserSetting ProductLaserSetting { get; set; }

        public List<LaserDataRowClass> dataList = new List<LaserDataRowClass>();


        public Matrix<double> zDataMatrix;
        public Matrix<double> xDataMatrix;
        public Matrix<ushort> wDataMatrix;
        public Matrix<double> iDataMatrix;


        public LaserDataClass(Data.DataObjects.ProductLaserSetting productLaserSetting)
        {
            // init as Empty
            ProfileDataStatus = ProfileDataStatusEnum.Zero;

            ProductLaserSetting = productLaserSetting;

           
        }

        public void AddDataRow(double[] adValueZ, double[] adValueX, ushort[] adValueW)
        {
            // is contains data
            if (adValueZ.Max() > 0)
            {
                //dataList.Add(new LaserDataRowClass() { AdValueZ = adValueZ, AdValueX = adValueX, AdValueW = adValueW });

                if(zDataMatrix == null)
                {
                    zDataMatrix = new Matrix<double>(adValueZ.Length, 1);
                    xDataMatrix = new Matrix<double>(adValueZ.Length, 1);
                    wDataMatrix = new Matrix<ushort>(adValueZ.Length, 1);
                    iDataMatrix = new Matrix<double>(adValueZ.Length, 1);

                    // lisa juurde
                    zDataMatrix = zDataMatrix.ConcateHorizontal(new Matrix<double>(adValueZ));
                    xDataMatrix = xDataMatrix.ConcateHorizontal(new Matrix<double>(adValueX));
                    wDataMatrix = wDataMatrix.ConcateHorizontal(new Matrix<ushort>(adValueW));
                    iDataMatrix = iDataMatrix.ConcateHorizontal(new Matrix<double>(adValueZ));

                    zDataMatrix = zDataMatrix.RemoveCols(0, 1);
                    xDataMatrix = xDataMatrix.RemoveCols(0, 1);
                    wDataMatrix = wDataMatrix.RemoveCols(0, 1);
                    iDataMatrix = iDataMatrix.RemoveCols(0, 1);
                } else
                {
                    // lisa juurde
                    zDataMatrix = zDataMatrix.ConcateHorizontal(new Matrix<double>(adValueZ));
                    xDataMatrix = xDataMatrix.ConcateHorizontal(new Matrix<double>(adValueX));
                    wDataMatrix = wDataMatrix.ConcateHorizontal(new Matrix<ushort>(adValueW));
                    iDataMatrix = iDataMatrix.ConcateHorizontal(new Matrix<double>(adValueZ));
                }

               

                // kui oli enne tühi ja ühtegi ei ole lisatud, siis on keskkoht
                if (ProfileDataStatus == ProfileDataStatusEnum.Zero)
                    ProfileDataStatus = ProfileDataStatusEnum.Mid;
                // kui enne oli Empty, siis nüüs start 
                else if (ProfileDataStatus == ProfileDataStatusEnum.Empty)
                    ProfileDataStatus = ProfileDataStatusEnum.Start;

                // Mid-i üle ei ki
            }
            else
            {
                // kui enne oli mis, siis nüüd on end
                if (ProfileDataStatus == ProfileDataStatusEnum.Mid)
                    ProfileDataStatus = ProfileDataStatusEnum.End;
                else if (ProfileDataStatus == ProfileDataStatusEnum.Zero)
                    ProfileDataStatus = ProfileDataStatusEnum.Empty;
            }
        }

        /// <summary>
        /// Process data
        /// </summary>
        /// <returns></returns>
        public Task PostProcessAsync()
        {
            return Task.Run(() =>
            {
                Process();
            });
        }

        private void Process()
        {
            //// TODO: Execute parallel
            //Parallel.ForEach(dataList, dataItem =>
            //{

            //    // lõika ääred
            //    //Stopwatch sw = new Stopwatch();
            //    //sw.Start();

            //    // eemalda W ja 
            //    dataItem.CutDataByZValues();
            //    //dataItem.CutData(ProductLaserSetting);

            //    // too defektid esile
            //    ShowDefects(dataItem);
            //    //sw.Stop();
            //});


            // TODO: Execute parallel

            // lõika ääred
            //Stopwatch sw = new Stopwatch();
            //sw.Start();

            // eemalda W ja 
            //dataItem.CutDataByZValues();
            //dataItem.CutData(ProductLaserSetting);

            // too defektid esile

            ShowDefects();
            
            //sw.Stop();
            


        }

        private void ShowDefects(LaserDataRowClass dataClass)
        {
            // itereeri üle profiilide, arvuta joon, joone erisus
            // joone erisus kahe anmdme pealt ? Z, W?
            // see erisus ongi siis pildi andmestik

            double[] curvData = null;
            var curve = Utils.MathUtils.GetFittedCurve(dataClass.AdValueX, dataClass.AdValueZ, 19, out curvData);
            var diff = Utils.MathUtils.CalculateDifference(curve, dataClass.AdValueZ, 0.1, true);
            dataClass.ImageData = diff;
        }

        private void ShowDefects()
        {
            // itereeri üle profiilide, arvuta joon, joone erisus
            // joone erisus kahe anmdme pealt ? Z, W?
            // see erisus ongi siis pildi andmestik

            if (zDataMatrix == null)
                return;

           
            // leiti mitte 0 väärtusega ridade arv

           

            // tähtis on, et välja ei jookseks, seega itereeri üle selle

            // heledus juurde
            iDataMatrix = new Matrix<double>(xDataMatrix.Rows, xDataMatrix.Cols);

            //Parallel.For(0, xDataMatrix.Cols, j =>
            for (int j = 0; j < xDataMatrix.Cols; j++)
            {
                int validRowCount = 0;
                for (int i = 0; i < zDataMatrix.Rows; i++)
                {
                    // otsi 
                    if (zDataMatrix[i, j] > 0)
                    {
                        validRowCount++;
                    }
                }

                // selle sisse mahuta edaspidi kogu data
                double[] curveInputxData = new double[validRowCount];
                double[] curveInputzData = new double[validRowCount];

                int addedCount = 0;
                for (int i = 0; i < zDataMatrix.Rows; i++)
                {
                    // lisa mitte 0 väärtused vastvasse listi
                    if (zDataMatrix[i, j] > 0 && addedCount < validRowCount)
                    {
                        curveInputxData[addedCount] = xDataMatrix[i, j];
                        curveInputzData[addedCount] = zDataMatrix[i, j];
                        addedCount++;
                    } else
                    {

                    }
                }


                // arvuta ainult reaalsete väärtustega andmetele (et nullid välja ei tuleks)
                if(validRowCount == addedCount)
                {
                    double[] curvData = null;
                    var curve = Utils.MathUtils.GetFittedCurve(curveInputxData, curveInputzData, 12, out curvData);

                    var diff = Utils.MathUtils.CalculateDifference(curveInputzData, curve, 0.15, true);

                    for (int i = 0; i < diff.Length; i++)
                    {
                        iDataMatrix[i, j] = diff[i];
                    }
                }

                //iDataMatrix[xDataMatrix.Rows-5, j] = 100;
            }
        }


        // TODO: Eemalda puktid mis jäävad välja,
        // testimisel selgub, et kas koordinaate peab ka ümber arvutama, et andmeid joondada

        // 1. Otsi joone peal punktid mis jäävad üles ja mis jäävad alla
        // igale z väärtusele arvutab sisse uue väärtuse joone ja selle kalde pealt, siis tekib
        // kalibreeritud data kus + on kühm ja - on auk.
        // 

        // kõvera ja silge kontrolliks on erinev metoodika

        public class LaserDataRowClass
        {
            public double[] ImageData { get; set; }

            public double[] AdValueZ
            {
                get
                {
                    return adValueZ;
                }
                set
                {
                    // init image data 
                    ImageData = new double[value.Length];

                    adValueZ = new double[value.Length];
                    Array.Copy(value, adValueZ, value.Length);
                }
            }

            public ushort[] AdValueW
            {
                get
                {
                    return adValueW;
                }
                set
                {
                    adValueW = new ushort[value.Length];
                    Array.Copy(value, adValueW, value.Length);
                }
            }

            public double[] AdValueX
            {
                get
                {
                    return adValueX;
                }
                set
                {
                    adValueX = new double[value.Length];
                    Array.Copy(value, adValueX, value.Length);
                }
            }

            private double[] adValueZ;
            private double[] adValueX;
            private ushort[] adValueW;

            /// <summary>
            /// Leaves only check area data
            /// </summary>
            /// <param name="productLaserSetting"></param>
            public void CutData(Data.DataObjects.ProductLaserSetting productLaserSetting)
            {
                // otsi esimene väärtus (vasak serv)
                var indexFirstValue = this.adValueZ.FindIndex(a => a != 0);

                // küsi selle väärtus
                var firstValue = this.adValueX[indexFirstValue];

                // arvuta juurde nihe paremale
                var leftValueWithOffset = firstValue + productLaserSetting.ProfileValues.OffsetMM;

                // arvuta juurde nihe ja laius (et saada lõpp)
                var leftValueWithOffsetAndWidth = firstValue + productLaserSetting.ProfileValues.OffsetMM+ productLaserSetting.ProfileValues.CheckAreaWidthMM;

                // leia lõpu index
                var indStart = this.adValueX.FindIndex(s => s >= leftValueWithOffset - 0.1 && s <= leftValueWithOffset + 0.1);
                var indEnd = this.adValueX.FindIndex(s => s >= leftValueWithOffsetAndWidth - 0.1 && s <= leftValueWithOffsetAndWidth + 0.1);
                if (indEnd < 0)
                    indEnd = adValueX.Length - 1;

                // ideaali erinevus jooksva väärtusega (alguspunkt MM)
                for (int i = 0; i < adValueX.Count(); i++)
                {
                    // kontrolli kõiki väärtuseid ja säti välised tühjaks
                    if (i > indStart && i < indEnd)
                        continue;

                    // võta väärtused maha
                    adValueZ[i] = 0;
                    adValueW[i] = 0;
                }
            }

            /// <summary>
            /// Remove data that has no corresponding Z value
            /// </summary>
            public void CutDataByZValues()
            {
                int neededCount = 0;
                for (int i = 0; i < adValueZ.Length; i++)
                {
                    if (adValueZ[i] > 0)
                        neededCount++;
                }

                double[] adValueXTemp = new double[neededCount];
                ushort[] adValueWTemp = new ushort[neededCount];
                double[] adValueZTemp = new double[neededCount];
                double[] ImageDataTemp = new double[neededCount];

                int addedCount = 0;
                for (int i = 0; i < adValueZ.Length; i++)
                {
                    if (adValueZ[i] > 0)
                    {
                        adValueXTemp[addedCount] = adValueX[i];
                        adValueWTemp[addedCount] = adValueW[i];
                        adValueZTemp[addedCount] = adValueZ[i];

                        addedCount++;
                    }
                }

                adValueX = adValueXTemp;
                adValueW = adValueWTemp;
                adValueZ = adValueZTemp;
            }

            public void CalculateLine()
            {
                var value = MathNet.Numerics.LinearRegression.SimpleRegression.Fit(adValueX, adValueZ);
                var goodness = MathNet.Numerics.GoodnessOfFit.RSquared(adValueX.Select(x => value.Item1 + value.Item2 * x), adValueZ); // 1 is good
            }


        }
    }
}