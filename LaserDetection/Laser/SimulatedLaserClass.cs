﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Laser
{
    public class SimulatedLaserClass
    {
        public static Data.DataObjects.ProductLaserSetting ProductLaserSetting1 { get; set; }


        public delegate void DataReceivedEventHandler(LaserDataClass dataClass);
        public static event DataReceivedEventHandler DataReceived;

        static StringBuilder sbAviFilename = new StringBuilder("LoadSaveSample.avi");

        public static void Disconnect()
        {

        }
        public Task ConnectAsync(List<Data.DataObjects.ProductLaserSetting> laserSettingList)
        {
            return Task.Run<bool>(() =>
            {
                return Connect(laserSettingList);
            });
        }

        public unsafe bool Connect(List<Data.DataObjects.ProductLaserSetting> laserSettingList)
        {
            foreach (var laserSetting in laserSettingList)
            {
                // calculate profile values for later use
                laserSetting.CalculateProfileValues();
                if (laserSetting.LaserID == Common.Constants.Lasers.LaserTop)
                    ProductLaserSetting1 = laserSetting;

            }

            CreateEvents();

            return true;
        }

        private void CreateEvents()
        {
            LaserDataClass dataClass = new LaserDataClass(ProductLaserSetting1);

            double[] adValueX = new double[ProductLaserSetting1.Resolution];
            double[] adValueZ = new double[ProductLaserSetting1.Resolution];
            ushort[] adValueW = new ushort[ProductLaserSetting1.Resolution];

            // saada tühi rida
                adValueX[0] = 0;
                adValueW[0] = 0;
                adValueZ[0] = 0;
            dataClass.AddDataRow(adValueZ, adValueX, adValueW);
            if (DataReceived != null)
            {
                DataReceived(dataClass);
            }
            adValueX = new double[ProductLaserSetting1.Resolution];
            adValueZ = new double[ProductLaserSetting1.Resolution];
            adValueW = new ushort[ProductLaserSetting1.Resolution];


            int counter = 0;
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file = new System.IO.StreamReader("TestProfiles/TopProfiilid.asc");
            while ((line = file.ReadLine()) != null)
            {
                if (counter == 325)
                {
                    //System.Threading.Thread.Sleep(1);

                    dataClass.AddDataRow(adValueZ, adValueX, adValueW);

                    adValueX = new double[ProductLaserSetting1.Resolution];
                    adValueZ = new double[ProductLaserSetting1.Resolution];
                    adValueW = new ushort[ProductLaserSetting1.Resolution];
                    counter = 0;

                    if(dataClass.xDataMatrix.Width == 100)
                    {
                        if (DataReceived != null)
                        {
                            DataReceived(dataClass);
                        }

                        dataClass = new LaserDataClass(ProductLaserSetting1);
                    }
                }

                var split = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                int pos = 0;
                foreach (var item in split)
                {
                    if (pos == 0)
                        adValueX[counter] = double.Parse(item.Replace(".", ","));
                    if (pos == 1)
                        adValueW[counter] = (ushort)double.Parse(item.Replace(".", ","));
                    if (pos == 2)
                        adValueZ[counter] = double.Parse(item.Replace(".", ","));

                    pos++;
                }

                counter++;

                // saada event andmetega

                //if (DataReceived != null)
                //{
                //    DataReceived(dataClass);
                //}


            }

            file.Close();


            if (DataReceived != null)
            {
                // tühi rida lõppu
                adValueX = new double[ProductLaserSetting1.Resolution];
                adValueZ = new double[ProductLaserSetting1.Resolution];
                adValueW = new ushort[ProductLaserSetting1.Resolution];

                adValueX[0] = 0;
                adValueW[0] = 0;
                adValueZ[0] = 0;
                dataClass.AddDataRow(adValueZ, adValueX, adValueW);

                DataReceived(dataClass);
            }
        }
    }
}
