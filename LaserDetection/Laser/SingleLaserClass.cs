﻿using CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Laser
{
    public class SingleLaserClass
    {
        public const int MAX_INTERFACE_COUNT = 5;
        public const int MAX_RESOULUTIONS = 6;
        public bool bConnected = false;

        static public uint m_uiResolution = 0;
        static public uint m_hLLT = 0;
        static public CLLTI.TScannerType m_tscanCONTROLType;

        static public uint m_uiRecivedProfileCount = 0;
        static public uint m_uiNeededProfileCount = 2;

        static public uint m_uiShutterTime = 100;
        static public uint m_uiIdleTime = 3900;

        public bool Connect(string ipAddress)
        {
            m_uiRecivedProfileCount = 0;
            uint[] auiInterfaces = new uint[MAX_INTERFACE_COUNT];
            uint[] auiResolutions = new uint[MAX_RESOULUTIONS];

            int iInterfaceCount = 0;
            int iRetValue;

            m_hLLT = 0;
            m_uiResolution = 0;

            uint uiBufferCount = 20, uiMainReflection = 0, uiPacketSize = 1024;


            //Create a Ethernet Device -> returns handle to LLT device
            m_hLLT = CLLTI.CreateLLTDevice(CLLTI.TInterfaceType.INTF_TYPE_ETHERNET);
            if (m_hLLT == 0)
                return false;

            //Gets the available interfaces from the scanCONTROL-device
            iInterfaceCount = CLLTI.GetDeviceInterfacesFast(m_hLLT, auiInterfaces, auiInterfaces.GetLength(0));
            if (iInterfaceCount <= 0)
                Console.WriteLine("There is no scanCONTROL connected");
            else if (iInterfaceCount == 1)
                Console.WriteLine("There is 1 scanCONTROL connected ");
            else
                Console.WriteLine("There are " + iInterfaceCount + " scanCONTROL's connected");

            // Set the first IP address detected by GetDeviceInterfacesFast to handle
            int interfaceIndex = GetInterfaceIndexByIP(ipAddress, auiInterfaces);
            if (interfaceIndex == -1)
                return false;

            if ((iRetValue = CLLTI.SetDeviceInterface(m_hLLT, auiInterfaces[interfaceIndex], 0))
                < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            // Connect to sensor with the device interface set before
            if ((iRetValue = CLLTI.Connect(m_hLLT)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }
            else
                bConnected = true;

            // Get the scanCONTROL type
            if ((iRetValue = CLLTI.GetLLTType(m_hLLT, ref m_tscanCONTROLType)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            // Get all possible resolutions for connected sensor and save them in array 
            if ((iRetValue = CLLTI.GetResolutions(m_hLLT, auiResolutions, auiResolutions.GetLength(0))) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            // Set the max. possible resolution
            m_uiResolution = auiResolutions[0];

            // Set scanner settings to valid parameters for this example
            if ((iRetValue = CLLTI.SetProfileConfig(m_hLLT, CLLTI.TProfileConfig.PROFILE)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            // set trigger to internal
            if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_TRIGGER, 0x00000000)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_SHUTTERTIME, m_uiShutterTime)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_IDLETIME, m_uiIdleTime)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                return false;
            }

            if ((iRetValue = CLLTI.SetResolution(m_hLLT, m_uiResolution)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during SetResolution", iRetValue);
                return false;
            }

            if ((iRetValue = CLLTI.SetBufferCount(m_hLLT, uiBufferCount)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during SetBufferCount", iRetValue);
                return false;
            }

            if ((iRetValue = CLLTI.SetMainReflection(m_hLLT, uiMainReflection)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during SetMainReflection", iRetValue);
                return false;
            }

            if ((iRetValue = CLLTI.SetPacketSize(m_hLLT, uiPacketSize)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during SetPacketSize", iRetValue);
                return false;
            }

            return true;
        }

        /*
         * Disconnect scanCONTROL and free ressources 
         */
        public void Disconnect()
        {
            int iRetValue = 0;
            if (bConnected)
            {
                // Disconnect from the sensor
                Console.WriteLine("Disconnect the scanCONTROL");
                if ((iRetValue = CLLTI.Disconnect(m_hLLT)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }

            if (bConnected)
            {
                // Free ressources
                Console.WriteLine("Delete the scanCONTROL instance");
                if ((iRetValue = CLLTI.DelDevice(m_hLLT)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }
        }

        public void GetProfiles(ref double[] adValueZ, ref double[] adValueX, ref ushort[] adValueW)
        {
            int iRetValue;
            uint uiLostProfiles = 0;
            adValueX = new double[m_uiResolution];
            adValueZ = new double[m_uiResolution];
            adValueW = new ushort[m_uiResolution];

            // Allocate profile buffer to the maximal profile size
            byte[] abyProfileBuffer = new byte[m_uiResolution * 64];
            byte[] abyTimestamp = new byte[16];

            // Allocate buffer for multiple profiles
            byte[] abyFullProfileBuffer = new byte[m_uiResolution * 64 * m_uiNeededProfileCount];

            Console.WriteLine("Demonstrate the profile transfer via poll function");

            // Start continous profile transmission
            Console.WriteLine("Enable the measurement");
            if ((iRetValue = CLLTI.TransferProfiles(m_hLLT, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 1)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during TransferProfiles", iRetValue);
            }

            //Sleep for a while to warm up the transfer
            System.Threading.Thread.Sleep(100);

            /*
             * This shows how to get multiple Profile in polling mode with PROFILE config, which means all data is evaluated.
             * To see how to get a single profiles with the PURE_PROFILE config, see the Firewire Poll example!	
             */
            while (m_uiRecivedProfileCount < m_uiNeededProfileCount)
            {
                // Get the next transmitted partial profile
                if ((iRetValue = CLLTI.GetActualProfile(m_hLLT, abyProfileBuffer, abyProfileBuffer.GetLength(0), CLLTI.TProfileConfig.PROFILE, ref uiLostProfiles))
                                                     != abyProfileBuffer.GetLength(0))
                {
                    OnError("Error during GetActualProfile", iRetValue);
                }
                Console.WriteLine("Get profile in polling-mode and PROFILE configuration OK");

                // Copy received buffer to buffer for multiple profiles
                Array.Copy(abyProfileBuffer, 0, abyFullProfileBuffer, m_uiRecivedProfileCount * m_uiResolution * 64, abyProfileBuffer.GetLength(0));

                // Sleep according to scanner frequency (for high frequencies it is recommended to use the callback example)
                System.Threading.Thread.Sleep((int)(m_uiShutterTime + m_uiIdleTime) / 100);
                m_uiRecivedProfileCount++;
            }

            // Convert partial profile to x and z values
            Console.WriteLine("Converting of profile data from the first reflection");
            iRetValue = CLLTI.ConvertProfile2Values(m_hLLT, abyProfileBuffer, m_uiResolution, CLLTI.TProfileConfig.PROFILE, m_tscanCONTROLType, 0, 1, adValueW, null, null, adValueX, adValueZ, null, null);
            if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0))
            {
                OnError("Error during Converting of profile data", iRetValue);
            }

            // Display x and z values
            //DisplayProfile(adValueX, adValueZ, m_uiResolution);

            Console.WriteLine("Display the timestamp from the profile:");

            // Extract the 16-byte timestamp from the profile buffer into timestamp buffer and display it
            //for (int i = 1; i < m_uiNeededProfileCount; i++)
            //{
            //    for (int iPos = 0; iPos < 16; iPos++)
            //    {
            //        abyTimestamp[iPos] = abyFullProfileBuffer[(i * m_uiResolution * 64 - 16) + iPos];
            //    }
            //   // DisplayTimestamp(abyTimestamp);
            //}

            // Stop continous profile transmission
            Console.WriteLine("Disable the measurement");
            if ((iRetValue = CLLTI.TransferProfiles(m_hLLT, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 0)) < CLLTI.GENERAL_FUNCTION_OK)
            {
                OnError("Error during TransferProfiles", iRetValue);
            }
        }

        // Display the X/Z-Data of one profile
        static void DisplayProfile(double[] adValueX, double[] adValueZ, uint uiResolution)
        {
            int iNumberSize = 0;
            for (uint i = 0; i < uiResolution; i++)
            {

                //Prints the X- and Z-values
                iNumberSize = adValueX[i].ToString().Length;
                Console.Write("\r" + "Profiledata: X = " + adValueX[i].ToString());

                for (; iNumberSize < 8; iNumberSize++)
                {
                    Console.Write(" ");
                }

                iNumberSize = adValueZ[i].ToString().Length;
                Console.Write(" Z = " + adValueZ[i].ToString());

                for (; iNumberSize < 8; iNumberSize++)
                {
                    Console.Write(" ");
                }

                //Somtimes wait a short time (only for display)
                if (i % 8 == 0)
                {
                    System.Threading.Thread.Sleep(10);
                }
            }
        }

        static void OnError(string strErrorTxt, int iErrorValue)
        {
            byte[] acErrorString = new byte[200];

            Console.WriteLine(strErrorTxt);
            if (CLLTI.TranslateErrorValue(m_hLLT, iErrorValue, acErrorString, acErrorString.GetLength(0))
                                            >= CLLTI.GENERAL_FUNCTION_OK)
                Console.WriteLine(System.Text.Encoding.ASCII.GetString(acErrorString, 0, acErrorString.GetLength(0)));
        }

        private int GetInterfaceIndexByIP(string ip, uint[] auiInterfaces)
        {
            int interfaceIndex = 0;
            foreach (var item in auiInterfaces)
            {
                uint target4 = item & 0x000000FF;
                uint target3 = (item & 0x0000FF00) >> 8;
                uint target2 = (item & 0x00FF0000) >> 16;
                uint target1 = (item & 0xFF000000) >> 24;

                if (ip == string.Format("{0}.{1}.{2}.{3}", target1, target2, target3, target4))
                    return interfaceIndex;

                        interfaceIndex++;
            }

            return -1;
        }
    }
}
