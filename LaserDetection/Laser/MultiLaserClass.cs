﻿using CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;

namespace LaserDetection.Laser
{
    public class MultiLaserClass
    {
        /* Global variables */
        public const int MAX_INTERFACE_COUNT = 5;
        public const int MAX_RESOULUTIONS = 6;
        public static bool bConnected = false;

        static public uint m_uiResolutionLaser1 = 0;
        static public uint m_uiResolutionLaser2 = 0;
        static public uint m_uiResolutionLaser3 = 0;

        static public uint m_hLLT_Laser1 = 0;
        static public uint m_hLLT_Laser2 = 0;
        static public uint m_hLLT_Laser3 = 0;

        static public CLLTI.TScannerType m_tscanCONTROLType_Laser1;
        static public CLLTI.TScannerType m_tscanCONTROLType_Laser2;
        static public CLLTI.TScannerType m_tscanCONTROLType_Laser3;


        static public uint m_uiReceivedProfileCount_Laser1 = 0;
        //static public uint m_uiNeededProfileCount = 10; // Profile count until event is set
        //static public uint m_uiProfileDataSize = 0;

        static public uint m_uiShutterTime = 100; // 0,35 ms
        static public uint m_uiIdleTime = 900; // see siis 100 HZ

        //static uint uiProfileCount = 20;   // Number of profiles in one container


        public static bool stopValue = false;

        // Define an array with two AutoResetEvent WaitHandles. 
        static AutoResetEvent m_hProfileEvent = new AutoResetEvent(false);



        public delegate void DataReceivedEventHandler(LaserDataClass dataClass);
        public static event DataReceivedEventHandler DataReceived;

        static public byte[] m_cProfileData_Laser1;
        static public byte[] r_cProfileData_Laser1;

        static public byte[] m_cProfileData_Laser2;
        static public byte[] r_cProfileData_Laser2;

        static public byte[] m_cProfileData_Laser3;
        static public byte[] r_cProfileData_Laser3;

        static byte[] abyContainerBufferCopyLaser1;
        static byte[] abyContainerBufferCopyLaser2;
        static byte[] abyContainerBufferCopyLaser3;

        static public uint m_bProfileReceived_Laser1 = 0;
        static public uint m_bProfileReceived_Laser2 = 0;
        static public uint m_bProfileReceived_Laser3 = 0;

        static public bool profileReceived_Laser1 = false;
        static public bool profileReceived_Laser2 = false;
        static public bool profileReceived_Laser3 = false;


        static public uint m_uiNeededProfileCount_Laser = 30;

        public static Data.DataObjects.ProductLaserSetting ProductLaserSetting1 { get; set; }
        public static Data.DataObjects.ProductLaserSetting ProductLaserSetting2 { get; set; }
        public static Data.DataObjects.ProductLaserSetting ProductLaserSetting3 { get; set; }

        public static void Disconnect()
        {
           // fnProfileReceiveMethod = null;

            int iRetValue = 0;
            if (m_hLLT_Laser1 != 0)
            {
                // Disconnect from the sensor
                Console.WriteLine("Disconnect the scanCONTROL");
                if ((iRetValue = CLLTI.Disconnect(m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }

            if (m_hLLT_Laser2 != 0)
            {
                // Disconnect from the sensor
                Console.WriteLine("Disconnect the scanCONTROL");
                if ((iRetValue = CLLTI.Disconnect(m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }

            if (m_hLLT_Laser3 != 0)
            {
                // Disconnect from the sensor
                Console.WriteLine("Disconnect the scanCONTROL");
                if ((iRetValue = CLLTI.Disconnect(m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }

            if (m_hLLT_Laser1 != 0)
            {
                // Free ressources
                Console.WriteLine("Delete the scanCONTROL instance");
                if ((iRetValue = CLLTI.DelDevice(m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }
            if (m_hLLT_Laser2 != 0)
            {
                // Free ressources
                Console.WriteLine("Delete the scanCONTROL instance");
                if ((iRetValue = CLLTI.DelDevice(m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }
            if (m_hLLT_Laser3 != 0)
            {
                // Free ressources
                Console.WriteLine("Delete the scanCONTROL instance");
                if ((iRetValue = CLLTI.DelDevice(m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    return;
                }
            }

            m_hLLT_Laser1 = 0;
            m_hLLT_Laser2 = 0;
            m_hLLT_Laser3 = 0;
        }

        

        public Task ConnectAsync(List<Data.DataObjects.ProductLaserSetting> laserSettingList)
        {
            return Task.Run<bool>(() =>
            {
                return Connect(laserSettingList);
            });
        }

        private void SetLaserUIResolution(Guid LaserID, uint uiResolution)
        {
            if (LaserID == Common.Constants.Lasers.LaserTop)
                m_uiResolutionLaser1 = uiResolution;
            if (LaserID == Common.Constants.Lasers.LaserLeft)
                m_uiResolutionLaser2 = uiResolution;
            if (LaserID == Common.Constants.Lasers.LaserRight)
                m_uiResolutionLaser3 = uiResolution;
        }

        private void SetLaserReference(Guid LaserID, uint laserRef)
        {
            if (LaserID == Common.Constants.Lasers.LaserTop)
                m_hLLT_Laser1 = laserRef;
            if (LaserID == Common.Constants.Lasers.LaserLeft)
                m_hLLT_Laser2 = laserRef;
            if (LaserID == Common.Constants.Lasers.LaserRight)
                m_hLLT_Laser3 = laserRef;
        }

        private void SetLaserControl(Guid LaserID, uint laserRef)
        {
            // Get the scanCONTROL type
            if (LaserID == Common.Constants.Lasers.LaserTop)
            {
                if ((CLLTI.GetLLTType(laserRef, ref m_tscanCONTROLType_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }

               

            }
            if (LaserID == Common.Constants.Lasers.LaserLeft)
            {
                if ((CLLTI.GetLLTType(laserRef, ref m_tscanCONTROLType_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                {

                }
                
            }
            if (LaserID == Common.Constants.Lasers.LaserRight)
            {
                if ((CLLTI.GetLLTType(laserRef, ref m_tscanCONTROLType_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                {

                }
                
            }
        }

        private static void StopLaser(Guid LaserID)
        {
            if(LaserID == Common.Constants.Lasers.LaserTop && m_hLLT_Laser1 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser1, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 0)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
                //if ((CLLTI.Disconnect(m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}

                //if ((CLLTI.DelDevice(m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}
            }
            if (LaserID == Common.Constants.Lasers.LaserLeft && m_hLLT_Laser2 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser2, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 0)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
                //if ((CLLTI.Disconnect(m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}

                //if ((CLLTI.DelDevice(m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}
            }
            if (LaserID == Common.Constants.Lasers.LaserRight && m_hLLT_Laser3 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser3, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 0)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
                //if ((CLLTI.Disconnect(m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}

                //if ((CLLTI.DelDevice(m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //}
            }
        }

        private static void StartLaser(Guid LaserID)
        {
            if (LaserID == Common.Constants.Lasers.LaserTop && m_hLLT_Laser1 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser1, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
            }
            if (LaserID == Common.Constants.Lasers.LaserLeft && m_hLLT_Laser2 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser2, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
            }
            if (LaserID == Common.Constants.Lasers.LaserRight && m_hLLT_Laser3 != 0)
            {
                if ((CLLTI.TransferProfiles(m_hLLT_Laser3, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                }
            }
        }

        //private static void SetProductSetting(Guid LaserID, Data.DataObjects.ProductLaserSetting laserProductSetting)
        //{
        //    if (LaserID == Common.Constants.Lasers.LaserTop && m_hLLT_Laser1 != 0)
        //    {
        //        ProductLaserSetting1 = laserProductSetting;
        //    }
        //    if (LaserID == Common.Constants.Lasers.LaserLeft && m_hLLT_Laser2 != 0)
        //    {
        //        ProductLaserSetting2 = laserProductSetting;
        //    }
        //    if (LaserID == Common.Constants.Lasers.LaserRight && m_hLLT_Laser3 != 0)
        //    {
        //        ProductLaserSetting3 = laserProductSetting;
        //    }
        //}


        public unsafe bool Connect(List<Data.DataObjects.ProductLaserSetting> laserSettingList)
        {
            m_uiResolutionLaser1 = 0;
            uint uiBufferCount = 10, uiMainReflection = 0, uiPacketSize = 1024;

            ProductLaserSetting1 = laserSettingList.Where(a => a.LaserID == Common.Constants.Lasers.LaserTop).FirstOrDefault();
            ProductLaserSetting2 = laserSettingList.Where(a => a.LaserID == Common.Constants.Lasers.LaserLeft).FirstOrDefault();
            ProductLaserSetting3 = laserSettingList.Where(a => a.LaserID == Common.Constants.Lasers.LaserRight).FirstOrDefault();


            uint[] auiResolutions = new uint[MAX_RESOULUTIONS];

            int iRetValue = 0;


            uint[] auiInterfaces = new uint[MAX_INTERFACE_COUNT];

            int iInterfaceCount = 0;

            int interfaceIndex1 = -1;
            int interfaceIndex2 = -1;
            int interfaceIndex3 = -1;


            //Create a Ethernet Device -> returns handle to LLT device
            if (ProductLaserSetting1 != null)
            {
                m_hLLT_Laser1 = CLLTI.CreateLLTDevice(CLLTI.TInterfaceType.INTF_TYPE_ETHERNET);
                if (m_hLLT_Laser1 == 0)
                {
                    OnError("Error creating device", iRetValue, m_hLLT_Laser1);
                    return false;

                }
            }
            if (ProductLaserSetting2 != null)
            {
                m_hLLT_Laser2 = CLLTI.CreateLLTDevice(CLLTI.TInterfaceType.INTF_TYPE_ETHERNET);
                if (m_hLLT_Laser2 == 0)
                {
                    OnError("Error creating device", iRetValue, m_hLLT_Laser2);
                    return false;

                }
            }
            if (ProductLaserSetting3 != null)
            {
                m_hLLT_Laser3 = CLLTI.CreateLLTDevice(CLLTI.TInterfaceType.INTF_TYPE_ETHERNET);
                if (m_hLLT_Laser3 == 0)
                {
                    OnError("Error creating device", iRetValue, m_hLLT_Laser3);
                    return false;

                }
            }

            CLLTI.ProfileReceiveMethod fnProfileReceiveMethod = null;


            if (fnProfileReceiveMethod == null)
                fnProfileReceiveMethod += new CLLTI.ProfileReceiveMethod(ProfileEvent);



            //Gets the available interfaces from the scanCONTROL-device
            if (m_hLLT_Laser1 != 0)
            {
                iInterfaceCount = CLLTI.GetDeviceInterfacesFast(m_hLLT_Laser1, auiInterfaces, auiInterfaces.GetLength(0));
                if (iInterfaceCount <= 0)
                    Console.WriteLine("There is no scanCONTROL connected");
                else if (iInterfaceCount == 1)
                    Console.WriteLine("There is 1 scanCONTROL connected ");
                else
                    Console.WriteLine("There are " + iInterfaceCount + " scanCONTROL's connected");

                if (ProductLaserSetting1 != null)
                    interfaceIndex1 = GetInterfaceIndexByIP(ProductLaserSetting1.IPAddress, auiInterfaces);
                if (ProductLaserSetting2 != null)
                    interfaceIndex2 = GetInterfaceIndexByIP(ProductLaserSetting2.IPAddress, auiInterfaces);
                if (ProductLaserSetting3 != null)
                    interfaceIndex3 = GetInterfaceIndexByIP(ProductLaserSetting3.IPAddress, auiInterfaces);
            }
            else if (m_hLLT_Laser2 != 0)
            {
                iInterfaceCount = CLLTI.GetDeviceInterfacesFast(m_hLLT_Laser2, auiInterfaces, auiInterfaces.GetLength(0));
                if (iInterfaceCount <= 0)
                    Console.WriteLine("There is no scanCONTROL connected");
                else if (iInterfaceCount == 1)
                    Console.WriteLine("There is 1 scanCONTROL connected ");
                else
                    Console.WriteLine("There are " + iInterfaceCount + " scanCONTROL's connected");

                if (ProductLaserSetting1 != null)
                    interfaceIndex1 = GetInterfaceIndexByIP(ProductLaserSetting1.IPAddress, auiInterfaces);
                if (ProductLaserSetting2 != null)
                    interfaceIndex2 = GetInterfaceIndexByIP(ProductLaserSetting2.IPAddress, auiInterfaces);
                if (ProductLaserSetting3 != null)
                    interfaceIndex3 = GetInterfaceIndexByIP(ProductLaserSetting3.IPAddress, auiInterfaces);

            }
            else if (m_hLLT_Laser3 != 0)
            {
                iInterfaceCount = CLLTI.GetDeviceInterfacesFast(m_hLLT_Laser3, auiInterfaces, auiInterfaces.GetLength(0));
                if (iInterfaceCount <= 0)
                    Console.WriteLine("There is no scanCONTROL connected");
                else if (iInterfaceCount == 1)
                    Console.WriteLine("There is 1 scanCONTROL connected ");
                else
                    Console.WriteLine("There are " + iInterfaceCount + " scanCONTROL's connected");

                if (ProductLaserSetting1 != null)
                    interfaceIndex1 = GetInterfaceIndexByIP(ProductLaserSetting1.IPAddress, auiInterfaces);
                if (ProductLaserSetting2 != null)
                    interfaceIndex2 = GetInterfaceIndexByIP(ProductLaserSetting2.IPAddress, auiInterfaces);
                if (ProductLaserSetting3 != null)
                    interfaceIndex3 = GetInterfaceIndexByIP(ProductLaserSetting3.IPAddress, auiInterfaces);
            }

            if (m_hLLT_Laser1 != 0)
            {
                if ((iRetValue = CLLTI.SetDeviceInterface(m_hLLT_Laser1, auiInterfaces[interfaceIndex1], 0))
                   < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error setting interfaceIndex: {0}", interfaceIndex1), iRetValue, m_hLLT_Laser1);
                    return false;
                }
            }

            if (m_hLLT_Laser2 != 0)
            {
                if ((iRetValue = CLLTI.SetDeviceInterface(m_hLLT_Laser2, auiInterfaces[interfaceIndex2], 0))
                   < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error setting interfaceIndex: {0}", interfaceIndex2), iRetValue, m_hLLT_Laser2);
                    return false;
                }
            }

            if (m_hLLT_Laser3 != 0)
            {
                if ((iRetValue = CLLTI.SetDeviceInterface(m_hLLT_Laser3, auiInterfaces[interfaceIndex3], 0))
                   < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error setting interfaceIndex: {0}", interfaceIndex2), iRetValue, m_hLLT_Laser2);
                    return false;
                }
            }

            if (m_hLLT_Laser1 != 0)
            {

                // Connect to sensor with the device interface set before
                if ((iRetValue = CLLTI.Connect(m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error connecting interfaceIndex: {0}, Total interfaces: {1}", interfaceIndex1, string.Join(",", auiInterfaces)), iRetValue, m_hLLT_Laser1);
                    return false;
                }
                else
                    bConnected = true;

                if ((CLLTI.GetLLTType(m_hLLT_Laser1, ref m_tscanCONTROLType_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error gettingtype interfaceIndex: {0}", interfaceIndex1), iRetValue, m_hLLT_Laser1);

                }

                ProductLaserSetting1.CalculateProfileValues();
                m_uiResolutionLaser1 = ProductLaserSetting1.Resolution;


                if ((iRetValue = CLLTI.SetProfileConfig(m_hLLT_Laser1, CLLTI.TProfileConfig.PROFILE)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error set profile", iRetValue, m_hLLT_Laser1);

                    return false;
                }


                if ((iRetValue = CLLTI.SetPacketSize(m_hLLT_Laser1, uiPacketSize)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during SetPacketSize", iRetValue, m_hLLT_Laser1);
                    return false;
                }

                uint uiCurrentUserMode = 0;
                uint uiUserModeCount = 0;
                if ((iRetValue = CLLTI.GetActualUserMode(m_hLLT_Laser1, ref uiCurrentUserMode, ref uiUserModeCount)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during getting UM", iRetValue, m_hLLT_Laser1);
                    return false;
                }
                else
                {
                    Console.WriteLine(" - Current UM : " + uiCurrentUserMode + " - Available UMs: " + uiUserModeCount);
                }


                    if ((CLLTI.RegisterCallback(m_hLLT_Laser1, CLLTI.TCallbackType.STD_CALL, fnProfileReceiveMethod, m_hLLT_Laser1)) < CLLTI.GENERAL_FUNCTION_OK)
                    {
                        OnError("Error registering callback", iRetValue, m_hLLT_Laser1);
                    }

            }


            if (m_hLLT_Laser2 != 0)
            {
                // Connect to sensor with the device interface set before
                if ((iRetValue = CLLTI.Connect(m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error connecting interfaceIndex: {0}, Total interfaces: {1}", interfaceIndex2, string.Join(",", auiInterfaces)), iRetValue, m_hLLT_Laser2);
                    return false;
                }
                else
                    bConnected = true;

                if ((CLLTI.GetLLTType(m_hLLT_Laser2, ref m_tscanCONTROLType_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error gettingtype interfaceIndex: {0}", interfaceIndex2), iRetValue, m_hLLT_Laser2);

                }

                ProductLaserSetting2.CalculateProfileValues();
                m_uiResolutionLaser2 = ProductLaserSetting2.Resolution;


                if ((iRetValue = CLLTI.SetProfileConfig(m_hLLT_Laser2, CLLTI.TProfileConfig.PROFILE)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error set profile", iRetValue, m_hLLT_Laser2);

                    return false;
                }


                if ((iRetValue = CLLTI.SetPacketSize(m_hLLT_Laser2, uiPacketSize)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during SetPacketSize", iRetValue, m_hLLT_Laser2);
                    return false;
                }

                uint uiCurrentUserMode = 0;
                uint uiUserModeCount = 0;
                if ((iRetValue = CLLTI.GetActualUserMode(m_hLLT_Laser2, ref uiCurrentUserMode, ref uiUserModeCount)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during getting UM", iRetValue, m_hLLT_Laser2);
                    return false;
                }
                else
                {
                    Console.WriteLine(" - Current UM : " + uiCurrentUserMode + " - Available UMs: " + uiUserModeCount);
                }


                if (m_hLLT_Laser2 != 0)
                    if ((CLLTI.RegisterCallback(m_hLLT_Laser2, CLLTI.TCallbackType.STD_CALL, fnProfileReceiveMethod, m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
                    {
                        OnError("Error registering callback", iRetValue, m_hLLT_Laser2);
                    }

            }

            if (m_hLLT_Laser3 != 0)
            {
                //// Set the first IP address detected by GetDeviceInterfacesFast to handle
                //int interfaceIndex = GetInterfaceIndexByIP(ProductLaserSetting3.IPAddress, auiInterfaces);
                //if (interfaceIndex == -1)
                //{
                //    OnError("Error finding interface", iRetValue, m_hLLT_Laser3);
                //    return false;
                //}

                //if ((iRetValue = CLLTI.SetDeviceInterface(m_hLLT_Laser3, auiInterfaces[interfaceIndex], 0))
                //   < CLLTI.GENERAL_FUNCTION_OK)
                //{
                //    OnError(string.Format("Error setting interfaceIndex: {0}", interfaceIndex), iRetValue, m_hLLT_Laser3);
                //    return false;
                //}

                // Connect to sensor with the device interface set before
                if ((iRetValue = CLLTI.Connect(m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error connecting interfaceIndex: {0}, Total interfaces: {1}", interfaceIndex3, string.Join(",", auiInterfaces)), iRetValue, m_hLLT_Laser3);
                    return false;
                }
                else
                    bConnected = true;

                if ((CLLTI.GetLLTType(m_hLLT_Laser3, ref m_tscanCONTROLType_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError(string.Format("Error gettingtype interfaceIndex: {0}", interfaceIndex3), iRetValue, m_hLLT_Laser3);

                }

                ProductLaserSetting3.CalculateProfileValues();
                m_uiResolutionLaser3 = ProductLaserSetting3.Resolution;


                if ((iRetValue = CLLTI.SetProfileConfig(m_hLLT_Laser3, CLLTI.TProfileConfig.PROFILE)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error set profile", iRetValue, m_hLLT_Laser3);

                    return false;
                }


                if ((iRetValue = CLLTI.SetPacketSize(m_hLLT_Laser3, uiPacketSize)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during SetPacketSize", iRetValue, m_hLLT_Laser3);
                    return false;
                }

                uint uiCurrentUserMode = 0;
                uint uiUserModeCount = 0;
                if ((iRetValue = CLLTI.GetActualUserMode(m_hLLT_Laser3, ref uiCurrentUserMode, ref uiUserModeCount)) < CLLTI.GENERAL_FUNCTION_OK)
                {
                    OnError("Error during getting UM", iRetValue, m_hLLT_Laser3);
                    return false;
                }
                else
                {
                    Console.WriteLine(" - Current UM : " + uiCurrentUserMode + " - Available UMs: " + uiUserModeCount);
                }


                if (m_hLLT_Laser3 != 0)
                    if ((CLLTI.RegisterCallback(m_hLLT_Laser3, CLLTI.TCallbackType.STD_CALL, fnProfileReceiveMethod, m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
                    {
                        OnError("Error registering callback", iRetValue, m_hLLT_Laser3);
                    }

            }

            GetProfiles_Callback();

            return true;

            /*
            //if (laserSettingLaser1 != null)
            //{

            //    // calculate profile values for later use
            //   // laserSettingLaser1.CalculateProfileValues();










            //    // Set reference
            //    //SetLaserReference(laserSetting.LaserID, m_hLLT);


            //    // Get the scanCONTROL type
            //    //SetLaserControl(laserSetting.LaserID, m_hLLT);

            //    // Set references
            //   // SetProductSetting(laserSetting.LaserID, laserSetting);

            //    // Get all possible resolutions for connected sensor and save them in array 
            //    //if ((iRetValue = CLLTI.GetResolutions(m_hLLT, auiResolutions, auiResolutions.GetLength(0))) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error getting resolutions", iRetValue);
            //    //    return false;
            //    //}

            //    // Set the max. possible resolution
            //   // SetLaserUIResolution(laserSetting.LaserID, laserSetting.Resolution);// auiResolutions[0]);

            //    // Set scanner settings to valid parameters for this example




            //    // set trigger to internal
            //    //if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_TRIGGER, 0x00000000)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    return false;
            //    //}

            //    //if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_SHUTTERTIME, m_uiShutterTime)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    return false;
            //    //}

            //    //if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_IDLETIME, m_uiIdleTime)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    return false;
            //    //}

            //    //if ((iRetValue = CLLTI.SetResolution(m_hLLT, m_uiResolution)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during SetResolution", iRetValue);
            //    //    return false;
            //    //}

            //    //if ((iRetValue = CLLTI.SetBufferCount(m_hLLT, uiBufferCount)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during SetBufferCount", iRetValue);
            //    //    return false;
            //    //}

            //    // Check if sensor supports the container mode
            //    //uint uiInquiry = 0;
            //    //if ((iRetValue = CLLTI.GetFeature(m_hLLT, CLLTI.INQUIRY_FUNCTION_REARRANGEMENT_PROFILE, ref uiInquiry)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during GetFeature", iRetValue);
            //    //    return false;
            //    //}
            //    //if ((uiInquiry & 0x80000000) == 0)
            //    //{
            //    //    Console.WriteLine("The connected scanCONTROL doesn't support the container mode");
            //    //    return false;
            //    //}

            //    //uint  uiMaxHeight = 0, uiMaxWidth = 0;
            //    //if ((iRetValue = CLLTI.GetMaxProfileContainerSize(m_hLLT, ref uiMaxWidth, ref uiMaxHeight)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during GetProfileConfig", iRetValue);
            //    //    return false;
            //    //}

            //    ////Console.WriteLine("Set profile container size");
            //    //if ((iRetValue = CLLTI.SetProfileContainerSize(m_hLLT, 640, uiProfileCount)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during SetProfileContainerSize", iRetValue);
            //    //    return false;
            //    //}



            //    //if ((iRetValue = CLLTI.SetMainReflection(m_hLLT, uiMainReflection)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during SetMainReflection", iRetValue);
            //    //    return false;
            //    //}


            //    //uint Trigger = 0; uint Encoder = 0;

            //    //// Set trigger input to encoder    
            //    //Trigger += (3 << 16);

            //    //// Set digital inputs as trigger input and activate ext. triggering    
            //    //Trigger += (1 << 21) + (1 << 25);

            //    //// Set trigger settings    
            //    //CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_TRIGGER, Trigger);

            //    //// Activate encoder    
            //    //Encoder += (1 << 3);
            //    //CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_MAINTENANCEFUNCTIONS, Encoder); 





            //    //if ((iRetValue = CLLTI.ReadWriteUserModes(m_hLLT, 0, 1)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //{
            //    //    OnError("Error during loading UM 7", iRetValue);
            //    //    return false;
            //    //}



            //    // Set the callback function


            //    // Register the callback


            //    //if (m_hLLT_Laser2 != 0)
            //    //    if ((CLLTI.RegisterCallback(m_hLLT_Laser2, CLLTI.TCallbackType.STD_CALL, fnProfileReceiveMethod, m_hLLT_Laser2)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //    {

            //    //    }

            //    //if (m_hLLT_Laser3 != 0)
            //    //    if ((CLLTI.RegisterCallback(m_hLLT_Laser3, CLLTI.TCallbackType.STD_CALL, fnProfileReceiveMethod, m_hLLT_Laser3)) < CLLTI.GENERAL_FUNCTION_OK)
            //    //    {

            //    //    }

            //}


           // return true;
           */
        }

        static void GetProfiles_Callback()
        {
            int iRetValue;
            double[] adValueX = new double[m_uiResolutionLaser1];
            double[] adValueZ = new double[m_uiResolutionLaser1];
            ushort[] adValueW = new ushort[m_uiResolutionLaser1];

            m_cProfileData_Laser1 = new byte[m_uiResolutionLaser1 * 64 * m_uiNeededProfileCount_Laser];
            r_cProfileData_Laser1 = new byte[m_uiResolutionLaser1 * 64 * m_uiNeededProfileCount_Laser];

            m_cProfileData_Laser2 = new byte[m_uiResolutionLaser2 * 64 * m_uiNeededProfileCount_Laser];
            r_cProfileData_Laser2 = new byte[m_uiResolutionLaser2 * 64 * m_uiNeededProfileCount_Laser];

            m_cProfileData_Laser3 = new byte[m_uiResolutionLaser3 * 64 * m_uiNeededProfileCount_Laser];
            r_cProfileData_Laser3 = new byte[m_uiResolutionLaser3 * 64 * m_uiNeededProfileCount_Laser];


            abyContainerBufferCopyLaser1 = new byte[m_uiResolutionLaser1 * 64];
            abyContainerBufferCopyLaser2 = new byte[m_uiResolutionLaser2 * 64];
            abyContainerBufferCopyLaser3 = new byte[m_uiResolutionLaser3 * 64];

            // Allocate the profile buffer to the maximal profile size times the profile count
            //m_cProfileData = new byte[m_uiResolution * 64 * m_uiNeededProfileCount];

            //var hexcode = 3 + (int)(Math.Pow((double)2, (int)11) + Math.Pow((double)2, (int)10) + Math.Pow((double)2, (int)12) * 1 + Math.Pow((double)2, 20));
            //double dTempLog = 1.0 / Math.Log(2.0);
            //uint uiResolutionBitField = (uint)Math.Floor((Math.Log((double)m_uiResolution) * dTempLog) + 0.5);
            //if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_REARRANGEMENT_PROFILE, uiResolutionBitField << 12)) < CLLTI.GENERAL_FUNCTION_OK)
            //{
            //    OnError("Error during SetFeature", iRetValue);
            //    return;
            //}


            // Start continous profile transmission
            //Console.WriteLine("Enable the measurement");
            StartLaser(Common.Constants.Lasers.LaserTop);
            StartLaser(Common.Constants.Lasers.LaserLeft);
            StartLaser(Common.Constants.Lasers.LaserRight);

          

            // Wait for profile event (or timeout)
            //Console.WriteLine("Wait for needed profiles\n");

            while (true)
            {
                if (m_hProfileEvent.WaitOne(5000) != true)
                {
                    if (stopValue)
                        break;

                    Console.WriteLine("No profile received \n\n");
                    continue;
                }

               
                
                // Iterate over profiles (Laser1)
                if (m_hLLT_Laser1 != 0 && profileReceived_Laser1)
                {
                    profileReceived_Laser1 = false;

                    LaserDataClass dataClass = new LaserDataClass(ProductLaserSetting1);

                    for (uint uiProfile = 0; uiProfile < m_uiNeededProfileCount_Laser; uiProfile++)
                    {
                        Array.Copy(r_cProfileData_Laser1, uiProfile * m_uiResolutionLaser1 * 64, abyContainerBufferCopyLaser1, 0, abyContainerBufferCopyLaser1.GetLength(0));

                        // Convert profile to x and z values
                        if ((iRetValue = CLLTI.ConvertProfile2Values(m_hLLT_Laser1, abyContainerBufferCopyLaser1, m_uiResolutionLaser1, CLLTI.TProfileConfig.PROFILE, m_tscanCONTROLType_Laser1, 0, 1, adValueW, null, null, adValueX, adValueZ, null, null)) < CLLTI.GENERAL_FUNCTION_OK)
                        {
                            OnError("Error during TransferProfiles", iRetValue, m_hLLT_Laser1);
                            return;
                        }

                        if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0))
                        {
                            OnError("Error during Converting of profile data", iRetValue, m_hLLT_Laser1);
                            return;
                        }

                        dataClass.AddDataRow(adValueZ, adValueX, adValueW);
                    }

                    // saada event andmetega
                    if (DataReceived != null)
                    {
                        DataReceived(dataClass);
                    }
                }

                if (m_hLLT_Laser2 != 0 && profileReceived_Laser2)
                {

                    profileReceived_Laser2 = false;

                    LaserDataClass dataClass = new LaserDataClass(ProductLaserSetting2);

                    for (uint uiProfile = 0; uiProfile < m_uiNeededProfileCount_Laser; uiProfile++)
                    {
                        Array.Copy(r_cProfileData_Laser2, uiProfile * m_uiResolutionLaser2 * 64, abyContainerBufferCopyLaser2, 0, abyContainerBufferCopyLaser2.GetLength(0));

                        // Convert profile to x and z values
                        if ((iRetValue = CLLTI.ConvertProfile2Values(m_hLLT_Laser2, abyContainerBufferCopyLaser2, m_uiResolutionLaser2, CLLTI.TProfileConfig.PROFILE, m_tscanCONTROLType_Laser2, 0, 1, adValueW, null, null, adValueX, adValueZ, null, null)) < CLLTI.GENERAL_FUNCTION_OK)
                        {
                            OnError("Error during TransferProfiles", iRetValue, m_hLLT_Laser2);
                            return;
                        }

                        if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0))
                        {
                            OnError("Error during Converting of profile data", iRetValue, m_hLLT_Laser2);
                            return;
                        }

                        dataClass.AddDataRow(adValueZ, adValueX, adValueW);
                    }

                    // saada event andmetega
                    if (DataReceived != null)
                    {
                        DataReceived(dataClass);
                    }
                }

                if (m_hLLT_Laser3 != 0 && profileReceived_Laser3)
                {
                    profileReceived_Laser3 = false;

                    LaserDataClass dataClass = new LaserDataClass(ProductLaserSetting3);

                    for (uint uiProfile = 0; uiProfile < m_uiNeededProfileCount_Laser; uiProfile++)
                    {
                        Array.Copy(r_cProfileData_Laser3, uiProfile * m_uiResolutionLaser3 * 64, abyContainerBufferCopyLaser3, 0, abyContainerBufferCopyLaser3.GetLength(0));

                        // Convert profile to x and z values
                        if ((iRetValue = CLLTI.ConvertProfile2Values(m_hLLT_Laser3, abyContainerBufferCopyLaser3, m_uiResolutionLaser3, CLLTI.TProfileConfig.PROFILE, m_tscanCONTROLType_Laser3, 0, 1, adValueW, null, null, adValueX, adValueZ, null, null)) < CLLTI.GENERAL_FUNCTION_OK)
                        {
                            OnError("Error during TransferProfiles", iRetValue, m_hLLT_Laser3);
                            return;
                        }

                        if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0))
                        {
                            OnError("Error during Converting of profile data", iRetValue, m_hLLT_Laser3);
                            return;
                        }

                        dataClass.AddDataRow(adValueZ, adValueX, adValueW);
                    }

                    // saada event andmetega
                    if (DataReceived != null)
                    {
                        DataReceived(dataClass);

                    }
                }


                // If needs to stop
                if (stopValue)
                {
                    StopLaser(Common.Constants.Lasers.LaserTop);
                    StopLaser(Common.Constants.Lasers.LaserLeft);
                    StopLaser(Common.Constants.Lasers.LaserRight);
                }

               

            }


            // Convert profile to x and z values
            //Console.WriteLine("Converting of profile data from the first reflection");


            // Stop continous profile transmission
            //Console.WriteLine("Disable the measurement");
            //if ((iRetValue = CLLTI.TransferProfiles(m_hLLT, CLLTI.TTransferProfileType.NORMAL_TRANSFER, 0)) < CLLTI.GENERAL_FUNCTION_OK)
            //{
            //    OnError("Error during TransferProfiles", iRetValue);
            //    return;
            //}
        }
        //static void ContainerModeProfile()
        //{
        //    int iRetValue;
        //    uint uiProfileCount = 10;   // Number of profiles in one container
        //    uint uiProfileCounter = 0;
        //    uint uiInquiry = 0;
        //    uint uiLostProfiles = 0;
        //    double dTimeShutterOpen = 0.0;
        //    double dTimeShutterClose = 0.0;

        //    double[] adValueX = new double[m_uiResolution];
        //    double[] adValueZ = new double[m_uiResolution];

        //    // Calculate the bitfield for the resolution (e.g. if Resolution 160 the result must be 7; for 1280 the result must be 10)
        //    double dTempLog = 1.0 / Math.Log(2.0);
        //    uint uiResolutionBitField = (uint)Math.Floor((Math.Log((double)m_uiResolution) * dTempLog) + 0.5);

        //    Console.WriteLine("Demonstrate the container mode for profile transfer");

        //    // Check if sensor supports the container mode
        //    if ((iRetValue = CLLTI.GetFeature(m_hLLT, CLLTI.INQUIRY_FUNCTION_REARRANGEMENT_PROFILE, ref uiInquiry)) < CLLTI.GENERAL_FUNCTION_OK)
        //    {
        //        OnError("Error during GetFeature", iRetValue);
        //        return;
        //    }
        //    if ((uiInquiry & 0x80000000) == 0)
        //    {
        //        Console.WriteLine("The connected scanCONTROL doesn't support the container mode");
        //        return;
        //    }


        //    //calculation for the points per profile = Round(Log2(Auflösung))        
        //    Console.WriteLine("Set the rearrangement parameter");
        //    if ((iRetValue = CLLTI.SetFeature(m_hLLT, CLLTI.FEATURE_FUNCTION_REARRANGEMENT_PROFILE, uiResolutionBitField << 12)) < CLLTI.GENERAL_FUNCTION_OK)
        //    {
        //        OnError("Error during SetFeature", iRetValue);
        //        return;
        //    }

        //    // Set the profile container size according to the given profile count
        //    Console.WriteLine("Set profile container size");
        //    if ((iRetValue = CLLTI.SetProfileContainerSize(m_hLLT, 0, uiProfileCount)) < CLLTI.GENERAL_FUNCTION_OK)
        //    {
        //        OnError("Error during SetProfileContainerSize", iRetValue);
        //        return;
        //    }

        //    // Start continous profile transmission
        //    Console.WriteLine("Enable the measurement");
        //    if ((iRetValue = CLLTI.TransferProfiles(m_hLLT, CLLTI.TTransferProfileType.NORMAL_CONTAINER_MODE, 1)) < CLLTI.GENERAL_FUNCTION_OK)
        //    {
        //        OnError("Error during TransferProfiles", iRetValue);
        //        return;
        //    }

        //    // Set buffersize according to transmitted data
        //    byte[] abyContainerBuffer = new byte[m_uiResolution * 64 * uiProfileCount]; // 2* because 1 value has 2 bytes
        //    byte[] abyContainerBufferCopy = new byte[m_uiResolution * 64];
        //    byte[] abyTimestamp = new byte[16];

        //    //Sleep for a while to warm up the transfer
        //    System.Threading.Thread.Sleep(1000);

        //    // Poll one container
        //    if ((iRetValue = CLLTI.GetActualProfile(m_hLLT, abyContainerBuffer, abyContainerBuffer.GetLength(0), CLLTI.TProfileConfig.CONTAINER, ref uiLostProfiles)) != abyContainerBuffer.GetLength(0))
        //    {
        //        OnError("Error during GetActualProfile", iRetValue);
        //        Console.WriteLine("iRetValue: " + iRetValue + " Bufferlength: " + abyContainerBuffer.GetLength(0));
        //        return;
        //    }

        //    // Iterate over profiles
        //    for (uint uiProfile = 0; uiProfile < uiProfileCount; uiProfile++)
        //    {
        //        Array.Copy(abyContainerBuffer, uiProfile * m_uiResolution * 64, abyContainerBufferCopy, 0, abyContainerBufferCopy.GetLength(0));

        //        // Convert profile to x and z values
        //        if ((iRetValue = CLLTI.ConvertProfile2Values(m_hLLT, abyContainerBufferCopy, m_uiResolution, CLLTI.TProfileConfig.PROFILE, m_tscanCONTROLType, 0, 1, null, null, null, adValueX, adValueZ, null, null)) < CLLTI.GENERAL_FUNCTION_OK)
        //        {
        //            OnError("Error during TransferProfiles", iRetValue);
        //            return;
        //        }

        //        if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0))
        //        {
        //            OnError("Error during Converting of profile data", iRetValue);
        //            return;
        //        }

        //        for (uint j = 0; j < 40; j++)
        //        {
        //            Console.WriteLine("(" + adValueX[j] + ", " + adValueZ[j] + "), ");
        //        }

        //        for (int i = 0; i < 16; i++)
        //        {
        //            abyTimestamp[i] = abyContainerBufferCopy[(m_uiResolution * 64) - 16 + i];
        //        }
        //        CLLTI.Timestamp2TimeAndCount(abyTimestamp, ref dTimeShutterOpen, ref dTimeShutterClose, ref uiProfileCounter);
        //        Console.WriteLine("Profile Counter: " + uiProfileCounter);
        //    }

        //    // Stop continous profile transmission
        //    Console.WriteLine("Disable the measurement");
        //    if ((iRetValue = CLLTI.TransferProfiles(m_hLLT, CLLTI.TTransferProfileType.NORMAL_CONTAINER_MODE, 0)) < CLLTI.GENERAL_FUNCTION_OK)
        //    {
        //        OnError("Error during TransferProfiles", iRetValue);
        //        return;
        //    }
        //}



        private int GetInterfaceIndexByIP(string ip, uint[] auiInterfaces)
        {
            int interfaceIndex = 0;
            foreach (var item in auiInterfaces)
            {
                uint target4 = item & 0x000000FF;
                uint target3 = (item & 0x0000FF00) >> 8;
                uint target2 = (item & 0x00FF0000) >> 16;
                uint target1 = (item & 0xFF000000) >> 24;

                if (ip == string.Format("{0}.{1}.{2}.{3}", target1, target2, target3, target4))
                    return interfaceIndex;

                interfaceIndex++;
            }

            return -1;
        }

        [DllImport("msvcrt.dll", EntryPoint = "memcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
        static unsafe extern IntPtr memcpy(byte* dest, byte* src, uint count);

        //static unsafe void NewProfile(byte* data, uint iSize, uint UserData)
        //{
        //    // peab olema 40960

        //    int iRetValue;
        //    double[] adValueX = new double[m_uiResolutionLaser1];
        //    double[] adValueZ = new double[m_uiResolutionLaser1];
        //    ushort[] adValueW = new ushort[m_uiResolutionLaser1];


        //    // Set buffersize according to transmitted data
        //    //byte[] abyContainerBuffer = new byte[m_uiResolution * 64 * uiProfileCount]; // 2* because 1 value has 2 bytes
        //    byte[] abyContainerBufferCopy = new byte[m_uiResolutionLaser1 * 64];

        //    byte[] m_cProfileData = new byte[m_uiResolutionLaser1 * 64* m_uiNeededProfileCount_Laser1];//  new byte[m_uiResolution * 64 * uiProfileCount];

        //    fixed (byte* dst = &m_cProfileData[iSize]) { memcpy(dst, data, iSize); }

        //    // Iterate over profiles
        //    for (uint uiProfile = 0; uiProfile < m_uiNeededProfileCount_Laser1; uiProfile++)
        //    {
        //        Array.Copy(m_cProfileData, uiProfile * m_uiResolutionLaser1 * 64, abyContainerBufferCopy, 0, abyContainerBufferCopy.GetLength(0));

        //        // Convert profile to x and z values
        //        if ((iRetValue = CLLTI.ConvertProfile2Values(m_hLLT_Laser1, abyContainerBufferCopy, m_uiResolutionLaser1, CLLTI.TProfileConfig.CONTAINER, m_tscanCONTROLType, 0, 1, adValueW, null, null, adValueX, adValueZ, null, null)) < CLLTI.GENERAL_FUNCTION_OK)
        //        {
        //            OnError("Error during TransferProfiles", iRetValue);
        //            return;
        //        }

        //        if (((iRetValue & CLLTI.CONVERT_X) == 0) || ((iRetValue & CLLTI.CONVERT_Z) == 0) || ((iRetValue & CLLTI.CONVERT_WIDTH) == 0))
        //        {
        //            OnError("Error during Converting of profile data", iRetValue);
        //            return;
        //        }

        //        Console.WriteLine("(" + adValueX[0] + ", " + adValueZ[0] + "), ");

        //        //for (uint j = 0; j < 4; j++)
        //        //{
        //        //    Console.WriteLine("(" + adValueX[j] + ", " + adValueZ[j] + "), ");
        //        //}

        //        //abyTimestamp[uiProfile] = abyContainerBufferCopy[(m_uiResolution * 64) - 16 + uiProfile];
        //        //CLLTI.Timestamp2TimeAndCount(abyTimestamp, ref dTimeShutterOpen, ref dTimeShutterClose, ref uiProfileCounter);
        //        //Console.WriteLine("Profile Counter: " + uiProfileCounter);

        //        //if (uiProfileCounter > 0)
        //        //{

        //        //}

        //    }
        //    m_hProfileEvent.Set();
        //}

        /*
         * Callback function which copies the received data into the buffer and sets an event after the specified profiles
         */
        static unsafe void ProfileEvent(byte* data, uint uiSize, uint uiUserData)
        {
            try
            {


                if (uiSize > 0)
                {
                    if (uiUserData == m_hLLT_Laser1)
                    {
                        if (m_bProfileReceived_Laser1 < m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count not arrived: copy the new Profile in the buffer and increase the recived buffer count
                            //Kopieren des Unmanaged Datenpuffers (data) in die Anwendung
                            fixed (byte* dst = &m_cProfileData_Laser1[m_bProfileReceived_Laser1 * uiSize])
                            {
                                memcpy(dst, data, uiSize);
                            }
                            m_bProfileReceived_Laser1++;
                        }

                        if (m_bProfileReceived_Laser1 >= m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count is arrived: set the event
                            Array.Copy(m_cProfileData_Laser1, r_cProfileData_Laser1, r_cProfileData_Laser1.Length);
                            m_bProfileReceived_Laser1 = 0;
                            profileReceived_Laser1 = true;
                            m_hProfileEvent.Set();
                        }
                    }

                    if (uiUserData == m_hLLT_Laser2)
                    {
                        if (m_bProfileReceived_Laser2 < m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count not arrived: copy the new Profile in the buffer and increase the recived buffer count
                            //Kopieren des Unmanaged Datenpuffers (data) in die Anwendung
                            fixed (byte* dst = &m_cProfileData_Laser2[m_bProfileReceived_Laser2 * uiSize])
                            {
                                memcpy(dst, data, uiSize);
                            }
                            m_bProfileReceived_Laser2++;
                        }

                        if (m_bProfileReceived_Laser2 >= m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count is arrived: set the event
                            Array.Copy(m_cProfileData_Laser2, r_cProfileData_Laser2, r_cProfileData_Laser2.Length);
                            m_bProfileReceived_Laser2 = 0;
                            profileReceived_Laser2 = true;
                            m_hProfileEvent.Set();
                        }
                    }
                    if (uiUserData == m_hLLT_Laser3)
                    {
                        if (m_bProfileReceived_Laser3 < m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count not arrived: copy the new Profile in the buffer and increase the recived buffer count
                            //Kopieren des Unmanaged Datenpuffers (data) in die Anwendung
                            fixed (byte* dst = &m_cProfileData_Laser3[m_bProfileReceived_Laser3 * uiSize])
                            {
                                memcpy(dst, data, uiSize);
                            }
                            m_bProfileReceived_Laser3++;
                        }

                        if (m_bProfileReceived_Laser3 >= m_uiNeededProfileCount_Laser)
                        {
                            //If the needed profile count is arrived: set the event
                            Array.Copy(m_cProfileData_Laser3, r_cProfileData_Laser3, r_cProfileData_Laser3.Length);
                            m_bProfileReceived_Laser3 = 0;
                            profileReceived_Laser3 = true;
                            m_hProfileEvent.Set();
                        }
                    }
                }
            }catch(Exception rr)

            {
                OnError(rr.Message, 0, uiUserData);
            }
        }


        static void OnError(string strErrorTxt, int iErrorValue, uint llt)
        {
            byte[] acErrorString = new byte[200];

            System.Windows.Forms.MessageBox.Show(strErrorTxt);
            if (CLLTI.TranslateErrorValue(llt, iErrorValue, acErrorString, acErrorString.GetLength(0))
                                            >= CLLTI.GENERAL_FUNCTION_OK)
            {
                System.Diagnostics.Debug.WriteLine(System.Text.Encoding.ASCII.GetString(acErrorString, 0, acErrorString.GetLength(0)));
                System.Windows.Forms.MessageBox.Show(System.Text.Encoding.ASCII.GetString(acErrorString, 0, acErrorString.GetLength(0)));
            }
        }
    }
}
