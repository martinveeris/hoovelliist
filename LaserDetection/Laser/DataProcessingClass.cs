﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Laser
{
    public class DataProcessingClass
    {
        public enum ProcessingStatusEnum
        {
            WaitingData, ReceivingData, DataReceived
        }

        public ProcessingStatusEnum ProcessingStatus { get; set; }

        public Data.DataObjects.QualityClass CalculatedQuality { get; set; }
        QualityCalculation.QualityCalculationClass qualityCalculationClass;

        public Data.DataObjects.Product SelectedProduct { get; set; }

        List<LaserDataClass> dataList = new List<LaserDataClass>();

        private int CurrentEndCount = 0;
        private int NeededEndCount = 0;

        public int NotCompletedProfileCount { get; set; }

        public delegate void QualityProcessingCompleteEventHandler(DataProcessingClass processingClass);
        public event QualityProcessingCompleteEventHandler QualityCompleteEvent;

        // protsesside nimekiri
        List<Task> postProcessingTaskList = new List<Task>();

        // leitud kontuuride nimekiri (LaserID, ..)
        internal Dictionary<Guid, List<Contour<Point>>> contourList = new Dictionary<Guid, List<Contour<Point>>>();

        // piltide nimekiri (LaserID, ..)
        internal Dictionary<Guid, Emgu.CV.Image<Bgr, double>> imageList = new Dictionary<Guid, Emgu.CV.Image<Bgr, double>>();

        List<Data.DataObjects.ProductLaserSetting> productSettingList = null;

        public DataProcessingClass(int neededEndCount, List<Data.DataObjects.ProductLaserSetting> productSettingList, Data.DataObjects.Product selectedProduct)
        {
            NeededEndCount = neededEndCount;
            ProcessingStatus = ProcessingStatusEnum.WaitingData;
            CalculatedQuality = new Data.DataObjects.QualityClass();
            CalculatedQuality.QualityClassID = Common.Constants.QualityClasses.Correct;
            CalculatedQuality.CalculatedDefectList = new List<Data.DataObjects.QualityClass.CalculatedDefectClass>();

            this.productSettingList = productSettingList;

            SelectedProduct = selectedProduct;

            // kvaliteedi klass hilisemaks arvutamiseks
            qualityCalculationClass = new QualityCalculation.QualityCalculationClass(SelectedProduct);

            // init image and contour lists
            foreach (var settingItem in productSettingList)
            {
                contourList.Add(settingItem.LaserID, new List<Contour<Point>>());
                imageList.Add(settingItem.LaserID, null);
            }
        }

        public void AddData(LaserDataClass laserDataClass)
        {
            //if (laserDataClass.ProfileDataStatus == LaserDataClass.ProfileDataStatusEnum.Empty && ProcessingStatus == ProcessingStatusEnum.ReceivingData)
            //{
            //    // start and end in the same buffer, close data
            //    ProcessingStatus = ProcessingStatusEnum.DataReceived;
            //}
            //// do not add more
            //else 
            if (laserDataClass.ProfileDataStatus == LaserDataClass.ProfileDataStatusEnum.Empty && ProcessingStatus == ProcessingStatusEnum.DataReceived)
                return;

            // count data that has End status
            if (laserDataClass.ProfileDataStatus == LaserDataClass.ProfileDataStatusEnum.End)
                CurrentEndCount++;
            

            if (laserDataClass.ProfileDataStatus != LaserDataClass.ProfileDataStatusEnum.Empty)
            {
                ProcessingStatus = ProcessingStatusEnum.ReceivingData;
                dataList.Add(laserDataClass);
            }

            // lisa listi, et oleks võimalik jälgida
            Task t = laserDataClass.PostProcessAsync();
            postProcessingTaskList.Add(t);

            // all data is received now, new cannot be added
            if (NeededEndCount == CurrentEndCount)
                ProcessingStatus = ProcessingStatusEnum.DataReceived;

            // if all data reiceived, then calculate quality
            if (ProcessingStatus == ProcessingStatusEnum.DataReceived)
            {
                //// võiks panna mingi piiri, et kui läheb üle sekundi, siis ei jää ootama.
                //Stopwatch sw = new Stopwatch();
                //sw.Start();

                foreach (var task in postProcessingTaskList)
                {
                    if (task.Status == TaskStatus.Running)
                        task.Wait();
                        {
                            // kui ei jõua ära oodata
                        }
                }

                //if(postProcessingTaskList.Where(a => a.Status == TaskStatus.Running).Count() > 0)
                //{
                //    // kui ei ole kõik protsessitud
                //}

                NotCompletedProfileCount = postProcessingTaskList.Where(a => a.Status == TaskStatus.Running).Count(); // mitu taski on lõpetamata

                // TODO: oota, et kõik klassid oleks protsessimise lõpetanud


                // tekita pilt ja kontuurid laseri kaupa
                foreach (var settingItem in productSettingList)
                {
                    var contour = contourList[settingItem.LaserID];
                    imageList[settingItem.LaserID] = GetImage(settingItem.LaserID, ref contour);
                    contourList[settingItem.LaserID] = contour;
                }

                // arvuta kvaliteet
                qualityCalculationClass.CalculateQuality(this);


                if (QualityCompleteEvent != null)
                {
                    QualityCompleteEvent(this);
                }
            }
        }

        #region GetImage
        public Bitmap GetLaserImage(Guid LaserID)
        {
            if (imageList[LaserID] == null)
                return new Bitmap(1, 640);

            return imageList[LaserID].Bitmap;
        }

        private Emgu.CV.Image<Bgr, double> GetImage(Guid LaserID, ref List<Contour<Point>> foundContours)
        {
            return Utils.ImageUtils.GetImageFromLaserClassMatrix(dataList.Where(a => a.ProductLaserSetting.LaserID == LaserID).ToList(), ref foundContours);
        }
        #endregion

    }
}
