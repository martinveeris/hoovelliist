﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Sensor
{
    public class SensorReadClass
    {

        public bool GetSensorValue(int lineNumber)
        {
            return Sensors.ReadWriteSensor.GetSensorValue(lineNumber);
        }

        public void SetSensorValue(int lineNumber)
        {
            try
            {
                Task.Delay(10).ContinueWith(t => Sensors.ReadWriteSensor.SetSensorValue(lineNumber, true));

                Task.Delay(1000).ContinueWith(t => Sensors.ReadWriteSensor.SetSensorValue(lineNumber, false));
            } catch
            {
                Task.Delay(500).ContinueWith(t => Sensors.ReadWriteSensor.SetSensorValue(lineNumber, false));
            }

        }

    }
}