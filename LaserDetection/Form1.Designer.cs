﻿namespace LaserDetection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItemStartControl = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSelectProduct = new DevExpress.XtraBars.BarButtonItem();
            this.btnRibbonProducts = new DevExpress.XtraBars.BarButtonItem();
            this.btnRibbonQualityClasses = new DevExpress.XtraBars.BarButtonItem();
            this.btnRibbonSystemSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnRibbonLaserSettings = new DevExpress.XtraBars.BarButtonItem();
            this.btnSensors = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.lblSelectedProductInfo = new DevExpress.XtraEditors.LabelControl();
            this.btnSelectProduct = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lblLaser3Info = new DevExpress.XtraEditors.LabelControl();
            this.lblLaser1Info = new DevExpress.XtraEditors.LabelControl();
            this.lblLaser2Info = new DevExpress.XtraEditors.LabelControl();
            this.pictureEditLaser3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEditLaser1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEditLaser2 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemLaser2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLaser1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLaser3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlError = new DevExpress.XtraEditors.LabelControl();
            this.lblTime = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bindingSourceDefects = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnSaveStatistics = new DevExpress.XtraEditors.SimpleButton();
            this.lblFinalQulity = new DevExpress.XtraEditors.LabelControl();
            this.btnClearStatistics = new DevExpress.XtraEditors.SimpleButton();
            this.lblCorrect = new DevExpress.XtraEditors.LabelControl();
            this.lblFixable = new DevExpress.XtraEditors.LabelControl();
            this.lblNotFixable = new DevExpress.XtraEditors.LabelControl();
            this.lblTotalCount = new DevExpress.XtraEditors.LabelControl();
            this.labelControlStartDateTime = new DevExpress.XtraEditors.LabelControl();
            this.labelControlControlStarted = new DevExpress.XtraEditors.LabelControl();
            this.btnStartControl = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemNotFixable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemFixable = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCorrect = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDefects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotFixable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFixable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCorrect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItemStartControl,
            this.barButtonItemSelectProduct,
            this.btnRibbonProducts,
            this.btnRibbonQualityClasses,
            this.btnRibbonSystemSettings,
            this.btnRibbonLaserSettings,
            this.btnSensors});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 8;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.Size = new System.Drawing.Size(1143, 141);
            // 
            // barButtonItemStartControl
            // 
            this.barButtonItemStartControl.Caption = "Käivita kontroll";
            this.barButtonItemStartControl.Id = 1;
            this.barButtonItemStartControl.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartControl.ImageOptions.Image")));
            this.barButtonItemStartControl.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemStartControl.ImageOptions.LargeImage")));
            this.barButtonItemStartControl.Name = "barButtonItemStartControl";
            this.barButtonItemStartControl.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemStartControl_ItemClick);
            // 
            // barButtonItemSelectProduct
            // 
            this.barButtonItemSelectProduct.Caption = "Vali kontrollitav toode";
            this.barButtonItemSelectProduct.Id = 2;
            this.barButtonItemSelectProduct.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemSelectProduct.ImageOptions.Image")));
            this.barButtonItemSelectProduct.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItemSelectProduct.ImageOptions.LargeImage")));
            this.barButtonItemSelectProduct.Name = "barButtonItemSelectProduct";
            this.barButtonItemSelectProduct.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSelectProduct_ItemClick);
            // 
            // btnRibbonProducts
            // 
            this.btnRibbonProducts.Caption = "Tooted";
            this.btnRibbonProducts.Id = 3;
            this.btnRibbonProducts.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRibbonProducts.ImageOptions.Image")));
            this.btnRibbonProducts.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRibbonProducts.ImageOptions.LargeImage")));
            this.btnRibbonProducts.Name = "btnRibbonProducts";
            this.btnRibbonProducts.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRibbonProducts_ItemClick);
            // 
            // btnRibbonQualityClasses
            // 
            this.btnRibbonQualityClasses.Caption = "Kvaliteediklassid";
            this.btnRibbonQualityClasses.Id = 4;
            this.btnRibbonQualityClasses.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRibbonQualityClasses.ImageOptions.Image")));
            this.btnRibbonQualityClasses.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRibbonQualityClasses.ImageOptions.LargeImage")));
            this.btnRibbonQualityClasses.Name = "btnRibbonQualityClasses";
            this.btnRibbonQualityClasses.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRibbonQualityClasses_ItemClick);
            // 
            // btnRibbonSystemSettings
            // 
            this.btnRibbonSystemSettings.Caption = "Süsteemiseaded";
            this.btnRibbonSystemSettings.Id = 5;
            this.btnRibbonSystemSettings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRibbonSystemSettings.ImageOptions.Image")));
            this.btnRibbonSystemSettings.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRibbonSystemSettings.ImageOptions.LargeImage")));
            this.btnRibbonSystemSettings.Name = "btnRibbonSystemSettings";
            this.btnRibbonSystemSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRibbonSystemSettings_ItemClick);
            // 
            // btnRibbonLaserSettings
            // 
            this.btnRibbonLaserSettings.Caption = "Laserite seaded";
            this.btnRibbonLaserSettings.Id = 6;
            this.btnRibbonLaserSettings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRibbonLaserSettings.ImageOptions.Image")));
            this.btnRibbonLaserSettings.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRibbonLaserSettings.ImageOptions.LargeImage")));
            this.btnRibbonLaserSettings.Name = "btnRibbonLaserSettings";
            this.btnRibbonLaserSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRibbonLaserSettings_ItemClick);
            // 
            // btnSensors
            // 
            this.btnSensors.Caption = "Sensorid";
            this.btnSensors.Id = 7;
            this.btnSensors.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSensors.ImageOptions.Image")));
            this.btnSensors.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnSensors.ImageOptions.LargeImage")));
            this.btnSensors.Name = "btnSensors";
            this.btnSensors.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSensors_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Kvaliteedikontroll";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemStartControl);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItemSelectProduct);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Kontroll";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRibbonProducts);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRibbonQualityClasses);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRibbonSystemSettings);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRibbonLaserSettings);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnSensors);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Seaded";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.layoutControl3);
            this.groupControl1.Location = new System.Drawing.Point(12, 148);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(736, 131);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Toode";
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.lblSelectedProductInfo);
            this.layoutControl3.Controls.Add(this.btnSelectProduct);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(2, 20);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(732, 109);
            this.layoutControl3.TabIndex = 2;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // lblSelectedProductInfo
            // 
            this.lblSelectedProductInfo.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblSelectedProductInfo.Appearance.Options.UseFont = true;
            this.lblSelectedProductInfo.Location = new System.Drawing.Point(12, 72);
            this.lblSelectedProductInfo.Name = "lblSelectedProductInfo";
            this.lblSelectedProductInfo.Size = new System.Drawing.Size(708, 25);
            this.lblSelectedProductInfo.StyleController = this.layoutControl3;
            this.lblSelectedProductInfo.TabIndex = 4;
            // 
            // btnSelectProduct
            // 
            this.btnSelectProduct.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.btnSelectProduct.Appearance.Options.UseFont = true;
            this.btnSelectProduct.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectProduct.Image")));
            this.btnSelectProduct.Location = new System.Drawing.Point(12, 12);
            this.btnSelectProduct.Name = "btnSelectProduct";
            this.btnSelectProduct.Size = new System.Drawing.Size(708, 56);
            this.btnSelectProduct.StyleController = this.layoutControl3;
            this.btnSelectProduct.TabIndex = 1;
            this.btnSelectProduct.Text = "Vali kontrollitav toode";
            this.btnSelectProduct.Click += new System.EventHandler(this.btnSelectProduct_Click);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem16});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(732, 109);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnSelectProduct;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(209, 42);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(712, 60);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.lblSelectedProductInfo;
            this.layoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(712, 29);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.layoutControl1);
            this.groupControl2.Location = new System.Drawing.Point(12, 285);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(736, 443);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Viimase toote kontrolli tulemus";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lblLaser3Info);
            this.layoutControl1.Controls.Add(this.lblLaser1Info);
            this.layoutControl1.Controls.Add(this.lblLaser2Info);
            this.layoutControl1.Controls.Add(this.pictureEditLaser3);
            this.layoutControl1.Controls.Add(this.pictureEditLaser1);
            this.layoutControl1.Controls.Add(this.pictureEditLaser2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(2, 20);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1057, 478, 450, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(732, 421);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lblLaser3Info
            // 
            this.lblLaser3Info.Location = new System.Drawing.Point(12, 396);
            this.lblLaser3Info.Name = "lblLaser3Info";
            this.lblLaser3Info.Size = new System.Drawing.Size(708, 13);
            this.lblLaser3Info.StyleController = this.layoutControl1;
            this.lblLaser3Info.TabIndex = 9;
            // 
            // lblLaser1Info
            // 
            this.lblLaser1Info.Location = new System.Drawing.Point(12, 287);
            this.lblLaser1Info.Name = "lblLaser1Info";
            this.lblLaser1Info.Size = new System.Drawing.Size(708, 13);
            this.lblLaser1Info.StyleController = this.layoutControl1;
            this.lblLaser1Info.TabIndex = 8;
            // 
            // lblLaser2Info
            // 
            this.lblLaser2Info.Location = new System.Drawing.Point(12, 145);
            this.lblLaser2Info.Name = "lblLaser2Info";
            this.lblLaser2Info.Size = new System.Drawing.Size(708, 13);
            this.lblLaser2Info.StyleController = this.layoutControl1;
            this.lblLaser2Info.TabIndex = 7;
            // 
            // pictureEditLaser3
            // 
            this.pictureEditLaser3.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEditLaser3.Location = new System.Drawing.Point(47, 304);
            this.pictureEditLaser3.MenuManager = this.ribbonControl1;
            this.pictureEditLaser3.Name = "pictureEditLaser3";
            this.pictureEditLaser3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEditLaser3.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEditLaser3.Size = new System.Drawing.Size(673, 88);
            this.pictureEditLaser3.StyleController = this.layoutControl1;
            this.pictureEditLaser3.TabIndex = 6;
            // 
            // pictureEditLaser1
            // 
            this.pictureEditLaser1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEditLaser1.Location = new System.Drawing.Point(47, 162);
            this.pictureEditLaser1.MenuManager = this.ribbonControl1;
            this.pictureEditLaser1.Name = "pictureEditLaser1";
            this.pictureEditLaser1.Properties.ReadOnly = true;
            this.pictureEditLaser1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEditLaser1.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEditLaser1.Size = new System.Drawing.Size(673, 121);
            this.pictureEditLaser1.StyleController = this.layoutControl1;
            this.pictureEditLaser1.TabIndex = 5;
            // 
            // pictureEditLaser2
            // 
            this.pictureEditLaser2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEditLaser2.Location = new System.Drawing.Point(47, 12);
            this.pictureEditLaser2.MenuManager = this.ribbonControl1;
            this.pictureEditLaser2.Name = "pictureEditLaser2";
            this.pictureEditLaser2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEditLaser2.Properties.ZoomAccelerationFactor = 1D;
            this.pictureEditLaser2.Size = new System.Drawing.Size(673, 129);
            this.pictureEditLaser2.StyleController = this.layoutControl1;
            this.pictureEditLaser2.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemLaser2,
            this.layoutControlItemLaser1,
            this.layoutControlItemLaser3,
            this.layoutControlItem3,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(732, 421);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemLaser2
            // 
            this.layoutControlItemLaser2.Control = this.pictureEditLaser2;
            this.layoutControlItemLaser2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLaser2.Name = "layoutControlItemLaser2";
            this.layoutControlItemLaser2.Size = new System.Drawing.Size(712, 133);
            this.layoutControlItemLaser2.Text = "Laser2";
            this.layoutControlItemLaser2.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutControlItemLaser1
            // 
            this.layoutControlItemLaser1.Control = this.pictureEditLaser1;
            this.layoutControlItemLaser1.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItemLaser1.Name = "layoutControlItemLaser1";
            this.layoutControlItemLaser1.Size = new System.Drawing.Size(712, 125);
            this.layoutControlItemLaser1.Text = "Laser1";
            this.layoutControlItemLaser1.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutControlItemLaser3
            // 
            this.layoutControlItemLaser3.Control = this.pictureEditLaser3;
            this.layoutControlItemLaser3.Location = new System.Drawing.Point(0, 292);
            this.layoutControlItemLaser3.Name = "layoutControlItemLaser3";
            this.layoutControlItemLaser3.Size = new System.Drawing.Size(712, 92);
            this.layoutControlItemLaser3.Text = "Laser3";
            this.layoutControlItemLaser3.TextSize = new System.Drawing.Size(32, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblLaser2Info;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 133);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(712, 17);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.lblLaser1Info;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 275);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(712, 17);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lblLaser3Info;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 384);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(712, 17);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.layoutControl2);
            this.groupControl3.Location = new System.Drawing.Point(752, 148);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(377, 580);
            this.groupControl3.TabIndex = 8;
            this.groupControl3.Text = "Staatus";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.labelControlError);
            this.layoutControl2.Controls.Add(this.lblTime);
            this.layoutControl2.Controls.Add(this.gridControl1);
            this.layoutControl2.Controls.Add(this.btnSaveStatistics);
            this.layoutControl2.Controls.Add(this.lblFinalQulity);
            this.layoutControl2.Controls.Add(this.btnClearStatistics);
            this.layoutControl2.Controls.Add(this.lblCorrect);
            this.layoutControl2.Controls.Add(this.lblFixable);
            this.layoutControl2.Controls.Add(this.lblNotFixable);
            this.layoutControl2.Controls.Add(this.lblTotalCount);
            this.layoutControl2.Controls.Add(this.labelControlStartDateTime);
            this.layoutControl2.Controls.Add(this.labelControlControlStarted);
            this.layoutControl2.Controls.Add(this.btnStartControl);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(2, 20);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1438, 380, 450, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(373, 558);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // labelControlError
            // 
            this.labelControlError.Location = new System.Drawing.Point(12, 77);
            this.labelControlError.Name = "labelControlError";
            this.labelControlError.Size = new System.Drawing.Size(349, 13);
            this.labelControlError.StyleController = this.layoutControl2;
            this.labelControlError.TabIndex = 15;
            // 
            // lblTime
            // 
            this.lblTime.Location = new System.Drawing.Point(174, 255);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(187, 13);
            this.lblTime.StyleController = this.layoutControl2;
            this.lblTime.TabIndex = 14;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bindingSourceDefects;
            this.gridControl1.Location = new System.Drawing.Point(12, 373);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbonControl1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(349, 121);
            this.gridControl1.TabIndex = 13;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // btnSaveStatistics
            // 
            this.btnSaveStatistics.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.btnSaveStatistics.Appearance.Options.UseFont = true;
            this.btnSaveStatistics.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveStatistics.Image")));
            this.btnSaveStatistics.Location = new System.Drawing.Point(12, 508);
            this.btnSaveStatistics.Name = "btnSaveStatistics";
            this.btnSaveStatistics.Size = new System.Drawing.Size(349, 38);
            this.btnSaveStatistics.StyleController = this.layoutControl2;
            this.btnSaveStatistics.TabIndex = 12;
            this.btnSaveStatistics.Text = "Salvesta";
            this.btnSaveStatistics.Click += new System.EventHandler(this.btnSaveStatistics_Click);
            // 
            // lblFinalQulity
            // 
            this.lblFinalQulity.Appearance.Font = new System.Drawing.Font("Tahoma", 14F);
            this.lblFinalQulity.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblFinalQulity.Appearance.Options.UseFont = true;
            this.lblFinalQulity.Appearance.Options.UseForeColor = true;
            this.lblFinalQulity.Location = new System.Drawing.Point(174, 346);
            this.lblFinalQulity.Name = "lblFinalQulity";
            this.lblFinalQulity.Size = new System.Drawing.Size(94, 23);
            this.lblFinalQulity.StyleController = this.layoutControl2;
            this.lblFinalQulity.TabIndex = 11;
            this.lblFinalQulity.Text = "Määramata";
            // 
            // btnClearStatistics
            // 
            this.btnClearStatistics.Appearance.Font = new System.Drawing.Font("Tahoma", 13F);
            this.btnClearStatistics.Appearance.Options.UseFont = true;
            this.btnClearStatistics.Image = ((System.Drawing.Image)(resources.GetObject("btnClearStatistics.Image")));
            this.btnClearStatistics.Location = new System.Drawing.Point(12, 272);
            this.btnClearStatistics.Name = "btnClearStatistics";
            this.btnClearStatistics.Size = new System.Drawing.Size(349, 38);
            this.btnClearStatistics.StyleController = this.layoutControl2;
            this.btnClearStatistics.TabIndex = 10;
            this.btnClearStatistics.Text = "Nullli statistika";
            this.btnClearStatistics.Click += new System.EventHandler(this.btnClearStatistics_Click);
            // 
            // lblCorrect
            // 
            this.lblCorrect.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblCorrect.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblCorrect.Appearance.Options.UseFont = true;
            this.lblCorrect.Appearance.Options.UseForeColor = true;
            this.lblCorrect.Location = new System.Drawing.Point(174, 232);
            this.lblCorrect.Name = "lblCorrect";
            this.lblCorrect.Size = new System.Drawing.Size(10, 19);
            this.lblCorrect.StyleController = this.layoutControl2;
            this.lblCorrect.TabIndex = 9;
            this.lblCorrect.Text = "0";
            // 
            // lblFixable
            // 
            this.lblFixable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblFixable.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblFixable.Appearance.Options.UseFont = true;
            this.lblFixable.Appearance.Options.UseForeColor = true;
            this.lblFixable.Location = new System.Drawing.Point(174, 209);
            this.lblFixable.Name = "lblFixable";
            this.lblFixable.Size = new System.Drawing.Size(10, 19);
            this.lblFixable.StyleController = this.layoutControl2;
            this.lblFixable.TabIndex = 8;
            this.lblFixable.Text = "0";
            // 
            // lblNotFixable
            // 
            this.lblNotFixable.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblNotFixable.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblNotFixable.Appearance.Options.UseFont = true;
            this.lblNotFixable.Appearance.Options.UseForeColor = true;
            this.lblNotFixable.Location = new System.Drawing.Point(174, 186);
            this.lblNotFixable.Name = "lblNotFixable";
            this.lblNotFixable.Size = new System.Drawing.Size(10, 19);
            this.lblNotFixable.StyleController = this.layoutControl2;
            this.lblNotFixable.TabIndex = 7;
            this.lblNotFixable.Text = "0";
            // 
            // lblTotalCount
            // 
            this.lblTotalCount.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTotalCount.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblTotalCount.Appearance.Options.UseFont = true;
            this.lblTotalCount.Appearance.Options.UseForeColor = true;
            this.lblTotalCount.Location = new System.Drawing.Point(174, 150);
            this.lblTotalCount.Name = "lblTotalCount";
            this.lblTotalCount.Size = new System.Drawing.Size(10, 19);
            this.lblTotalCount.StyleController = this.layoutControl2;
            this.lblTotalCount.TabIndex = 6;
            this.lblTotalCount.Text = "0";
            // 
            // labelControlStartDateTime
            // 
            this.labelControlStartDateTime.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.labelControlStartDateTime.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelControlStartDateTime.Appearance.Options.UseFont = true;
            this.labelControlStartDateTime.Appearance.Options.UseForeColor = true;
            this.labelControlStartDateTime.AutoEllipsis = true;
            this.labelControlStartDateTime.Location = new System.Drawing.Point(174, 112);
            this.labelControlStartDateTime.Name = "labelControlStartDateTime";
            this.labelControlStartDateTime.Size = new System.Drawing.Size(36, 19);
            this.labelControlStartDateTime.StyleController = this.layoutControl2;
            this.labelControlStartDateTime.TabIndex = 5;
            this.labelControlStartDateTime.Text = "0:00";
            // 
            // labelControlControlStarted
            // 
            this.labelControlControlStarted.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.labelControlControlStarted.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelControlControlStarted.Appearance.Options.UseFont = true;
            this.labelControlControlStarted.Appearance.Options.UseForeColor = true;
            this.labelControlControlStarted.Location = new System.Drawing.Point(174, 54);
            this.labelControlControlStarted.Name = "labelControlControlStarted";
            this.labelControlControlStarted.Size = new System.Drawing.Size(15, 19);
            this.labelControlControlStarted.StyleController = this.layoutControl2;
            this.labelControlControlStarted.TabIndex = 4;
            this.labelControlControlStarted.Text = "Ei";
            // 
            // btnStartControl
            // 
            this.btnStartControl.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.btnStartControl.Appearance.Options.UseFont = true;
            this.btnStartControl.Image = ((System.Drawing.Image)(resources.GetObject("btnStartControl.Image")));
            this.btnStartControl.Location = new System.Drawing.Point(12, 12);
            this.btnStartControl.Name = "btnStartControl";
            this.btnStartControl.Size = new System.Drawing.Size(349, 38);
            this.btnStartControl.StyleController = this.layoutControl2;
            this.btnStartControl.TabIndex = 2;
            this.btnStartControl.Text = "Käivita kontroll";
            this.btnStartControl.Click += new System.EventHandler(this.btnStartControl_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItemNotFixable,
            this.layoutControlItemFixable,
            this.layoutControlItemCorrect,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.layoutControlItem11,
            this.emptySpaceItem4,
            this.layoutControlItem12,
            this.emptySpaceItem5,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(373, 558);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnStartControl;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(353, 42);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 486);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(353, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.labelControlControlStarted;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItem5.Text = "Kontroll käivitatud:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(159, 19);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.labelControlStartDateTime;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 100);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItem6.Text = "Kontrolli algus:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(159, 19);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.lblTotalCount;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItem7.Text = "Kontrollitud arv:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(159, 19);
            // 
            // layoutControlItemNotFixable
            // 
            this.layoutControlItemNotFixable.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItemNotFixable.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemNotFixable.Control = this.lblNotFixable;
            this.layoutControlItemNotFixable.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItemNotFixable.Name = "layoutControlItemNotFixable";
            this.layoutControlItemNotFixable.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItemNotFixable.Text = "Millest praak:";
            this.layoutControlItemNotFixable.TextSize = new System.Drawing.Size(159, 19);
            // 
            // layoutControlItemFixable
            // 
            this.layoutControlItemFixable.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItemFixable.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemFixable.Control = this.lblFixable;
            this.layoutControlItemFixable.Location = new System.Drawing.Point(0, 197);
            this.layoutControlItemFixable.Name = "layoutControlItemFixable";
            this.layoutControlItemFixable.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItemFixable.Text = "Millest parandatav:";
            this.layoutControlItemFixable.TextSize = new System.Drawing.Size(159, 19);
            // 
            // layoutControlItemCorrect
            // 
            this.layoutControlItemCorrect.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.layoutControlItemCorrect.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemCorrect.Control = this.lblCorrect;
            this.layoutControlItemCorrect.Location = new System.Drawing.Point(0, 220);
            this.layoutControlItemCorrect.Name = "layoutControlItemCorrect";
            this.layoutControlItemCorrect.Size = new System.Drawing.Size(353, 23);
            this.layoutControlItemCorrect.Text = "Millest korrektne:";
            this.layoutControlItemCorrect.TextSize = new System.Drawing.Size(159, 19);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 82);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(353, 18);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 123);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(353, 15);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnClearStatistics;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 260);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(353, 42);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 161);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(353, 13);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.lblFinalQulity;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 334);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(353, 27);
            this.layoutControlItem12.Text = "Kvaliteet:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(159, 23);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 302);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(353, 32);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnSaveStatistics;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 496);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(353, 42);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.gridControl1;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 361);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(353, 125);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lblTime;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 243);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(353, 17);
            this.layoutControlItem2.Text = "Aeg:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(159, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControlError;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(353, 17);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 740);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kvaliteedikontroll";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditLaser2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLaser3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDefects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNotFixable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemFixable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCorrect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemStartControl;
        private DevExpress.XtraBars.BarButtonItem barButtonItemSelectProduct;
        private DevExpress.XtraBars.BarButtonItem btnRibbonProducts;
        private DevExpress.XtraBars.BarButtonItem btnRibbonQualityClasses;
        private DevExpress.XtraBars.BarButtonItem btnRibbonSystemSettings;
        private DevExpress.XtraBars.BarButtonItem btnRibbonLaserSettings;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton btnSelectProduct;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton btnClearStatistics;
        private DevExpress.XtraEditors.LabelControl lblCorrect;
        private DevExpress.XtraEditors.LabelControl lblFixable;
        private DevExpress.XtraEditors.LabelControl lblNotFixable;
        private DevExpress.XtraEditors.LabelControl lblTotalCount;
        private DevExpress.XtraEditors.LabelControl labelControlStartDateTime;
        private DevExpress.XtraEditors.LabelControl labelControlControlStarted;
        private DevExpress.XtraEditors.SimpleButton btnStartControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNotFixable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemFixable;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCorrect;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnSaveStatistics;
        private DevExpress.XtraEditors.LabelControl lblFinalQulity;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.LabelControl lblSelectedProductInfo;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraBars.BarButtonItem btnSensors;
        private DevExpress.XtraEditors.PictureEdit pictureEditLaser2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser2;
        private DevExpress.XtraEditors.LabelControl lblTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.PictureEdit pictureEditLaser1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser1;
        private DevExpress.XtraEditors.PictureEdit pictureEditLaser3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLaser3;
        private System.Windows.Forms.BindingSource bindingSourceDefects;
        private DevExpress.XtraEditors.LabelControl labelControlError;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LabelControl lblLaser3Info;
        private DevExpress.XtraEditors.LabelControl lblLaser1Info;
        private DevExpress.XtraEditors.LabelControl lblLaser2Info;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
    }
}

