﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CSharp
{
    public class CLLTI
    {
        //Sample interface class with all needed functions for the example
        //Note not all functions of the LLT.dll are in this interface.
        //Please add the needed functions.

        #region "Constant declarations"

        public const string DRIVER_DLL_NAME = "LLT.dll";

        public const uint CONVERT_X = 0x00000800;
        public const uint CONVERT_Z = 0x00001000;
        public const uint CONVERT_WIDTH = 0x00000100;
        public const uint CONVERT_MAXIMUM = 0x00000200;
        public const uint CONVERT_THRESHOLD = 0x00000400;
        public const uint CONVERT_M0 = 0x00002000;
        public const uint CONVERT_M1 = 0x00004000;

        //specify the type for the RS422-multiplexer (27xx)
        public const int RS422_INTERFACE_FUNCTION_AUTO_27xx = 0;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_115200_27xx = 1;
        public const int RS422_INTERFACE_FUNCTION_TRIGGER_27xx = 2;
        public const int RS422_INTERFACE_FUNCTION_CMM_TRIGGER_27xx = 3;
        public const int RS422_INTERFACE_FUNCTION_ENCODER_27xx = 4;
        public const int RS422_INTERFACE_FUNCTION_DIGITAL_OUTPUT_27xx = 6;
        public const int RS422_INTERFACE_FUNCTION_TRIGGER_LASER_PULSE_27xx = 7;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_57600_27xx = 8;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_38400_27xx = 9;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_19200_27xx = 10;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_9600_27xx = 11;

		//specify the type for the RS422-multiplexer (26xx/29xx)
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_115200 = 0;
		public const int RS422_INTERFACE_FUNCTION_SERIALPORT_57600 = 1;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_38400 = 2;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_19200 = 3;
        public const int RS422_INTERFACE_FUNCTION_SERIALPORT_9600 = 4;
        public const int RS422_INTERFACE_FUNCTION_TRIGGER_INPUT = 5;
		public const int RS422_INTERFACE_FUNCTION_TRIGGER_OUTPUT = 6;
        public const int RS422_INTERFACE_FUNCTION_CMM_TRIGGER = 7;
		
        #endregion

        #region "Return Values"

        //Message-Values
        public const int ERROR_OK = 0;
        public const int ERROR_SERIAL_COMM = 1;
        public const int ERROR_SERIAL_LLT = 7;
        public const int ERROR_CONNECTIONLOST = 10;
        public const int ERROR_STOPSAVING = 100;

        //Return-Values for the is-functions
        public const int IS_FUNC_NO = 0;
        public const int IS_FUNC_YES = 1;

        //General return-values for all functions
        public const int GENERAL_FUNCTION_DEVICE_NAME_NOT_SUPPORTED = 4;
        public const int GENERAL_FUNCTION_PACKET_SIZE_CHANGED = 3;
        public const int GENERAL_FUNCTION_CONTAINER_MODE_HEIGHT_CHANGED = 2;
        public const int GENERAL_FUNCTION_OK = 1;
        public const int GENERAL_FUNCTION_NOT_AVAILABLE = 0;

        public const int ERROR_GENERAL_WHILE_LOAD_PROFILE = -1000;
        public const int ERROR_GENERAL_NOT_CONNECTED = -1001;
        public const int ERROR_GENERAL_DEVICE_BUSY = -1002;
        public const int ERROR_GENERAL_WHILE_LOAD_PROFILE_OR_GET_PROFILES = -1003;
        public const int ERROR_GENERAL_WHILE_GET_PROFILES = -1004;
        public const int ERROR_GENERAL_GET_SET_ADDRESS = -1005;
        public const int ERROR_GENERAL_POINTER_MISSING = -1006;
        public const int ERROR_GENERAL_WHILE_SAVE_PROFILES = -1007;
        public const int ERROR_GENERAL_SECOND_CONNECTION_TO_LLT = -1008;

        //Return-Values for GetDeviceName
        public const int ERROR_GETDEVICENAME_SIZE_TOO_LOW = -1;
        public const int ERROR_GETDEVICENAME_NO_BUFFER = -2;

        //Return-Values for Load/SaveProfiles
        public const int ERROR_LOADSAVE_WRITING_LAST_BUFFER = -50;
        public const int ERROR_LOADSAVE_WHILE_SAVE_PROFILE = -51;
        public const int ERROR_LOADSAVE_NO_PROFILELENGTH_POINTER = -52;
        public const int ERROR_LOADSAVE_NO_LOAD_PROFILE = -53;
        public const int ERROR_LOADSAVE_STOP_ALREADY_LOAD = -54;
        public const int ERROR_LOADSAVE_CANT_OPEN_FILE = -55;
        public const int ERROR_LOADSAVE_FILE_POSITION_TOO_HIGH = -57;
        public const int ERROR_LOADSAVE_AVI_NOT_SUPPORTED = -58;
        public const int ERROR_LOADSAVE_NO_REARRANGEMENT_POINTER = -59;
        public const int ERROR_LOADSAVE_WRONG_PROFILE_CONFIG = -60;
        public const int ERROR_LOADSAVE_NOT_TRANSFERING = -61;

        //Return-Values for profile transfer functions
        public const int ERROR_PROFTRANS_SHOTS_NOT_ACTIVE = -100;
        public const int ERROR_PROFTRANS_SHOTS_COUNT_TOO_HIGH = -101;
        public const int ERROR_PROFTRANS_WRONG_PROFILE_CONFIG = -102;
        public const int ERROR_PROFTRANS_FILE_EOF = -103;
        public const int ERROR_PROFTRANS_NO_NEW_PROFILE = -104;
        public const int ERROR_PROFTRANS_BUFFER_SIZE_TOO_LOW = -105;
        public const int ERROR_PROFTRANS_NO_PROFILE_TRANSFER = -106;
        public const int ERROR_PROFTRANS_PACKET_SIZE_TOO_HIGH = -107;
        public const int ERROR_PROFTRANS_CREATE_BUFFERS = -108;
        public const int ERROR_PROFTRANS_WRONG_PACKET_SIZE_FOR_CONTAINER = -109;
        public const int ERROR_PROFTRANS_REFLECTION_NUMBER_TOO_HIGH = -110;
        public const int ERROR_PROFTRANS_MULTIPLE_SHOTS_ACTIVE = -111;
        public const int ERROR_PROFTRANS_BUFFER_HANDOUT = -112;
        public const int ERROR_PROFTRANS_WRONG_BUFFER_POINTER = -113;
        public const int ERROR_PROFTRANS_WRONG_TRANSFER_CONFIG = -114;

        //Return-Values for Set/GetFunctions
        public const int ERROR_SETGETFUNCTIONS_WRONG_BUFFER_COUNT = -150;
        public const int ERROR_SETGETFUNCTIONS_PACKET_SIZE = -151;
        public const int ERROR_SETGETFUNCTIONS_WRONG_PROFILE_CONFIG = -152;
        public const int ERROR_SETGETFUNCTIONS_NOT_SUPPORTED_RESOLUTION = -153;
        public const int ERROR_SETGETFUNCTIONS_REFLECTION_NUMBER_TOO_HIGH = -154;
        public const int ERROR_SETGETFUNCTIONS_WRONG_FEATURE_ADRESS = -155;
        public const int ERROR_SETGETFUNCTIONS_SIZE_TOO_LOW = -156;
        public const int ERROR_SETGETFUNCTIONS_WRONG_PROFILE_SIZE = -157;
        public const int ERROR_SETGETFUNCTIONS_MOD_4 = -158;
        public const int ERROR_SETGETFUNCTIONS_REARRANGEMENT_PROFILE = -159;
        public const int ERROR_SETGETFUNCTIONS_USER_MODE_TOO_HIGH = -160;
        public const int ERROR_SETGETFUNCTIONS_USER_MODE_FACTORY_DEFAULT = -161;
        public const int ERROR_SETGETFUNCTIONS_HEARTBEAT_TOO_HIGH = -162;


        //Return-Values PostProcessingParameter
        public const int ERROR_POSTPROCESSING_NO_PROF_BUFFER = -200;
        public const int ERROR_POSTPROCESSING_MOD_4 = -201;
        public const int ERROR_POSTPROCESSING_NO_RESULT = -202;
        public const int ERROR_POSTPROCESSING_LOW_BUFFERSIZE = -203;
        public const int ERROR_POSTPROCESSING_WRONG_RESULT_SIZE = -204;

        //Return-Values for GetDeviceInterfaces
        public const int ERROR_GETDEVINTERFACES_WIN_NOT_SUPPORTED = -250;
        public const int ERROR_GETDEVINTERFACES_REQUEST_COUNT = -251;
        public const int ERROR_GETDEVINTERFACES_CONNECTED = -252;
        public const int ERROR_GETDEVINTERFACES_INTERNAL = -253;

        //Return-Values for Connect
        public const int ERROR_CONNECT_LLT_COUNT = -300;
        public const int ERROR_CONNECT_SELECTED_LLT = -301;
        public const int ERROR_CONNECT_ALREADY_CONNECTED = -302;
        public const int ERROR_CONNECT_LLT_NUMBER_ALREADY_USED = -303;
        public const int ERROR_CONNECT_SERIAL_CONNECTION = -304;
        public const int ERROR_CONNECT_INVALID_IP = -305;

        //Return-Values for SetPartialProfile
        public const int ERROR_PARTPROFILE_NO_PART_PROF = -350;
        public const int ERROR_PARTPROFILE_TOO_MUCH_BYTES = -351;
        public const int ERROR_PARTPROFILE_TOO_MUCH_POINTS = -352;
        public const int ERROR_PARTPROFILE_NO_POINT_COUNT = -353;
        public const int ERROR_PARTPROFILE_NOT_MOD_UNITSIZE_POINT = -354;
        public const int ERROR_PARTPROFILE_NOT_MOD_UNITSIZE_DATA = -355;

        //Return-Values for Start/StopTransmissionAndCmmTrigger
        public const int ERROR_CMMTRIGGER_NO_DIVISOR = -400;
        public const int ERROR_CMMTRIGGER_TIMEOUT_AFTER_TRANSFERPROFILES = -401;
        public const int ERROR_CMMTRIGGER_TIMEOUT_AFTER_SETCMMTRIGGER = -402;

        //Return-Values for TranslateErrorValue
        public const int ERROR_TRANSERRORVALUE_WRONG_ERROR_VALUE = -450;
        public const int ERROR_TRANSERRORVALUE_BUFFER_SIZE_TOO_LOW = -451;

        //Read/write config functions
        public const int ERROR_READWRITECONFIG_CANT_CREATE_FILE = -500;
        public const int ERROR_READWRITECONFIG_CANT_OPEN_FILE = -501;
        public const int ERROR_READWRITECONFIG_QUEUE_TO_SMALL = -502;

        #endregion

        #region "Registers"
        //Function defines for the Get/SetFeature function
        public const uint FEATURE_FUNCTION_SERIAL = 0xf0000410;
        public const uint FEATURE_FUNCTION_LASERPOWER = 0xf0f00824;
        public const uint INQUIRY_FUNCTION_LASERPOWER = 0xf0f00524;
        public const uint FEATURE_FUNCTION_MEASURINGFIELD = 0xf0f00880;
        public const uint INQUIRY_FUNCTION_MEASURINGFIELD = 0xf0f00580;
        public const uint FEATURE_FUNCTION_TRIGGER = 0xf0f00830;
        public const uint INQUIRY_FUNCTION_TRIGGER = 0xf0f00530;
        public const uint FEATURE_FUNCTION_SHUTTERTIME = 0xf0f0081c;
        public const uint INQUIRY_FUNCTION_SHUTTERTIME = 0xf0f0051c;
        public const uint FEATURE_FUNCTION_IDLETIME = 0xf0f00800;
        public const uint INQUIRY_FUNCTION_IDLETIME = 0xf0f00500;
        public const uint FEATURE_FUNCTION_PROCESSING_PROFILEDATA = 0xf0f00804;
        public const uint INQUIRY_FUNCTION_PROCESSING_PROFILEDATA = 0xf0f00504;
        public const uint FEATURE_FUNCTION_THRESHOLD = 0xf0f00810;
        public const uint INQUIRY_FUNCTION_THRESHOLD = 0xf0f00510;
        public const uint FEATURE_FUNCTION_MAINTENANCEFUNCTIONS = 0xf0f0088c;
        public const uint INQUIRY_FUNCTION_MAINTENANCEFUNCTIONS = 0xf0f0058c;
        public const uint FEATURE_FUNCTION_ANALOGFREQUENCY = 0xf0f00828;
        public const uint INQUIRY_FUNCTION_ANALOGFREQUENCY = 0xf0f00528;
        public const uint FEATURE_FUNCTION_ANALOGOUTPUTMODES = 0xf0f00820;
        public const uint INQUIRY_FUNCTION_ANALOGOUTPUTMODES = 0xf0f00520;
        public const uint FEATURE_FUNCTION_CMMTRIGGER = 0xf0f00888;
        public const uint INQUIRY_FUNCTION_CMMTRIGGER = 0xf0f00588;
        public const uint FEATURE_FUNCTION_REARRANGEMENT_PROFILE = 0xf0f0080c;
        public const uint INQUIRY_FUNCTION_REARRANGEMENT_PROFILE = 0xf0f0050c;
        public const uint FEATURE_FUNCTION_PROFILE_FILTER = 0xf0f00818;
        public const uint INQUIRY_FUNCTION_PROFILE_FILTER = 0xf0f00518;
        public const uint FEATURE_FUNCTION_RS422_INTERFACE_FUNCTION = 0xf0f008c0;
        public const uint INQUIRY_FUNCTION_RS422_INTERFACE_FUNCTION = 0xf0f005c0;

        public const uint FEATURE_FUNCTION_SATURATION = 0xf0f00814;
        public const uint INQUIRY_FUNCTION_SATURATION = 0xf0f00514;
        public const uint FEATURE_FUNCTION_TEMPERATURE = 0xf0f0082c;
        public const uint INQUIRY_FUNCTION_TEMPERATURE = 0xf0f0052c;
        public const uint FEATURE_FUNCTION_CAPTURE_QUALITY = 0xf0f008c4;
        public const uint INQUIRY_FUNCTION_CAPTURE_QUALITY = 0xf0f005c4;
        public const uint FEATURE_FUNCTION_SHARPNESS = 0xf0f00808;
        public const uint INQUIRY_FUNCTION_SHARPNESS = 0xf0f00508;

        public const uint FEATURE_FUNCTION_PACKET_DELAY = 0x00000d08;
        public const uint FEATURE_FUNCTION_CONNECTION_SPEED = 0x00000670;

        #endregion

        #region "Enums"

        //specifies the interface type for CreateLLTDevice and IsInterfaceType
        public enum TInterfaceType
        {
            INTF_TYPE_UNKNOWN = 0,
            INTF_TYPE_SERIAL = 1,
            INTF_TYPE_FIREWIRE = 2,
            INTF_TYPE_ETHERNET = 3
        }

        //specify the callback type
        //if you programming language don't support enums, you can use a signed int
        public enum TCallbackType
        {
            STD_CALL = 0,
            C_DECL = 1,
        }

        //specify the type of the scanner
        //if you programming language don't support enums, you can use a signed int
        public enum TScannerType
        {

            StandardType = -1,                    //can't decode scanCONTROL name use standard measurmentrange
            LLT25 = 0,                           //scanCONTROL28xx with 25mm measurmentrange
            LLT100 = 1,                           //scanCONTROL28xx with 100mm measurmentrange

            scanCONTROL28xx_25 = 0,              //scanCONTROL28xx with 25mm measurmentrange
            scanCONTROL28xx_100 = 1,              //scanCONTROL28xx with 100mm measurmentrange
            scanCONTROL28xx_10 = 2,              //scanCONTROL28xx with 10mm measurmentrange
            scanCONTROL28xx_xxx = 999,            //scanCONTROL28xx with no measurmentrange -> use standard measurmentrange

            scanCONTROL27xx_25 = 1000,           //scanCONTROL27xx with 25mm measurmentrange
            scanCONTROL27xx_100 = 1001,           //scanCONTROL27xx with 100mm measurmentrange
            scanCONTROL27xx_50 = 1002,           //scanCONTROL27xx with 50mm measurmentrange
            scanCONTROL2751_25 = 1020,           //scanCONTROL27xx with 25mm measurmentrange
            scanCONTROL2751_100 = 1021,           //scanCONTROL27xx with 100mm measurmentrange
            scanCONTROL2702_50 = 1032,           //scanCONTROL2702 with 50mm measurement range
            scanCONTROL27xx_xxx = 1999,           //scanCONTROL27xx with no measurmentrange -> use standard measurmentrange

            scanCONTROL26xx_25 = 2000,           //scanCONTROL26xx with 25mm measurmentrange
            scanCONTROL26xx_100 = 2001,           //scanCONTROL26xx with 100mm measurmentrange
            scanCONTROL26xx_50 = 2002,           //scanCONTROL26xx with 50mm measurmentrange
            scanCONTROL2651_25 = 2020,           //scanCONTROL26xx with 25mm measurmentrange
            scanCONTROL2651_100 = 2021,           //scanCONTROL26xx with 100mm measurmentrange
            scanCONTROL2602_50 = 2032,           //scanCONTROL2602 with 50mm measurement range
            scanCONTROL26xx_xxx = 2999,           //scanCONTROL26xx with no measurmentrange -> use standard measurmentrange

            scanCONTROL29xx_25 = 3000,           //scanCONTROL29xx with 25mm measurmentrange
            scanCONTROL29xx_100 = 3001,           //scanCONTROL29xx with 100mm measurmentrange
            scanCONTROL29xx_50 = 3002,           //scanCONTROL29xx with 50mm measurmentrange
            scanCONTROL2951_25 = 3020,           //scanCONTROL29xx with 25mm measurmentrange
            scanCONTROL2951_100 = 3021,           //scanCONTROL29xx with 100mm measurmentrange
            scanCONTROL2902_50 = 3032,           //scanCONTROL2902 with 50mm measurement range
            scanCONTROL2953_30 = 3033,           //scanCONTROL2953 with 30mm measurement range
            scanCONTROL29xx_xxx = 3999,           //scanCONTROL29xx with no measurmentrange -> use standard measurmentrange
            scanCONTROL2xxx = 4000,           //scanCONTROL2xxx with 100mm measurmentrange

        }

        //specify the profile configuration
        //if you programming language don't support enums, you can use a signed int
        public enum TProfileConfig
        {
            NONE = 0,
            PROFILE = 1,
            CONTAINER = 1,
            VIDEO_IMAGE = 1,
            PURE_PROFILE = 2,
            QUARTER_PROFILE = 3,
            CSV_PROFILE = 4,
            PARTIAL_PROFILE = 5,
        }

        public enum TTransferProfileType
        {
            NORMAL_TRANSFER = 0,
            SHOT_TRANSFER = 1,
            NORMAL_CONTAINER_MODE = 2,
            SHOT_CONTAINER_MODE = 3,
            NONE_TRANSFER = 4,
        }

        public struct TPartialProfile
        {
            public uint nStartPoint;
            public uint nStartPointData;
            public uint nPointCount;
            public uint nPointDataWidth;
        }

        public enum TTransferVideoType
        {
            VIDEO_MODE_0 = 0,
            VIDEO_MODE_1 = 1,
            NONE_VIDEOMODE = 2,
        }

        public enum TFileType
        {
            AVI = 0,
            LLT = 1,
            CSV = 2,
            BMP = 3,
            CSV_NEG = 4,
        }

        public unsafe delegate void ProfileReceiveMethod(byte *data, uint iSize, uint pUserData);

        #endregion

        #region "DLL references"

        // Instance functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_CreateLLTDevice",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern uint CreateLLTDevice(TInterfaceType iInterfaceType);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_CreateLLTFirewire",
             CallingConvention = CallingConvention.StdCall)]
        internal static extern uint CreateLLTFirewire();

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_CreateLLTSerial",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern uint CreateLLTSerial();

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetInterfaceType",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetInterfaceType(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_DelDevice",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int DelDevice(uint pLLT);

        // Connect functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_Connect",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int Connect(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_Disconnect",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int Disconnect(uint pLLT);

        // Write config functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ExportLLTConfig",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int ExportLLTConfig(uint pLLT, StringBuilder pFileName);

        // Device interface functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetDeviceInterfaces",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetDeviceInterfaces(uint pLLT, uint[] pInterfaces, int nSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetDeviceInterfacesFast",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetDeviceInterfacesFast(uint pLLT, uint[] pInterfaces, int nSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetDeviceInterface",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetDeviceInterface(uint pLLT, uint nInterface, int nAdditional);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetDiscoveryBroadcastTarget",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern uint GetDiscoveryBroadcastTarget(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetDiscoveryBroadcastTarget",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetDiscoveryBroadcastTarget(uint pLLT, uint nNetworkAddress, uint nSubnetMask);

        // scanControl identification functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetDeviceName",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetDeviceName(uint pLLT, StringBuilder sbDevName, int nDevNameSize, StringBuilder sbVenName, int nVenNameSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetLLTVersions",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetLLTVersions(uint pLLT, ref uint pDSP, ref uint pFPGA1, ref uint pFPGA2);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetLLTType",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetLLTType(uint pLLT, ref TScannerType ScannerType);

        //Get functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetMinMaxPacketSize",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetMinMaxPacketSize(uint pLLT, ref ulong pMinPacketSize, ref ulong pMaxPacketSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetResolutions",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetResolutions(uint pLLT, uint[] pValue, int nSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetFeature",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetFeature(uint pLLT, uint Function, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetBufferCount",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetBufferCount(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetMainReflection",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetMainReflection(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetMaxFileSize",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetMaxFileSize(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetPacketSize",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetPacketSize(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetFirewireConnectionSpeed",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetFirewireConnectionSpeed(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetProfileConfig",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetProfileConfig(uint pLLT, ref TProfileConfig pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetResolution",
             CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetResolution(uint pLLT, ref uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetProfileContainerSize",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetProfileContainerSize(uint pLLT, ref uint pWidth, ref uint pHeight);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetMaxProfileContainerSize",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetMaxProfileContainerSize(uint pLLT, ref uint pMaxWidth, ref uint pMaxHeight);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetEthernetHeartbeatTimeout",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetEthernetHeartbeatTimeout(uint pLLT, ref uint pValue);

        //Set functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetFeature",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetFeature(uint pLLT, uint Function, uint Value);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetResolution",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetResolution(uint pLLT, uint Value);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetProfileConfig",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetProfileConfig(uint pLLT, TProfileConfig Value);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetBufferCount",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetBufferCount(uint pLLT, uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetMainReflection",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetMainReflection(uint pLLT, uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetMaxFileSize",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetMaxFileSize(uint pLLT, uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetPacketSize",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetPacketSize(uint pLLT, uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetFirewireConnectionSpeed",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetFirewireConnectionSpeed(uint pLLT, uint pValue);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetProfileContainerSize",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetProfileContainerSize(uint pLLT, uint nWidth, uint nHeight);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetEthernetHeartbeatTimeout",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetEthernetHeartbeatTimeout(uint pLLT, uint pValue);

        //Register functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_RegisterCallback",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int RegisterCallback(uint pLLT, TCallbackType tCallbackType, ProfileReceiveMethod tReceiveProfiles, uint pUserData);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_RegisterErrorMsg",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int RegisterErrorMsg(uint pLLT, uint Msg, IntPtr hWnd, UIntPtr WParam);

        // Profile transfer functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_TransferProfiles",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int TransferProfiles(uint pLLT, TTransferProfileType TransferProfileType, int nEnable);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetProfile",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetProfile(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_TransferVideoStream",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int TransferVideoStream(uint pLLT, TTransferVideoType TransferVideoType, int nEnable, ref uint pWidth, ref uint pHeight);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_MultiShot",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int MultiShot(uint pLLT, uint nCount);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetActualProfile",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetActualProfile(uint pLLT, byte[] pBuffer, int nBuffersize,
          TProfileConfig ProfileConfig, ref uint pLostProfiles);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ConvertProfile2Values",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int ConvertProfile2Values(uint pLLT, byte[] pProfile, uint nResolution,
          TProfileConfig ProfileConfig, TScannerType ScannerType, uint nReflection, int nConvertToMM,
          ushort[] pWidth, ushort[] pMaximum, ushort[] pThreshold, double[] pX, double[] pZ,
          uint[] pM0, uint[] pM1);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ConvertPartProfile2Values",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int ConvertPartProfile2Values(uint pLLT, byte[] pProfile, ref TPartialProfile ProfileConfig, TScannerType ScannerType, uint nReflection, int nConvertToMM,
          ushort[] pWidth, ushort[] pMaximum, ushort[] pThreshold, double[] pX, double[] pZ,
          uint[] pM0, uint[] pM1);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetHoldBuffersForPolling",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetHoldBuffersForPolling(uint pLLT, uint uiHoldBuffersForPolling);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetHoldBuffersForPolling",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetHoldBuffersForPolling(uint pLLT, ref uint puiHoldBuffersForPolling);

        //Is functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_IsInterfaceType",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int IsInterfaceType(uint pLLT, int iInterfaceType);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_IsFirewire",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int IsFirewire(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_IsSerial",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int IsSerial(uint pLLT);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_IsTransferingProfiles",
            CallingConvention = CallingConvention.StdCall)]
        internal static extern int IsTransferingProfiles(uint pLLT);

        //PartialProfile functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetPartialProfileUnitSize",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetPartialProfileUnitSize(uint pLLT, ref uint pUnitSizePoint, ref uint pUnitSizePointData);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetPartialProfile",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetPartialProfile(uint pLLT, ref TPartialProfile pPartialProfile);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SetPartialProfile",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int SetPartialProfile(uint pLLT, ref TPartialProfile pPartialProfile);

        //Timestamp convert functions,
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_Timestamp2CmmTriggerAndInCounter",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int Timestamp2CmmTriggerAndInCounter(byte[] pBuffer, ref uint pInCounter, ref int pCmmTrigger, ref int pCmmActive, ref uint pCmmCount);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_Timestamp2TimeAndCount",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int Timestamp2TimeAndCount(byte[] pBuffer, ref double dTimeShutterOpen, ref double dTimeShutterClose, ref uint uiProfileCount);

        //PostProcessing functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ReadPostProcessingParameter",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int ReadPostProcessingParameter(uint pLLT, ref uint pParameter, uint nSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_WritePostProcessingParameter",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int WritePostProcessingParameter(uint pLLT, ref uint pParameter, uint nSize);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ConvertProfile2ModuleResult",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int ConvertProfile2ModuleResult(uint pLLT, byte[] pProfileBuffer, uint nProfileBufferSize, byte[] pModuleResultBuffer, uint nResultBufferSize, ref TPartialProfile pPartialProfile /* = NULL*/);

        //Load/Save functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_LoadProfiles",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int LoadProfiles(uint pLLT, StringBuilder pFilename, ref TPartialProfile pPartialProfile, ref TProfileConfig pProfileConfig, ref TScannerType pScannerType, ref uint pRearrengementProfile);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SaveProfiles",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int SaveProfiles(uint pLLT, StringBuilder pFilename, TFileType FileType);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_LoadProfilesGetPos",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int LoadProfilesGetPos(uint pLLT, ref uint pActualPosition, ref uint pMaxPosition);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_LoadProfilesSetPos",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int LoadProfilesSetPos(uint pLLT, uint nNewPosition);

        //Special CMM trigger functions
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_StartTransmissionAndCmmTrigger",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int StartTransmissionAndCmmTrigger(uint pLLT, uint nCmmTrigger, TTransferProfileType TransferProfileType, uint nProfilesForerun, StringBuilder pFilename, TFileType FileType, uint Timeout);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_StopTransmissionAndCmmTrigger",
           CallingConvention = CallingConvention.StdCall)]
        internal static extern int StopTransmissionAndCmmTrigger(uint pLLT, int nCmmTriggerPolarity, uint nTimeout);

        // Converts a error-value in a string 
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_TranslateErrorValue",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int TranslateErrorValue(uint pLLT, int ErrorValue, byte[] pString, int nStringSize);

        //User mode
        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_GetActualUserMode",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int GetActualUserMode(uint pLLT, ref uint pActualUserMode, ref uint pUserModeCount);

        [DllImport(DRIVER_DLL_NAME, EntryPoint = "s_ReadWriteUserModes",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int ReadWriteUserModes(uint pLLT, int nWrite, uint nUserMode);
		
		[DllImport(DRIVER_DLL_NAME, EntryPoint = "s_TriggerProfile",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int TriggerProfile(uint pLLT);
		
		[DllImport(DRIVER_DLL_NAME, EntryPoint = "s_SaveGlobalParameter",
          CallingConvention = CallingConvention.StdCall)]
        internal static extern int SaveGlobalParameter(uint pLLT);

        #endregion

        // constructor
        public CLLTI()
        {

        }
    }
}
