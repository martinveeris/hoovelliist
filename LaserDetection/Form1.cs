﻿using DevExpress.XtraEditors;
using LaserDetection.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection
{
    public partial class Form1 : Form
    {
        Data.DataObjects.QualityClass.CalculatedDefectClass[] defectListFoSave = null;
        public Form1()
        {
            InitializeComponent();

            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
        }

        private void InitLasers()
        {
            layoutControlItemLaser1.Text = Data.Lasers.GetName(Constants.Lasers.LaserTop);
            layoutControlItemLaser2.Text = Data.Lasers.GetName(Constants.Lasers.LaserLeft);
            layoutControlItemLaser3.Text = Data.Lasers.GetName(Constants.Lasers.LaserRight);
        }

        private void SetButtonStates(bool isControlRunning)
        {
            // check if product is selected
            if (Common.SystemState.SelectedProductID.HasValue)
            {

                // get product
                var product = Data.Products.Get(Common.SystemState.SelectedProductID.Value);
                if (product != null)
                {
                    lblSelectedProductInfo.Text = string.Format("Toode \"{0}\" valitud. Kontrolli alustamiseks vajutage \"Alusta kontrolli\"", product.Name);

                    lblSelectedProductInfo.ForeColor = Color.DarkGreen;

                    // can start control
                    btnStartControl.Enabled = barButtonItemStartControl.Enabled = true;

                    // show/hide laser images
                    foreach (var laserSettingItem in product.ProductLaserSettingList)
                    {
                        if (laserSettingItem.LaserID == Constants.Lasers.LaserTop)
                        {
                            if (laserSettingItem.IsActive)
                            {
                                layoutControlItemLaser1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                                lblLaser1Info.Visible = true;
                            }
                            else
                            {
                                layoutControlItemLaser1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                                lblLaser1Info.Visible = false;
                            }
                        }
                        else if (laserSettingItem.LaserID == Constants.Lasers.LaserRight)
                        {
                            if (laserSettingItem.IsActive)
                            {
                                layoutControlItemLaser3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                                lblLaser3Info.Visible = true;

                            }
                            else
                            {
                                layoutControlItemLaser3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                                lblLaser3Info.Visible = false;

                            }
                        }
                        else if (laserSettingItem.LaserID == Constants.Lasers.LaserLeft)
                        {
                            if (laserSettingItem.IsActive)
                            {
                                layoutControlItemLaser2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                                lblLaser2Info.Visible = true;
                            }
                            else
                            {
                                layoutControlItemLaser2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                                lblLaser2Info.Visible = false;
                            }
                        }

                    }

                    layoutControlItemNotFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    layoutControlItemFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                    layoutControlItemCorrect.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

                    foreach (var qualityItem in product.QualityClassList)
                    {
                        if (qualityItem.QualityClassID == Common.Constants.QualityClasses.NotFixable)
                        {
                            layoutControlItemNotFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            layoutControlItemNotFixable.Text = string.Format("Millest: {0}", qualityItem.Name);
                        }

                        if (qualityItem.QualityClassID == Common.Constants.QualityClasses.Fixable)
                        {
                            layoutControlItemFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            layoutControlItemFixable.Text = string.Format("Millest: {0}", qualityItem.Name);
                        }

                        if (qualityItem.QualityClassID == Common.Constants.QualityClasses.Correct)
                        {
                            layoutControlItemCorrect.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                            layoutControlItemCorrect.Text = string.Format("Millest: {0}", qualityItem.Name);

                        }
                    }
                }

            }
            else
            {
                btnStartControl.Enabled = barButtonItemStartControl.Enabled = false;
                lblSelectedProductInfo.Text = "Toode valimata. Kontrolli alustamiseks valige toode.";
                lblSelectedProductInfo.ForeColor = Color.DarkRed;

                layoutControlItemNotFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItemFixable.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                layoutControlItemCorrect.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            // check if control is running
            if (isControlRunning)
            {
                btnStartControl.Text = barButtonItemStartControl.Caption = "Peata kontroll";
                btnSelectProduct.Enabled = barButtonItemSelectProduct.Enabled = false;
            }
            else
            {
                btnStartControl.Text = barButtonItemStartControl.Caption = "Käivita kontroll";
                btnSelectProduct.Enabled = barButtonItemSelectProduct.Enabled = true;
            }
        }

        private void btnRibbonSystemSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Forms.SystemSettingsForm newForm = new Forms.SystemSettingsForm();
            newForm.ShowDialog();
        }

        private void btnRibbonLaserSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Forms.LaserSettingForm newForm = new Forms.LaserSettingForm();
            if (newForm.ShowDialog() == DialogResult.OK)
                InitLasers();
        }

        private void btnRibbonQualityClasses_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Forms.QualityClassesListForm newForm = new Forms.QualityClassesListForm();
            if (newForm.ShowDialog() == DialogResult.OK)
            {
                SetButtonStates(false);
            }
        }

        private void btnRibbonProducts_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Forms.ProductListForm newForm = new Forms.ProductListForm();
            if (newForm.ShowDialog() == DialogResult.OK)
            { }
        }

        private void btnSelectProduct_Click(object sender, EventArgs e)
        {

            SelectProduct();
        }

        private void btnStartControl_Click(object sender, EventArgs e)
        {
            StartControl();
        }

        private void barButtonItemSelectProduct_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SelectProduct();
        }

        private void barButtonItemStartControl_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StartControl();
        }

        private void SelectProduct()
        {
            // Select product
            Forms.ProductSelectForm newForm = new Forms.ProductSelectForm();
            if (newForm.ShowDialog() == DialogResult.OK)
            {
                SetButtonStates(Common.SystemState.IsControlRunning);
            }
        }

        private void StartControl()
        {
            if (Common.SystemState.IsControlRunning)
            {
                SetButtonStates(false);

                Common.SystemState.DataProcessingCompleteEvent -= SystemState_DataReceivedEvent;
                Common.SystemState.ProcessingCompleteEvent -= SystemState_ProcessingCompleteEvent;
                Common.SystemState.StopControl();
            }
            else
            {
                SetButtonStates(true);

                Common.SystemState.DataProcessingCompleteEvent += SystemState_DataReceivedEvent;
                Common.SystemState.ProcessingCompleteEvent += SystemState_ProcessingCompleteEvent;
                Common.SystemState.StartControl();
                labelControlError.Text = "";
            }
        }

        private void SystemState_ProcessingCompleteEvent(string message)
        {
            StartControl();
            labelControlError.Text = message;
        }

        private void SystemState_DataReceivedEvent(Laser.DataProcessingClass dataProcessingClass)
        {

            if (layoutControlItemLaser1.Visible)
            {
                Bitmap bmp = dataProcessingClass.GetLaserImage(Common.Constants.Lasers.LaserTop);
                lock (bmp)
                {
                    SetLabelText(lblLaser1Info, string.Format("w:{0},h:{1}", bmp.Width, bmp.Height));
                    pictureEditLaser1.InvokeIfRequired(a => { ((PictureEdit)a).Image = bmp; });
                }
            }

            if (layoutControlItemLaser2.Visible)
            {
                Bitmap bmp = dataProcessingClass.GetLaserImage(Common.Constants.Lasers.LaserLeft);
                lock (bmp)
                {
                    SetLabelText(lblLaser2Info, string.Format("w:{0},h:{1}", bmp.Width, bmp.Height));

                    pictureEditLaser2.InvokeIfRequired(a => { ((PictureEdit)a).Image = bmp; });
                }
            }

            if (layoutControlItemLaser3.Visible)
            {
                Bitmap bmp = dataProcessingClass.GetLaserImage(Common.Constants.Lasers.LaserRight);
                lock (bmp)
                {
                    SetLabelText(lblLaser3Info, string.Format("w:{0},h:{1}", bmp.Width, bmp.Height));

                    pictureEditLaser3.InvokeIfRequired(a => { ((PictureEdit)a).Image = bmp; });
                }
            }

            SetLabelText(lblFinalQulity, dataProcessingClass.CalculatedQuality.Name);

            UpdateStatistics(dataProcessingClass);

        }

        private void UpdateStatistics(Laser.DataProcessingClass dataProcessingClass)
        {
            if (SystemState.CurrentStatistics == null)
                return;

            SetLabelText(lblTime, string.Format("{0}", SystemState.CurrentStatistics.DataPackageUnprocessedCount.ToString()));


            SetLabelText(labelControlStartDateTime, SystemState.CurrentStatistics.ControlStartDateTime.ToString());

            if (SystemState.IsControlRunning)
                SetLabelText(labelControlControlStarted, string.Format("{0}", "Jah"));
            else
                SetLabelText(labelControlControlStarted, string.Format("{0}", "Ei"));


            SetLabelText(lblTotalCount, string.Format("{0}", SystemState.CurrentStatistics.TotalDetailCount));
            SetLabelText(lblCorrect, string.Format("{0}", SystemState.CurrentStatistics.CorrectTotalDetailCount));
            SetLabelText(lblFixable, string.Format("{0}", SystemState.CurrentStatistics.FixableTotalCount));
            SetLabelText(lblNotFixable, string.Format("{0}", SystemState.CurrentStatistics.NotFixableTotalDetailCount));

            // uuenda kvaliteedi näitajaid UI-s
            if(dataProcessingClass != null)
            {
                
                // teisest threadist uuendamise vastu
                    this.Invoke((MethodInvoker)delegate
                   {
                       bindingSourceDefects.DataSource = dataProcessingClass.CalculatedQuality.CalculatedDefectList;
                   });
              
            } else
            {
                bindingSourceDefects.DataSource = null;
            }
        }

        private void SetLabelText(LabelControl control, string value)
        {
            control.InvokeIfRequired(a => { control.Text = string.Format("{0}", value); });

        }

        private void btnSensors_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Forms.SensorForm newForm = new Forms.SensorForm();
            if(newForm.ShowDialog() == DialogResult.OK)
            {

            }
        }

        private void btnClearStatistics_Click(object sender, EventArgs e)
        {
            if (SystemState.CurrentStatistics != null)
                SystemState.CurrentStatistics.ResetCounters();

            UpdateStatistics(null);

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            InitLasers();

            SetButtonStates(false);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void SaveStatistics()
        {

        }

        private void btnSaveStatistics_Click(object sender, EventArgs e)
        {
            //StringBuilder sb = new StringBuilder();
            //foreach (var item in defectListFoSave)
            //{
            //    sb.AppendLine(string.Format("{0},{1}{2}{3}", item.DefectName, item.Width, item.Height, item.Length));
            //}

            //var saveStatisticsPath = Data.Settings.GetSettingValueString(Common.Constants.Settings.SaveStatisticsPath)+"/stats.txt";

            //File.WriteAllText(saveStatisticsPath, sb.ToString());
        }
    }
}
