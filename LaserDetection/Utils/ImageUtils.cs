﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaserDetection.Utils
{
    public class ImageUtils
    {
        public static void GetImageFromProfile(PictureBox pictureBox, double[] zValues, double[] xValues)
        {
            var g = pictureBox.CreateGraphics();

            int height = pictureBox.Height;
            int width = pictureBox.Width;

            double average = zValues.Where(a => a > 0).Average();
            double maxX = xValues.Max();
            double minX = xValues.Min();
            double totalWidthmm = maxX + Math.Abs(minX);

            double fitWidthKoef = width / totalWidthmm;

            List<Point> pointList = new List<Point>();

            for (int i = 0; i < xValues.Count(); i++)
            {
                var zValue = zValues[i];
                var xValue = xValues[i];

                if (zValue == 0)
                    continue;

                pointList.Add(new Point((int)((xValue+ maxX)/ fitWidthKoef), (int)(zValue/ fitWidthKoef)));
            }

            g.DrawLines(Pens.Red, pointList.ToArray());
        }

        public static Bitmap GetImageFromLaserClass(List<Laser.LaserDataClass> dataClassList)
        {
            if (dataClassList.Count == 0)
                return null;

            var dataCount = dataClassList.SelectMany(d => d.dataList).Count();
            var reso = dataClassList.First().ProductLaserSetting.Resolution;

            Emgu.CV.Image<Gray, byte> newImage = new Emgu.CV.Image<Gray, byte>(dataCount, (int)reso);
            Emgu.CV.Image<Gray, double> newImag2e = new Emgu.CV.Image<Gray, double>(dataCount, (int)reso);

            int columnIndex = 0;
            for (int k = 0; k < dataClassList.Count; k++)
            {
                if (dataClassList[k].ProfileDataStatus == Laser.LaserDataClass.ProfileDataStatusEnum.Empty)
                    return null;

                double min = 0;
                double max = 0;

                //Parallel.For(0, dataClass.dataList.Count, i =>
                for (int i = 0; i < dataClassList[k].dataList.Count; i++)
                {
                    //min = dataClassList[k].dataList[i].ImageData.Min();
                    //max = dataClassList[k].dataList[i].ImageData.Max();

                    for (int j = 0; j < dataClassList[k].dataList[i].ImageData.Length; j++)
                    {
                        //if(j == 0)
                        //System.Diagnostics.Debug.WriteLine(dataClass.dataList[i].AdValueZ[j]);
                        newImage[j, columnIndex] = new Gray(dataClassList[k].dataList[i].ImageData[j]);// new Gray(scale(dataClassList[k].dataList[i].ImageData[j], min, max, 0, 255));

                    }
                    columnIndex++;

                }
                //});
            }
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //var contours = newImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST);

            //List<Contour<Point>> result = new List<Contour<Point>>();
            //while (contours != null)
            //{
            //    result.Add(contours);
            //    newImage.Draw(contours, new Gray(300), 1);
            //    contours = contours.HNext;
            //}
            //sw.Stop();

            return newImage.Bitmap;

           

            //Emgu.CV.Image<Gray, double> newIma = new Emgu.CV.Image<Gray, double>(ff.Data);

            //var xMin = item.adValueX.Min();

            //System.Collections.ObjectModel.Collection<NationalInstruments.Vision.PointContour> linePoints = new System.Collections.ObjectModel.Collection<NationalInstruments.Vision.PointContour>();

            //for (int i = 0; i < item.adValueX.Length; i++)
            //{
            //    var zValue = item.adValueZ[i];
            //    var wValue = item.adValueW[i];
            //    var xValue = Math.Abs(xMin + item.adValueX[i]);
            //    var alphaValue = (int)scale(zValue, min, max, 0, 255);
        }

        public static Emgu.CV.Image<Bgr, double> GetImageFromLaserClassMatrix(List<Laser.LaserDataClass> dataClassList, ref List<Contour<Point>> foundContours)
        {
            if (dataClassList.Count == 0)
            {
                return null;
            }


            var reso = dataClassList.First().ProductLaserSetting.Resolution;

            Matrix<double> imageMatrix = new Matrix<double>((int)reso, 1);

            foreach (var item in dataClassList)
            {
                imageMatrix = imageMatrix.ConcateHorizontal(item.iDataMatrix);
            }

            Emgu.CV.Image<Gray, double> newImage = new Emgu.CV.Image<Gray, double>(imageMatrix.Width, imageMatrix.Height);

            CvInvoke.cvConvert(imageMatrix, newImage);

            Emgu.CV.Image<Bgr, double> colorImage = new Image<Bgr, double>(newImage.Size);


            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            //CvInvoke.cvCvtColor(newImage, newImage, Emgu.CV.CvEnum.COLOR_CONVERSION.)
            var contours = newImage.Convert<Gray, byte>().FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST);

            foundContours = new List<Contour<Point>>();
            while (contours != null)
            {
                foundContours.Add(contours);

                if (newImage[contours[0].Y, contours[0].X].Intensity == Common.Constants.ImageHeight.Peak)
                    colorImage.Draw(contours, new Bgr(Color.Red), 2);
                else
                    colorImage.Draw(contours, new Bgr(Color.Green), 2);

                contours = contours.HNext;
            }
            //sw.Stop();

            return colorImage;



            //Emgu.CV.Image<Gray, double> newIma = new Emgu.CV.Image<Gray, double>(ff.Data);

            //var xMin = item.adValueX.Min();

            //System.Collections.ObjectModel.Collection<NationalInstruments.Vision.PointContour> linePoints = new System.Collections.ObjectModel.Collection<NationalInstruments.Vision.PointContour>();

            //for (int i = 0; i < item.adValueX.Length; i++)
            //{
            //    var zValue = item.adValueZ[i];
            //    var wValue = item.adValueW[i];
            //    var xValue = Math.Abs(xMin + item.adValueX[i]);
            //    var alphaValue = (int)scale(zValue, min, max, 0, 255);
        }

        public static double scale(double valueIn, double baseMin, double baseMax, double limitMin, double limitMax)
        {
            return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
        }

        private void ProcessUsingLockbitsAndUnsafeAndParallel(Bitmap processedBitmap)
        {
            unsafe
            {
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;
                    }
                });
                processedBitmap.UnlockBits(bitmapData);
            }
        }
    }
}
