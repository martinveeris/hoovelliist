﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Utils
{
    public class MathUtils
    {
        public static double[] GetFittedCurve(double[] xData, double[] zData, int curveOrder, double[] baseCurve)
        {
            // kurvi Y väärtused, kas on vaja muuta baas Z numbrit?
            var newZData = Fit(xData, baseCurve);

            return newZData;
        }

        public static double[] GetFittedCurve(double[] xData, double[] zData, int curveOrder, out double[] curve)
        {
            curve = null;

            // algoritm nõuab min 11 punkti
            if (zData.Count() < 13)
            {
                return zData;
            }

            var curve2 = PolynomialFit.LeastSquares.FitPolynomal(xData, zData, curveOrder, PolynomialFit.CalcMethod.CPU2_SIMD_COMPAT);
            //var curve2 = MathNet.Numerics.Fit.Polynomial(xData, zData, curveOrder);

            curve = curve2; // tagasta ka välja arvutatud funktsioon

            // kurvi Y väärtused
            var newZData = Fit(xData, curve2);

            return newZData;
        }

        public static double[] GetFittedLine(double[] xData, double[] zData)
        {
            // joon
            var line2 = MathNet.Numerics.Fit.Line(xData, zData);

            double[] y = new double[xData.Length];
            for (int i = 0; i < xData.Length; i++)
            {
                y[i] = line2.Item2 * xData[i] + line2.Item1;
            }

            return y;
        }

        public static double[] CalculateDifference(double[] dataToCheck, double[] idealData, double tolerance, bool cutFirstAndLastPoint)
        {
            double[] y = new double[dataToCheck.Length];

            for (int i = 0; i < dataToCheck.Length; i++)
            {
                double diff = dataToCheck[i] - idealData[i];

                if (diff > tolerance) // ülevalpool piiri
                    y[i] = Common.Constants.ImageHeight.Peak;
                else if (diff < -tolerance) // allpool piiri
                    y[i] = Common.Constants.ImageHeight.Hole;
                else
                    y[i] = Common.Constants.ImageHeight.Normal; // tolerantsi piires
            }

            if(cutFirstAndLastPoint && y.Length > 0)
            {
                y[0] = 0;
                y[y.Length-1] = 0;
            }

            return y;
        }

       

        private static double[] Fit(double[] x, double[] Coeff)
        {
            double[] y = new double[x.Length];
            int pos = 0;

            foreach (double xval in x)
            {
                double xcoeff = 1;
                foreach (double coeffval in Coeff)
                {
                    // multiply current x by a coefficient
                    y[pos] += coeffval * xcoeff;
                    // power up the X
                    xcoeff *= xval;
                }
                pos++;
            }

            return y;
        }
    }
}
