//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LaserDetection
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductDefectSettings
    {
        public System.Guid ProductDefectSettingID { get; set; }
        public System.Guid ProductDefectID { get; set; }
        public byte[] Version { get; set; }
        public System.Guid DefectParameterID { get; set; }
        public Nullable<double> FromValue { get; set; }
        public Nullable<double> ToValue { get; set; }
        public bool IsActive { get; set; }
    
        public virtual DefectParameters DefectParameters { get; set; }
        public virtual ProductDefects ProductDefects { get; set; }
    }
}
