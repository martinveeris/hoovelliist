﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class Settings
    {
        static List<SystemSettings> cachedSettings = new List<SystemSettings>();

        private static void AddCachedSetting(SystemSettings setting)
        {
            var existingSetting = cachedSettings.Where(a => a.SystemSettingsID == setting.SystemSettingsID).FirstOrDefault();
            if (existingSetting != null)
                cachedSettings.Remove(existingSetting);
            cachedSettings.Add(setting);
        }

        private static SystemSettings GetCachedSetting(string SettingKey)
        {
            return cachedSettings.Where(a => a.Settingkey == SettingKey).FirstOrDefault();
        }

        internal static string GetSettingValueString(string SettingKey)
        {
            // Check if cached value is available
            var settingValue = GetCachedSetting(SettingKey);

            if (settingValue != null)
                return settingValue.Settingvalue;

            using (DetectionDatabase db = new DetectionDatabase())
            {
                settingValue = db.SystemSettings.Where(a => a.Settingkey == SettingKey).FirstOrDefault();
                if (settingValue != null)
                {
                    AddCachedSetting(settingValue);
                    return settingValue.Settingvalue;
                }
                return string.Empty;
            }
        }

        internal static void SetSettingValueString(string SettingKey, string value)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var settingValue = db.SystemSettings.Where(a => a.Settingkey == SettingKey).FirstOrDefault();
                if (settingValue == null)
                {
                    settingValue = new SystemSettings()
                    {
                        SystemSettingsID = Guid.NewGuid(),
                        Settingkey = SettingKey,
                        Settingvalue = value
                    };
                    db.SystemSettings.Add(settingValue);
                }

                settingValue.Settingvalue = value;
                AddCachedSetting(settingValue);
                db.SaveChanges();
            }
        }

        internal static bool GetSettingValueBoolean(string SettingKey)
        {
            // Check if cached value is available
            var settingValue = GetCachedSetting(SettingKey);

            if(settingValue != null)
                return Convert.ToBoolean(settingValue.Settingvalue);

            using (DetectionDatabase db = new DetectionDatabase())
            {
                settingValue = db.SystemSettings.Where(a => a.Settingkey == SettingKey).FirstOrDefault();
                if (settingValue != null)
                {
                    AddCachedSetting(settingValue);
                    return Convert.ToBoolean(settingValue.Settingvalue);
                }
                return false;
            }
        }

        internal static void SetSettingValueBoolean(string SettingKey, bool value)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var settingValue = db.SystemSettings.Where(a => a.Settingkey == SettingKey).FirstOrDefault();
                if (settingValue == null)
                {
                    settingValue = new SystemSettings()
                    {
                        SystemSettingsID = Guid.NewGuid(),
                        Settingkey = SettingKey,
                        Settingvalue = value.ToString()
                    };
                    db.SystemSettings.Add(settingValue);
                }

                settingValue.Settingvalue = value.ToString();
                AddCachedSetting(settingValue);
                db.SaveChanges();
            }
        }
    }
}
