﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class ProductDefects
    {

        internal static List<DataObjects.ProductDefect> GetList(Guid ProductID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var itemList = GetListQuerable(db);

                return itemList.Where(a => a.ProductID == ProductID).ToList();
            }
        }


        internal static DataObjects.ProductDefect Get(Guid ProductDefectID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var itemList = GetListQuerable(db);

                return itemList.Where(a => a.ProductDefectID == ProductDefectID).FirstOrDefault();
            }
        }


        internal static void Save(DataObjects.ProductDefect entity)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                if(entity.Version == null)
                {
                    var newEntity = new LaserDetection.ProductDefects();
                    newEntity.DefectID = entity.DefectID;
                    newEntity.ProductDefectID = entity.ProductDefectID;
                    newEntity.ProductID = entity.ProductID;
                    newEntity.QualityClassID = entity.QualityClassID;

                    foreach (var item in entity.ProductDefectSettingList)
                    {
                        newEntity.ProductDefectSettings.Add(new ProductDefectSettings()
                        {
                            ProductDefectSettingID = item.ProductDefectSettingID,
                            DefectParameterID = item.DefectParameterID,
                            FromValue = item.FromValue,
                            ToValue = item.ToValue,
                            ProductDefectID = item.ProductDefectID,
                            IsActive = item.IsActive
                        });
                    }

                    db.ProductDefects.Add(newEntity);
                }
                else
                {
                    // update existing
                    var dbItem = db.ProductDefects.Where(a => a.ProductDefectID == entity.ProductDefectID).FirstOrDefault();
                    if(dbItem != null)
                    {
                        dbItem.QualityClassID = entity.QualityClassID;

                        foreach (var settingItem in entity.ProductDefectSettingList)
                        {
                            var dbSettingItem = dbItem.ProductDefectSettings.Where(a => a.ProductDefectSettingID == settingItem.ProductDefectSettingID).FirstOrDefault();
                            if(dbSettingItem != null)
                            {
                                dbSettingItem.IsActive = settingItem.IsActive;
                                dbSettingItem.FromValue = settingItem.FromValue;
                                dbSettingItem.ToValue = settingItem.ToValue;
                            }
                        }
                    }

                }

                db.SaveChanges();
            }
        }

        private static IQueryable<DataObjects.ProductDefect> GetListQuerable(DetectionDatabase db)
        {
            var itemList = from l in db.ProductDefects
                           select new DataObjects.ProductDefect()
                           {
                               ProductDefectID = l.ProductDefectID,
                               DefectID = l.DefectID,
                               ProductID = l.ProductID,
                               QualityClassID = l.QualityClassID,
                               Version = l.Version,
                               DefectName = l.Defects.Name,
                               QualityClassName = l.QualityClasses.Name,
                               ProductDefectSettingList = l.ProductDefectSettings.Select(a => new DataObjects.ProductDefectSetting()
                               {
                                   ProductDefectSettingID = a.ProductDefectSettingID,
                                   DefectParameterID = a.DefectParameterID,
                                   ProductDefectID = a.ProductDefectID,
                                   FromValue = a.FromValue,
                                   ToValue = a.ToValue,
                                   Version = a.Version,
                                   IsActive = a.IsActive,
                                   ParameterName = a.DefectParameters.Name
                               }).ToList()
                           };

            return itemList;
        }
    }
}