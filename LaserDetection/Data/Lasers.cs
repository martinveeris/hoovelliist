﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class Lasers
    {
        internal static string GetName(Guid LaserID)
        {
            try
            {
                using (DetectionDatabase db = new DetectionDatabase())
                {
                    var laser = db.Lasers.Where(a => a.LaserID == LaserID).FirstOrDefault();
                    if (laser != null)
                    {
                        return laser.Name;
                    }
                    return string.Empty;
                }
            } catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }

            return string.Empty;
        }

        internal static List<DataObjects.Laser> GetList()
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserList = from l in db.Lasers
                                select new DataObjects.Laser()
                                {
                                    LaserID = l.LaserID,
                                    Name = l.Name,
                                    ExposureTime = l.ExposureTime,
                                    IPAddress = l.IPAddress
                                };

                return laserList.ToList();
            }
        }

        internal static DataObjects.Laser Get(Guid LaserID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserList = from l in db.Lasers.Where(a => a.LaserID == LaserID)
                                select new DataObjects.Laser()
                                {
                                    LaserID = l.LaserID,
                                    Name = l.Name,
                                    ExposureTime = l.ExposureTime,
                                    IPAddress = l.IPAddress
                                };

                return laserList.FirstOrDefault();
            }
        }

        internal static void Save(DataObjects.Laser laser)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserItem = db.Lasers.Where(a => a.LaserID == laser.LaserID).FirstOrDefault();
                if(laserItem != null)
                {
                    laserItem.Name = laser.Name;
                    laserItem.IPAddress = laser.IPAddress;
                    laserItem.ExposureTime = laser.ExposureTime;

                    db.SaveChanges();
                }
            }
        }
    }
}