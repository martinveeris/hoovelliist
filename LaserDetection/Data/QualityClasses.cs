﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class QualityClasses
    {
        internal static List<DataObjects.QualityClass> GetList()
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserList = GetListQuerable(db);

                return laserList.ToList();
            }
        }

        internal static DataObjects.QualityClass Get(Guid QualityClassID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserList = GetListQuerable(db);

                return laserList.Where(a => a.QualityClassID == QualityClassID).FirstOrDefault();
            }
        }

        private static IQueryable<DataObjects.QualityClass> GetListQuerable(DetectionDatabase db)
        {

            var laserList = from l in db.QualityClasses
                            select new DataObjects.QualityClass()
                            {
                                QualityClassID = l.QualityClassID,
                                Name = l.Name,
                                SignalOutputISO = l.SignalOutputISO
                            };

            return laserList;

        }

        internal static void Save(DataObjects.QualityClass qualityClass)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var qualitytem = db.QualityClasses.Where(a => a.QualityClassID == qualityClass.QualityClassID).FirstOrDefault();
                if(qualitytem == null)
                {
                    qualitytem = new LaserDetection.QualityClasses()
                    {
                        QualityClassID = Guid.NewGuid()
                    };
                    db.QualityClasses.Add(qualitytem);
                }

                qualitytem.Name = qualityClass.Name;
                qualitytem.SignalOutputISO = qualityClass.SignalOutputISO;

                db.SaveChanges();

            }
        }

        internal static void Delete(DataObjects.QualityClass entity)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var foundEntity = db.QualityClasses.Where(a => a.QualityClassID == entity.QualityClassID).FirstOrDefault();
                if (foundEntity != null)
                {
                    db.QualityClasses.Remove(foundEntity);
                    db.SaveChanges();
                }
            }
        }
    }
}