﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class Products
    {

        internal static DataObjects.Product Get(Guid ProductID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var list = GetProductListQuerable(db);

                return list.Where(a => a.ProductID == ProductID).FirstOrDefault();
            }
        }

        internal static List<DataObjects.Product> GetList()
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var list = GetProductListQuerable(db);

                return list.ToList();
            }
        }

        private static IQueryable<DataObjects.Product> GetProductListQuerable(DetectionDatabase db)
        {
            var list = from l in db.Products
                       select new DataObjects.Product()
                       {
                           ProductID = l.ProductID,
                           Name = l.Name,
                           Code = l.Code,
                           Width = l.Width,
                           Version = l.Version,
                           QualityClassList = l.QualityClasses.Select(a => new DataObjects.QualityClass()
                           { Name = a.Name, QualityClassID = a.QualityClassID, SignalOutputISO = a.SignalOutputISO }).ToList(),
                           ProductLaserSettingList = l.ProductLaserSettings.Select(a => new DataObjects.ProductLaserSetting()
                           {
                               LaserID = a.LaserID,
                               ProductID = a.ProductID,
                               ProfileCheckEndPoint = a.ProfileCheckEndPoint ?? 0,
                               ProfileDataZ = a.ProfileDataZ,
                               ProfileDataX = a.ProfileDataX,
                               Version = a.Version,
                               IsActive = a.IsActive,
                               IPAddress = a.Lasers.IPAddress,
                               ProfileCheckStartPoint = a.ProfileCheckStartPoint ?? 0
                           }).ToList(),
                           ProductDefectList = l.ProductDefects.Select(b => new DataObjects.ProductDefect()
                           {
                               DefectID = b.DefectID,
                               DefectName = b.Defects.Name,
                               ProductDefectID = b.ProductDefectID,
                               ProductID = b.ProductID,
                               QualityClassID = b.QualityClassID,
                               ProductDefectSettingList = b.ProductDefectSettings.Select(s => new DataObjects.ProductDefectSetting()
                               {
                                   DefectParameterID = s.DefectParameterID,
                                   FromValue = s.FromValue,
                                   IsActive = s.IsActive,
                                   ProductDefectID = s.ProductDefectID,
                                   ProductDefectSettingID = s.ProductDefectSettingID,
                                   ToValue = s.ToValue
                               }).ToList()
                           }).ToList()
                       };

                return list;
        }

        internal static void Save(DataObjects.Product entity)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var item = db.Products.Where(a => a.ProductID == entity.ProductID).FirstOrDefault();
                if(item == null)
                {
                    item = new LaserDetection.Products()
                    {
                         ProductID = Guid.NewGuid()
                    };
                    db.Products.Add(item);
                }

                item.Name = entity.Name;
                item.Code = entity.Code;
                item.Width = entity.Width;

                // check allowed quality classes
                var dbQualityClassList = db.QualityClasses.ToList();
                foreach (var qualityItem in entity.QualityClassList)
                {
                    var existingQualityItem = item.QualityClasses.Where(a => a.QualityClassID == qualityItem.QualityClassID).FirstOrDefault();

                    if (existingQualityItem == null && qualityItem.Checked == false)
                        continue;
                    else if (existingQualityItem != null && qualityItem.Checked == true)
                        continue;
                    else if (existingQualityItem == null && qualityItem.Checked == true)
                    {
                        // add new
                        item.QualityClasses.Add(dbQualityClassList.Where(a => a.QualityClassID == qualityItem.QualityClassID).First());
                    }
                    else if (existingQualityItem != null && qualityItem.Checked == false)
                    {
                        // add new
                        item.QualityClasses.Remove(existingQualityItem);
                    }
                }

                // laser settings
                foreach (var laserSettingItem in entity.ProductLaserSettingList)
                {
                    if(laserSettingItem.Version == null)
                    {
                        // new
                        item.ProductLaserSettings.Add(new ProductLaserSettings()
                        {
                            LaserID = laserSettingItem.LaserID,
                            ProductID = laserSettingItem.ProductID,
                            ProfileCheckStartPoint = laserSettingItem.ProfileCheckStartPoint,
                            ProfileCheckEndPoint = laserSettingItem.ProfileCheckEndPoint,
                            ProfileDataZ = laserSettingItem.ProfileDataZ,
                            ProfileDataX = laserSettingItem.ProfileDataX,
                            IsActive = laserSettingItem.IsActive
                        });
                    } else
                    {
                        var existingSetting = item.ProductLaserSettings.Where(a => a.LaserID == laserSettingItem.LaserID && a.ProductID == laserSettingItem.ProductID).First();
                        existingSetting.IsActive = laserSettingItem.IsActive;
                        existingSetting.ProfileCheckStartPoint = laserSettingItem.ProfileCheckStartPoint;
                        existingSetting.ProfileCheckEndPoint = laserSettingItem.ProfileCheckEndPoint;
                        existingSetting.ProfileDataZ = laserSettingItem.ProfileDataZ;
                        existingSetting.ProfileDataX = laserSettingItem.ProfileDataX;
                    }
                }

                db.SaveChanges();
            }
        }

        internal static void Delete(DataObjects.Product entity)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var foundEntity = db.Products.Where(a => a.ProductID == entity.ProductID).FirstOrDefault();
                if (foundEntity != null)
                {
                    db.Products.Remove(foundEntity);
                    db.SaveChanges();
                }
            }
        }
    }
}