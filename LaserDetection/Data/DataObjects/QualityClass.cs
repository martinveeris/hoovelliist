﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class QualityClass
    {
        public Guid QualityClassID { get; set; }
        public string Name { get; set; }
        public int SignalOutputISO { get; set; }

        /// <summary>
        /// Used in product detail forms to mark items selected
        /// </summary>
        public bool Checked { get; set; }

        public int Order
        {
            get
            {
                if (QualityClassID == Common.Constants.QualityClasses.Correct)
                    return 0;
                if (QualityClassID == Common.Constants.QualityClasses.Fixable)
                    return 1;
                return 2;
            }
        }

        public List<CalculatedDefectClass> CalculatedDefectList { get; set; }

        public class CalculatedDefectClass
        {
            [Browsable(false)]
            public Guid DefectID { get; set; }
            public double Area { get; set; }
            public double Height { get; set; }
            public double Width { get; set; }
            public double Length { get; set; }
            public string DefectName { get; set; }
            public string LaserName { get; set; }
        }
    }
}
