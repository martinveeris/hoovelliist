﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class ProductDefect
    {
        public Guid ProductDefectID { get; set; }
        public Guid ProductID { get; set; }
        public Guid QualityClassID { get; set; }
        public Guid DefectID { get; set; }
        public string DefectName { get; set; }
        public string QualityClassName { get; set; }
        public byte[] Version { get; set; }
       

        public List<ProductDefectSetting> ProductDefectSettingList { get; set; }

        public string DefectSettingsString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var settingItem in ProductDefectSettingList)
                {
                    sb.Append(string.Format("{0}:{1}, ", settingItem.ParameterName, settingItem.ToValue));
                }
                return sb.ToString();
            }
        }
    }
}