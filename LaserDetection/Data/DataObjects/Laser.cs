﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class Laser
    {
        public Guid LaserID { get; set; }
        public string Name { get; set; }
        public string IPAddress { get; set; }
        public decimal ExposureTime { get; set; }
    }
}
