﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class Product
    {
        public Guid ProductID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public double? Width { get; set; }
        public byte[] Version { get; set; }

        public List<QualityClass> QualityClassList { get; set; }
        public List<ProductLaserSetting> ProductLaserSettingList { get; set; }
        public List<ProductDefect> ProductDefectList { get; set; }
    }
}
