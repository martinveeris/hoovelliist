﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class DefectParameter
    {
        public Guid DefectParameterID { get; set; }
        public string Name { get; set; }
        public byte[] Version { get; set; }
    }
}