﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class ProductDefectSetting
    {
        public Guid ProductDefectSettingID { get; set; }
        public Guid ProductDefectID { get; set; }
        public Guid DefectParameterID { get; set; }
        public double? FromValue { get; set; }
        public double? ToValue { get; set; }
        public bool IsActive { get; set; }
        public string ParameterName { get; set; }
        public byte[] Version { get; set; }
    }
}
