﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class ProductLaserSetting
    {
        public Guid LaserID { get; set; }
        public Guid ProductID { get; set; }
        public byte[] ProfileDataX { get; set; }
        public byte[] ProfileDataZ { get; set; }
        public double ProfileCheckStartPoint { get; set; }
        public double ProfileCheckEndPoint { get; set; }
        public bool IsActive { get; set; }
        public string IPAddress { get; set; }
        public byte[] Version { get; set; }

        public uint Resolution { get { return 640; } } // This is readonly value for laser resolution, could be in database as well

        /// <summary>
        /// Profile calculated values like offset and check area width
        /// </summary>
        public ProfileValuesClass ProfileValues { get; set; }


        public void CalculateProfileValues()
        {
            /*
            // -30 -(-25) = -5
            //var hh = this.ProfileDataInPointsX[0] - ProfileCheckStartPoint ?? 0.0;
            //this.ProfileDataInPointsX.FindIndex(a => a > 3);

            ProfileValues = new ProfileValuesClass();

            // calc profile check area width
            ProfileValues.CheckAreaWidthMM = Math.Abs(ProfileCheckStartPoint - ProfileCheckEndPoint);

            // set value in what side the offset is calculated from



            // get start and end Index in X data
            //var indStart = this.ProfileDataInPointsX.FindIndex(s => s >= ProfileCheckStartPoint - 0.1 && s <= ProfileCheckStartPoint + 0.1);
            //var indEnd = this.ProfileDataInPointsX.FindIndex(s => s >= ProfileCheckEndPoint - 0.1 && s <= ProfileCheckEndPoint + 0.1);

            // kui on parem pool, siis otsi kõige väiksem Z väärtus
            // sealt alates siis 
            // esimene väärtustatud Z vasakult
            var leftFirstValueIndex = this.ProfileDataInPointsZ.FindIndex(a => a != 0);
            var leftFirstValue = this.ProfileDataInPointsX[leftFirstValueIndex];

            // nüüd lahuta (telje punkt - kasutaja kriips)
            var offset = Math.Abs(leftFirstValue - ProfileCheckStartPoint);
            ProfileValues.OffsetMM = offset;
            ProfileValues.FirstXValueMM = leftFirstValue; // for using later to shift values


            //var valueStartX = this.ProfileDataInPointsX[indStart];
            //var valueStartZ = this.ProfileDataInPointsZ[indStart];
            //var valueEndX = this.ProfileDataInPointsX[indEnd];
            //var valueEndZ = this.ProfileDataInPointsZ[indEnd];

            // ProfileValues.LeftOffset = Math.Abs(leftFirstValue - ProfileCheckStartPoint);

    */
        }



        /// <summary>
        /// Profile Z coordinate points
        /// </summary>
        public double[] ProfileDataInPointsZ
        {
            get
            {
                if (ProfileDataZ == null)
                    return null;

                return Enumerable.Range(0, ProfileDataZ.Length / sizeof(double))
                    .Select(offset => BitConverter.ToDouble(ProfileDataZ, offset * sizeof(double)))
                    .ToArray();

            }
            set
            {
                var result = new byte[value.Length * sizeof(double)];
                Buffer.BlockCopy(value, 0, result, 0, result.Length);
                ProfileDataZ =  result;
            }
        }

        /// <summary>
        /// Profile X coordinate points
        /// </summary>
        public double[] ProfileDataInPointsX
        {
            get
            {
                if (ProfileDataX == null)
                    return null;

                return Enumerable.Range(0, ProfileDataX.Length / sizeof(double))
                   .Select(offset => BitConverter.ToDouble(ProfileDataX, offset * sizeof(double)))
                   .ToArray();
            }
            set
            {
                var result = new byte[value.Length * sizeof(double)];
                Buffer.BlockCopy(value, 0, result, 0, result.Length);
                ProfileDataX = result;
            }
        }

        public class ProfileValuesClass
        {
          
            public double CheckAreaWidthMM { get; set; }
            public double OffsetMM { get; set; }
            public double FirstXValueMM { get; set; }
        }
    }
}