﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data.DataObjects
{
    public class Defect
    {
        public Guid DefectID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public byte[] Version { get; set; }
        public int OrderNr { get; set; }
    }
}
