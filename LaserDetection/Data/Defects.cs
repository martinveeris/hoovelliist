﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class Defects
    {

        internal static List<DataObjects.Defect> GetList()
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var itemList = from l in db.Defects.Where(a => a.IsActive == true).OrderBy(a => a.OrderNr)
                               select new DataObjects.Defect()
                                {
                                    DefectID = l.DefectID,
                                    Name = l.Name,
                                    IsActive = l.IsActive,
                                    Version = l.Version,
                                    OrderNr = l.OrderNr
                                };

                return itemList.ToList();
            }
        }

        internal static DataObjects.Defect Get(Guid DefectID)
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var itemList = from l in db.Defects.Where(a => a.DefectID == DefectID).OrderBy(a => a.OrderNr)
                               select new DataObjects.Defect()
                               {
                                   DefectID = l.DefectID,
                                   Name = l.Name,
                                   IsActive = l.IsActive,
                                   Version = l.Version,
                                   OrderNr = l.OrderNr
                               };

                return itemList.FirstOrDefault();
            }
        }

    }
}