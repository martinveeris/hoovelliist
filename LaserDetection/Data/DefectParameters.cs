﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDetection.Data
{
    internal static class DefectParameters
    {
        internal static List<DataObjects.DefectParameter> GetList()
        {
            using (DetectionDatabase db = new DetectionDatabase())
            {
                var laserList = from l in db.DefectParameters
                                select new DataObjects.DefectParameter()
                                {
                                    DefectParameterID = l.DefectParameterID,
                                    Name = l.Name,
                                    Version = l.Version
                                };

                return laserList.ToList();
            }
        }
    }
}