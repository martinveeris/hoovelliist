﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sensors
{
    public class ReadWriteSensor
    {
        public static bool GetSensorValue(int lineNumber)
        {
            var readLine = new SensorRead.Line();
            readLine.line__32Number = Convert.ToSByte(lineNumber);
            readLine.line__32Type = SensorRead.Line__32Type.ISO;
            bool sensorValue = false;

            SensorRead.SensorRead.ReadSensorValue(new NationalInstruments.LabVIEW.Interop.LVDataAcquisitionChannel("RIO0"), readLine, out sensorValue);

            return sensorValue;
        }

        public static void SetSensorValue(int lineNumber, bool value)
        {
            var line = new SensorWrite.Line();
            line.line__32Number = Convert.ToSByte(lineNumber);
            line.line__32Type = SensorWrite.Line__32Type.ISO;

            SensorWrite.SensorWrite.WriteSignal(value, new NationalInstruments.LabVIEW.Interop.LVDataAcquisitionChannel("RIO0"), line);
        }
    }
}