﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorApp
{
    public partial class Form1 : Form
    {
        TCPAsyncClass tcpAsync;

        public Form1()
        {
            InitializeComponent();
        }

        private void InitTCP()
        {
            tcpAsync = new TCPAsyncClass(13000);
            tcpAsync.GetImage += TcpAsync_GetImage;
            tcpAsync.ProcessImage += TcpAsync_ProcessImage;
            tcpAsync.ShowResult += TcpAsync_ShowResult;
            tcpAsync.RunAsync();
        }

        private void TcpAsync_ShowResult(object sender, TCPAsyncClass.DataReceivedEventArgs e)
        {
            Bitmap resultImage = resultClass.GetResultImage();
            e.ResponseString = resultClass.GetJsonString();

            resultFormDialog.pictureBox1.InvokeIfRequired(a => { ((PictureBox)a).Image = resultImage; });
            resultFormDialog.Show();
            resultClass.ClearResult();
        }

        private void TcpAsync_ProcessImage(object sender, TCPAsyncClass.DataReceivedEventArgs e)
        {
            string text = string.Format("TCP Output: {0}", e.ReceivedString);
            UpdateLabelValue(labelTcpOutput, text);

            ProcessImage(e.ReceivedString, false);

            e.ResponseString = "PROCESSING_READY";
        }

        private void TcpAsync_GetImage(object sender, TCPAsyncClass.DataReceivedEventArgs e)
        {
            string text = string.Format("TCP Output: {0}", e.ReceivedString);
            UpdateLabelValue(labelTcpOutput, text);

            int row = GetCurrentRowParameter(text, true);
            UpdateLabelValue(labelFixture, string.Format("Area: {0}", row));

            GetImage(e.ReceivedString);

            e.ResponseString = "IMAGE_OK";

        }
    }
}
