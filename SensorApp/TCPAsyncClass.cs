﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SensorApp
{
    class TCPAsyncClass
    {
        public event EventHandler<SensorReadEventArgs> ReadSensorValue;

        public int Port { get; set; }

        private TcpListener listener;


        public TCPAsyncClass(int port)
        {
            Port = port;
            listener = new TcpListener(IPAddress.Any, Port);
            listener.Start();
        }

        public void Stop()
        {
            if (listener != null)
                listener.Stop();
        }

        //public void SendMessage(string message)
        //{
        //    var client = listener.AcceptTcpClient();
        //    NetworkStream stream = client.GetStream();
        //    byte[] msg = System.Text.Encoding.ASCII.GetBytes(message);
        //    stream.Write(msg, 0, msg.Length);
        //    client.Close();
        //}

        public async void RunAsync()
        {
            byte[] bytes = new byte[1024];
            string data;
            while (true)
            {
                Console.Write("Waiting for a connection... ");

                // Perform a blocking call to accept requests.
                // You could also user server.AcceptSocket() here.
                TcpClient client = await listener.AcceptTcpClientAsync();
                //Console.WriteLine("Connected!");

                // Get a stream object for reading and writing
                NetworkStream stream = client.GetStream();

                int i;

                // Loop to receive all the data sent by the client.
                i = stream.Read(bytes, 0, bytes.Length);

                string resultString = string.Empty;
                while (i != 0)
                {
                    // Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    resultString += data;
                    Console.WriteLine(String.Format("Received: {0}", data));

                    if (data.Contains("<EOF>"))
                    {
                        break;
                    }

                    i = stream.Read(bytes, 0, bytes.Length);
                }

                if (resultString.Contains("RESULT"))
                {
                    string responseString = FireShowResultEvent();
                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(responseString);
                    // Send back a response.
                    stream.Write(msg, 0, msg.Length);
                }
                else if (resultString.Contains("START"))
                {
                    CurrentRequestString = resultString;

                    string responseString = FireGetImageEvent(resultString);
                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(responseString);
                    // Send back a response.
                    stream.Write(msg, 0, msg.Length);

                    // send back second response
                    string responseStringProcess = FireProcessImageEvent(resultString);
                    byte[] msgResponse = System.Text.Encoding.ASCII.GetBytes(responseStringProcess);
                    // Send back a response.
                    stream.Write(msgResponse, 0, msgResponse.Length);
                }

                // Shutdown and end connection
                client.Close();
            }
        }

        private string FireGetImageEvent(string receivedString)
        {
            if (GetImage != null)
            {
                var args = new DataReceivedEventArgs();
                args.ReceivedString = receivedString;
                GetImage(this, args);
                return args.ResponseString;
            }

            return string.Empty;
        }

        private string FireProcessImageEvent(string receivedString)
        {
            if (ProcessImage != null)
            {
                var args = new DataReceivedEventArgs();
                args.ReceivedString = receivedString;
                ProcessImage(this, args);
                return args.ResponseString;
            }

            return string.Empty;
        }

        private string FireShowResultEvent()
        {
            if (ShowResult != null)
            {
                var args = new DataReceivedEventArgs();
                ShowResult(this, args);
                return args.ResponseString;
            }

            return string.Empty;
        }

        public class SensorReadEventArgs : EventArgs
        {
            public bool SensorValue { get; set; }
        }

    }
}
